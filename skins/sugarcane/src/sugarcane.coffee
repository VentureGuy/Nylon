###
**
** Sugarcane/Responsive specific code follows
**
###

hasPushState = ! !window.history and typeof window.history.pushState == 'function' and ((a) ->
  # iOS Safari: setItem throws in private mode
  try
    a.setItem 'test', '1'
    a.removeItem 'test'
    return true
  catch e
    return false
  return
)(window.sessionStorage)

#Tale::canBookmark = ->
#  @canUndo() and !@storysettings.lookup('hash') and (@storysettings.lookup('bookmark', true) or !hasPushState)

NylonHistory::init = ->
  @_author = null
  @_bookmark = null
  @_credits = null
  @_header = null
  @_passages = null
  @_restart = null
  @_rightMenu = null
  @_sidebar = null
  @_snapback = null
  @_snapbackMenu = null
  @_storyMenu = null
  @_subtitle = null
  @_title = null
  @_titlebar = null
  @_titlesep = null

  @createElements()
  a = this
  if !@restore()
    if tale.has('StoryInit')
      tale.get 'StoryInit', (page) ->
        new Wikifier(insertElement(null, 'span'), page.text)
    @display 'Start', null
  if !hasPushState
    @hash = window.location.hash
    @interval = window.setInterval((->
      a.watchHash()
      return
    ), 250)
  return

hasPushState and
(NylonHistory::pushState = (replace, uri) ->
  window.history[if replace then 'replaceState' else 'pushState'] {
    id: @id
    length: @history.length
  }, document.title, uri
  return
)

NylonHistory::addStoryElement = (parent, tag, id, classes, hide) ->
    e = document.createElement tag
    parent.appendChild e
    e.id = id
    e.classList.add 'storyElement'
    if classes.length
        e.classList.add(x) for x in classes
    if hide
        e.style.display = 'none'
    return e

NylonHistory::createElements = () ->
    '''
    <div id="header" class="storyElement" style="display:none"></div>
    <ul id="sidebar">
        <li id="title" class="storyElement">
            <span id="storyTitle" class="storyElement"></span>
            <span id="storySubtitle" class="storyElement"></span>
            <span id="titleSeparator"></span>
            <span id="storyAuthor" class="storyElement"></span>
        </li>
        <li id="storyMenu" class="storyElement" style="display:none"></li>
        <li id="rightMenu" class="storyElement" style="display:none"></li>
        <li><a href="javascript:;" id="snapback">Rewind</a></li>
        <li><a href="javascript:;" id="restart">Restart</a></li>
        <li><a id="bookmark" title="Permanent link to this passage">Bookmark</a></li>
        <li id="credits">
        This story was created with <s><a href="http://twinery.org/">Twine</a></s> <a href="https://gitlab.com/VentureGuy/Nylon" class="nylon-crossout">Nylon</a> and is powered by <a href="http://tiddlywiki.com/">TiddlyWiki</a>
        </li>
    </ul>
    <div id="snapbackMenu" class="menu"></div>
    <div id="passages">
    <noscript><div id="noscript">Please enable Javascript to play this story!</div></noscript>
    <style>
    #sidebar{display:none;}
    </style>
    </div>
    '''
    body = document.body
    @_header = @addStoryElement body, 'div', 'header', [], yes
    @_sidebar = @addStoryElement body, 'ul', 'sidebar', [], no

    @_titlebar = @addStoryElement @_sidebar, 'li', 'title', [], no

    @_title = @addStoryElement @_titlebar, 'span', 'storyTitle', [], no
    @_subtitle = @addStoryElement @_titlebar, 'span', 'storySubtitle', [], no
    @_titlesep = @addStoryElement @_titlebar, 'span', 'titleSeparator', [], no
    @_author = @addStoryElement @_titlebar, 'span', 'storyAuthor', [], no

    @_storyMenu = @addStoryElement @_sidebar, 'li', 'storyMenu', [], yes
    @_rightMenu = @addStoryElement @_sidebar, 'li', 'rightMenu', [], yes
    @_snapback = @addStoryElement @_sidebar, 'li', 'snapback', [], yes
    @_snapback.innerText = 'Rewind'
    @_restart = @addStoryElement @_sidebar, 'li', 'restart', [], yes
    @_restart.innerText = 'Restart'
    @_bookmark = @addStoryElement @_sidebar, 'li', 'bookmark', [], yes
    @_bookmark.innerText = 'Bookmark'
    @_credits = @addStoryElement @_sidebar, 'li', 'credits', [], yes
    @_credits.innerHTML = 'This story was created with <s><a href="http://twinery.org/">Twine</a></s> <a href="https://gitlab.com/VentureGuy/Nylon" class="nylon-crossout">Nylon</a> and is powered by <a href="http://tiddlywiki.com/">TiddlyWiki</a>'
    @_snapbackMenu = @addStoryElement body, 'div', 'snapbackMenu', ['menu'], no
    @_passages = @addStoryElement body, 'div', 'passages', [], no
    return

NylonHistory::display = (title, source, type, callback) ->
  i = undefined
  e = undefined
  q = undefined
  bookmark = undefined
  hash = undefined
  tale.get title, (c) =>
      p = document.getElementById('passages')
      if p == null
        p = document.createElement 'div'
        document.body.appendChild p
        p.setAttribute 'id', 'passages'
      if c == null
        return
      if type != 'back'
        @saveVariables c, source, callback
        hash = tale.storysettings.lookup('hash') and @save() or ''
        if hasPushState and tale.canUndo()
          try
            sessionStorage.setItem 'Twine.History' + @id, JSON.stringify(decompile(@history))
            @pushState @history.length <= 2 and window.history.state == '', hash
          catch e
            alert 'Your browser couldn\'t save the state of the ' + tale.identity() + '.\n' + 'You may continue playing, but it will no longer be possible to undo moves from here on in.'
            tale.storysettings.undo = 'off'
      @hash = hash or @save()
      e = c.render()
      if type != 'quietly'
        if hasTransition
          i = 0
          while i < p.childNodes.length
            q = p.childNodes[i]
            q.classList.add 'transition-out'
            setTimeout ((a) ->
              ->
                if a.parentNode
                  a.parentNode.removeChild a
                return
            )(q), 1000
            i += 1
          e.classList.add 'transition-in'
          setTimeout (->
            e.classList.remove 'transition-in'
            return
          ), 1
          e.style.visibility = 'visible'
          p.appendChild e
        else
          removeChildren p
          p.appendChild e
          fade e, fade: 'in'
      else
        p.appendChild e
        e.style.visibility = 'visible'

      ### Nylon Patch for Twine 1.4.2 Sugarcane ###

      if window.preDisplay != undefined
        console.log 'Calling preDisplay for %s', title
        if '*' of window.preDisplay
          window.preDisplay['*'] title, source, type
        if title of window.preDisplay
          window.preDisplay[title] title, source, type

      ### End Nylon Patch ###

      tale.setPageElements()
      if tale.canUndo()
        if !hasPushState and type != 'back'
          window.location.hash = @hash
        else if tale.canBookmark()
          bookmark = document.getElementById('bookmark')
          bookmark and (bookmark.href = @hash)
      window.scroll 0, 0

      ### Nylon Patch for Twine 1.4.2 Sugarcane ###

      if window.postDisplay != undefined
        console.log 'Calling postDisplay for %s', title
        if title of window.postDisplay
          window.postDisplay[title] title, source, type
        if '*' of window.postDisplay
          window.postDisplay['*'] title, source, type

      ### End Nylon Patch ###

      #e
      callback and callback e

NylonHistory::watchHash = ->
  if window.location.hash != @hash
    if window.location.hash and window.location.hash != '#'
      @history = [ {
        passage: null
        variables: {}
      } ]
      removeChildren document.getElementById('passages')
      if !@restore()
        alert 'The passage you had previously visited could not be found.'
    else
      window.location.reload()
    @hash = window.location.hash
  return

NylonHistory::loadLinkVars = ->
  for c of @history[0].linkVars
    @history[0].variables[c] = clone(@history[0].linkVars[c])
  return

NylonPassage::render = ->
  b = insertElement(null, 'div', 'passage' + @title, 'passage')
  b.style.visibility = 'hidden'
  @setTags b
  @setCSS()
  insertElement b, 'div', '', 'header'
  a = insertElement(b, 'div', '', 'body content')
  for cb in tale.Prerender
    typeof cb == 'function' and cb.call(this, a)
  new Wikifier(a, @processText())
  insertElement b, 'div', '', 'footer'
  for cb in tale.Postrender
    typeof cb == 'function' and cb.call(this, a)
  b

NylonPassage::excerpt = ->
  b = @text.replace(/<<.*?>>/g, '')
  b = b.replace(/!.*?\n/g, '')
  b = b.replace(/[\[\]\/]/g, '')
  a = b.split('\n')
  while a.length and a[0].length == 0
    a.shift()
  c = ''
  if a.length == 0 or a[0].length == 0
    c = @title
  else
    c = a[0].substr(0, 30) + '...'
  c

NylonPassage.transitionCache = ''

NylonPassage::setCSS = ->
  trans = false
  text = ''
  tags = @tags or []
  c = document.getElementById('tagCSS')
  c2 = document.getElementById('transitionCSS')
  if c and c.getAttribute('data-tags') != tags.join(' ')
    tale.forEachStylesheet tags, (passage) ->
      if ~passage.tags.indexOf('transition')
        if !NylonPassage.transitionCache and c2
          NylonPassage.transitionCache = c2.innerHTML
        setTransitionCSS passage.text
        trans = true
      else
        text += alterCSS(passage.text)
      return
    if !trans and NylonPassage.transitionCache and c2
      setTransitionCSS NylonPassage.transitionCache
      trans = false
      NylonPassage.transitionCache = ''
    if c.styleSheet then (c.styleSheet.cssText = text) else (c.innerHTML = text)
    c.setAttribute 'data-tags', tags.join(' ')
  return

Interface =
  init: ->
    snapback = document.getElementById('snapback')
    restart = document.getElementById('restart')
    bookmark = document.getElementById('bookmark')
    main()
    if !tale
      return
    if snapback
      if !tale.lookup('tags', 'bookmark').length
        snapback.parentNode.removeChild snapback
      else
        addClickHandler snapback, Interface.showSnapback
    if bookmark and (!tale.canBookmark() or !hasPushState)
      bookmark.parentNode.removeChild bookmark
    restart and addClickHandler(restart, Interface.restart)
    return
  restart: ->
    if confirm('Are you sure you want to restart this ' + tale.identity() + '?')
      state.restart()
    return
  showSnapback: (a) ->
    Interface.hideAllMenus()
    Interface.buildSnapback()
    Interface.showMenu a, document.getElementById('snapbackMenu')
    return
  buildSnapback: ->
    b = undefined
    c = false
    menuelem = document.getElementById('snapbackMenu')
    while menuelem.hasChildNodes()
      menuelem.removeChild menuelem.firstChild
    a = state.history.length - 1
    while a >= 0
      if state.history[a].passage and state.history[a].passage.tags.indexOf('bookmark') != -1
        b = document.createElement('div')
        b.pos = a
        addClickHandler b, ->
          macros.back.onclick true, @pos
        b.innerHTML = state.history[a].passage.excerpt()
        menuelem.appendChild b
        c = true
      a--
    b = null
    if !c
      b = document.createElement('div')
      b.innerHTML = '<i>No passages available</i>'
      document.getElementById('snapbackMenu').appendChild b
    return
  hideAllMenus: ->
    document.getElementById('snapbackMenu').style.display = 'none'
    return
  showMenu: (b, a) ->
    if !b
      b = window.event
    c =
      x: 0
      y: 0
    if b.pageX or b.pageY
      c.x = b.pageX
      c.y = b.pageY
    else
      if b.clientX or b.clientY
        c.x = b.clientX + document.body.scrollLeft + document.documentElement.scrollLeft
        c.y = b.clientY + document.body.scrollTop + document.documentElement.scrollTop
    a.style.top = c.y + 'px'
    a.style.left = c.x + 'px'
    a.style.display = 'block'
    addClickHandler document, Interface.hideAllMenus
    b.cancelBubble = true
    if b.stopPropagation
      b.stopPropagation()
    return
#window.onload = Interface.init;

macros.back.onclick = (back, steps) ->
  title = undefined
  if back
    if tale.canUndo()
      window.history.go -steps
      return
    while steps-- >= 0 and state.history.length > 1
      title = state.history[0].passage.title
      state.history.shift()
    state.loadLinkVars()
    tale.get title, (page) =>
        state.saveVariables page
        state.display title, null, 'back'
  else
    state.display state.history[steps].passage.title
  return

window.onpopstate = (e) ->
  title = undefined
  hist = undefined
  steps = undefined
  i = undefined
  s = e and e.state
  if s and s.id and s.length != null
    hist = recompile(JSON.parse(sessionStorage.getItem('Twine.History' + s.id)))
    if hist
      steps = hist.length - (s.length)
  if steps != null
    state.history = hist
    # Shift the position of history to match how far back we've gone
    while steps-- >= 0 and state.history.length > 1
      title = state.history[0].passage.title
      state.history.shift()
    state.loadLinkVars()
    tale.get title, (page) =>
        state.saveVariables page
        state.display title, null, 'back'
  return

# ---
# generated by js2coffee 2.2.0
