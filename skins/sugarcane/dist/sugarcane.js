/*
**
** Sugarcane/Responsive specific code follows
**
 */
var Interface, hasPushState;

hasPushState = !!window.history && typeof window.history.pushState === 'function' && (function(a) {
  var e;
  try {
    // iOS Safari: setItem throws in private mode
    a.setItem('test', '1');
    a.removeItem('test');
    return true;
  } catch (error) {
    e = error;
    return false;
  }
})(window.sessionStorage);

//Tale::canBookmark = ->
//  @canUndo() and !@storysettings.lookup('hash') and (@storysettings.lookup('bookmark', true) or !hasPushState)
NylonHistory.prototype.init = function() {
  var a;
  this._author = null;
  this._bookmark = null;
  this._credits = null;
  this._header = null;
  this._passages = null;
  this._restart = null;
  this._rightMenu = null;
  this._sidebar = null;
  this._snapback = null;
  this._snapbackMenu = null;
  this._storyMenu = null;
  this._subtitle = null;
  this._title = null;
  this._titlebar = null;
  this._titlesep = null;
  this.createElements();
  a = this;
  if (!this.restore()) {
    if (tale.has('StoryInit')) {
      tale.get('StoryInit', function(page) {
        return new Wikifier(insertElement(null, 'span'), page.text);
      });
    }
    this.display('Start', null);
  }
  if (!hasPushState) {
    this.hash = window.location.hash;
    this.interval = window.setInterval((function() {
      a.watchHash();
    }), 250);
  }
};

hasPushState && (NylonHistory.prototype.pushState = function(replace, uri) {
  window.history[replace ? 'replaceState' : 'pushState']({
    id: this.id,
    length: this.history.length
  }, document.title, uri);
});

NylonHistory.prototype.addStoryElement = function(parent, tag, id, classes, hide) {
  var e, j, len, x;
  e = document.createElement(tag);
  parent.appendChild(e);
  e.id = id;
  e.classList.add('storyElement');
  if (classes.length) {
    for (j = 0, len = classes.length; j < len; j++) {
      x = classes[j];
      e.classList.add(x);
    }
  }
  if (hide) {
    e.style.display = 'none';
  }
  return e;
};

NylonHistory.prototype.createElements = function() {
  `<div id="header" class="storyElement" style="display:none"></div>
<ul id="sidebar">
    <li id="title" class="storyElement">
        <span id="storyTitle" class="storyElement"></span>
        <span id="storySubtitle" class="storyElement"></span>
        <span id="titleSeparator"></span>
        <span id="storyAuthor" class="storyElement"></span>
    </li>
    <li id="storyMenu" class="storyElement" style="display:none"></li>
    <li id="rightMenu" class="storyElement" style="display:none"></li>
    <li><a href="javascript:;" id="snapback">Rewind</a></li>
    <li><a href="javascript:;" id="restart">Restart</a></li>
    <li><a id="bookmark" title="Permanent link to this passage">Bookmark</a></li>
    <li id="credits">
    This story was created with <s><a href="http://twinery.org/">Twine</a></s> <a href="https://gitlab.com/VentureGuy/Nylon" class="nylon-crossout">Nylon</a> and is powered by <a href="http://tiddlywiki.com/">TiddlyWiki</a>
    </li>
</ul>
<div id="snapbackMenu" class="menu"></div>
<div id="passages">
<noscript><div id="noscript">Please enable Javascript to play this story!</div></noscript>
<style>
#sidebar{display:none;}
</style>
</div>`;
  var body;
  body = document.body;
  this._header = this.addStoryElement(body, 'div', 'header', [], true);
  this._sidebar = this.addStoryElement(body, 'ul', 'sidebar', [], false);
  this._titlebar = this.addStoryElement(this._sidebar, 'li', 'title', [], false);
  this._title = this.addStoryElement(this._titlebar, 'span', 'storyTitle', [], false);
  this._subtitle = this.addStoryElement(this._titlebar, 'span', 'storySubtitle', [], false);
  this._titlesep = this.addStoryElement(this._titlebar, 'span', 'titleSeparator', [], false);
  this._author = this.addStoryElement(this._titlebar, 'span', 'storyAuthor', [], false);
  this._storyMenu = this.addStoryElement(this._sidebar, 'li', 'storyMenu', [], true);
  this._rightMenu = this.addStoryElement(this._sidebar, 'li', 'rightMenu', [], true);
  this._snapback = this.addStoryElement(this._sidebar, 'li', 'snapback', [], true);
  this._snapback.innerText = 'Rewind';
  this._restart = this.addStoryElement(this._sidebar, 'li', 'restart', [], true);
  this._restart.innerText = 'Restart';
  this._bookmark = this.addStoryElement(this._sidebar, 'li', 'bookmark', [], true);
  this._bookmark.innerText = 'Bookmark';
  this._credits = this.addStoryElement(this._sidebar, 'li', 'credits', [], true);
  this._credits.innerHTML = 'This story was created with <s><a href="http://twinery.org/">Twine</a></s> <a href="https://gitlab.com/VentureGuy/Nylon" class="nylon-crossout">Nylon</a> and is powered by <a href="http://tiddlywiki.com/">TiddlyWiki</a>';
  this._snapbackMenu = this.addStoryElement(body, 'div', 'snapbackMenu', ['menu'], false);
  this._passages = this.addStoryElement(body, 'div', 'passages', [], false);
};

NylonHistory.prototype.display = function(title, source, type, callback) {
  var bookmark, e, hash, i, q;
  i = void 0;
  e = void 0;
  q = void 0;
  bookmark = void 0;
  hash = void 0;
  return tale.get(title, (c) => {
    var p;
    p = document.getElementById('passages');
    if (p === null) {
      p = document.createElement('div');
      document.body.appendChild(p);
      p.setAttribute('id', 'passages');
    }
    if (c === null) {
      return;
    }
    if (type !== 'back') {
      this.saveVariables(c, source, callback);
      hash = tale.storysettings.lookup('hash') && this.save() || '';
      if (hasPushState && tale.canUndo()) {
        try {
          sessionStorage.setItem('Twine.History' + this.id, JSON.stringify(decompile(this.history)));
          this.pushState(this.history.length <= 2 && window.history.state === '', hash);
        } catch (error) {
          e = error;
          alert('Your browser couldn\'t save the state of the ' + tale.identity() + '.\n' + 'You may continue playing, but it will no longer be possible to undo moves from here on in.');
          tale.storysettings.undo = 'off';
        }
      }
    }
    this.hash = hash || this.save();
    e = c.render();
    if (type !== 'quietly') {
      if (hasTransition) {
        i = 0;
        while (i < p.childNodes.length) {
          q = p.childNodes[i];
          q.classList.add('transition-out');
          setTimeout((function(a) {
            return function() {
              if (a.parentNode) {
                a.parentNode.removeChild(a);
              }
            };
          })(q), 1000);
          i += 1;
        }
        e.classList.add('transition-in');
        setTimeout((function() {
          e.classList.remove('transition-in');
        }), 1);
        e.style.visibility = 'visible';
        p.appendChild(e);
      } else {
        removeChildren(p);
        p.appendChild(e);
        fade(e, {
          fade: 'in'
        });
      }
    } else {
      p.appendChild(e);
      e.style.visibility = 'visible';
    }
    /* Nylon Patch for Twine 1.4.2 Sugarcane */
    if (window.preDisplay !== void 0) {
      console.log('Calling preDisplay for %s', title);
      if ('*' in window.preDisplay) {
        window.preDisplay['*'](title, source, type);
      }
      if (title in window.preDisplay) {
        window.preDisplay[title](title, source, type);
      }
    }
    /* End Nylon Patch */
    tale.setPageElements();
    if (tale.canUndo()) {
      if (!hasPushState && type !== 'back') {
        window.location.hash = this.hash;
      } else if (tale.canBookmark()) {
        bookmark = document.getElementById('bookmark');
        bookmark && (bookmark.href = this.hash);
      }
    }
    window.scroll(0, 0);
    /* Nylon Patch for Twine 1.4.2 Sugarcane */
    if (window.postDisplay !== void 0) {
      console.log('Calling postDisplay for %s', title);
      if (title in window.postDisplay) {
        window.postDisplay[title](title, source, type);
      }
      if ('*' in window.postDisplay) {
        window.postDisplay['*'](title, source, type);
      }
    }
    /* End Nylon Patch */
    //e
    return callback && callback(e);
  });
};

NylonHistory.prototype.watchHash = function() {
  if (window.location.hash !== this.hash) {
    if (window.location.hash && window.location.hash !== '#') {
      this.history = [
        {
          passage: null,
          variables: {}
        }
      ];
      removeChildren(document.getElementById('passages'));
      if (!this.restore()) {
        alert('The passage you had previously visited could not be found.');
      }
    } else {
      window.location.reload();
    }
    this.hash = window.location.hash;
  }
};

NylonHistory.prototype.loadLinkVars = function() {
  var c;
  for (c in this.history[0].linkVars) {
    this.history[0].variables[c] = clone(this.history[0].linkVars[c]);
  }
};

NylonPassage.prototype.render = function() {
  var a, b, cb, j, k, len, len1, ref, ref1;
  b = insertElement(null, 'div', 'passage' + this.title, 'passage');
  b.style.visibility = 'hidden';
  this.setTags(b);
  this.setCSS();
  insertElement(b, 'div', '', 'header');
  a = insertElement(b, 'div', '', 'body content');
  ref = tale.Prerender;
  for (j = 0, len = ref.length; j < len; j++) {
    cb = ref[j];
    typeof cb === 'function' && cb.call(this, a);
  }
  new Wikifier(a, this.processText());
  insertElement(b, 'div', '', 'footer');
  ref1 = tale.Postrender;
  for (k = 0, len1 = ref1.length; k < len1; k++) {
    cb = ref1[k];
    typeof cb === 'function' && cb.call(this, a);
  }
  return b;
};

NylonPassage.prototype.excerpt = function() {
  var a, b, c;
  b = this.text.replace(/<<.*?>>/g, '');
  b = b.replace(/!.*?\n/g, '');
  b = b.replace(/[\[\]\/]/g, '');
  a = b.split('\n');
  while (a.length && a[0].length === 0) {
    a.shift();
  }
  c = '';
  if (a.length === 0 || a[0].length === 0) {
    c = this.title;
  } else {
    c = a[0].substr(0, 30) + '...';
  }
  return c;
};

NylonPassage.transitionCache = '';

NylonPassage.prototype.setCSS = function() {
  var c, c2, tags, text, trans;
  trans = false;
  text = '';
  tags = this.tags || [];
  c = document.getElementById('tagCSS');
  c2 = document.getElementById('transitionCSS');
  if (c && c.getAttribute('data-tags') !== tags.join(' ')) {
    tale.forEachStylesheet(tags, function(passage) {
      if (~passage.tags.indexOf('transition')) {
        if (!NylonPassage.transitionCache && c2) {
          NylonPassage.transitionCache = c2.innerHTML;
        }
        setTransitionCSS(passage.text);
        trans = true;
      } else {
        text += alterCSS(passage.text);
      }
    });
    if (!trans && NylonPassage.transitionCache && c2) {
      setTransitionCSS(NylonPassage.transitionCache);
      trans = false;
      NylonPassage.transitionCache = '';
    }
    if (c.styleSheet) {
      (c.styleSheet.cssText = text);
    } else {
      (c.innerHTML = text);
    }
    c.setAttribute('data-tags', tags.join(' '));
  }
};

Interface = {
  init: function() {
    var bookmark, restart, snapback;
    snapback = document.getElementById('snapback');
    restart = document.getElementById('restart');
    bookmark = document.getElementById('bookmark');
    main();
    if (!tale) {
      return;
    }
    if (snapback) {
      if (!tale.lookup('tags', 'bookmark').length) {
        snapback.parentNode.removeChild(snapback);
      } else {
        addClickHandler(snapback, Interface.showSnapback);
      }
    }
    if (bookmark && (!tale.canBookmark() || !hasPushState)) {
      bookmark.parentNode.removeChild(bookmark);
    }
    restart && addClickHandler(restart, Interface.restart);
  },
  restart: function() {
    if (confirm('Are you sure you want to restart this ' + tale.identity() + '?')) {
      state.restart();
    }
  },
  showSnapback: function(a) {
    Interface.hideAllMenus();
    Interface.buildSnapback();
    Interface.showMenu(a, document.getElementById('snapbackMenu'));
  },
  buildSnapback: function() {
    var a, b, c, menuelem;
    b = void 0;
    c = false;
    menuelem = document.getElementById('snapbackMenu');
    while (menuelem.hasChildNodes()) {
      menuelem.removeChild(menuelem.firstChild);
    }
    a = state.history.length - 1;
    while (a >= 0) {
      if (state.history[a].passage && state.history[a].passage.tags.indexOf('bookmark') !== -1) {
        b = document.createElement('div');
        b.pos = a;
        addClickHandler(b, function() {
          return macros.back.onclick(true, this.pos);
        });
        b.innerHTML = state.history[a].passage.excerpt();
        menuelem.appendChild(b);
        c = true;
      }
      a--;
    }
    b = null;
    if (!c) {
      b = document.createElement('div');
      b.innerHTML = '<i>No passages available</i>';
      document.getElementById('snapbackMenu').appendChild(b);
    }
  },
  hideAllMenus: function() {
    document.getElementById('snapbackMenu').style.display = 'none';
  },
  showMenu: function(b, a) {
    var c;
    if (!b) {
      b = window.event;
    }
    c = {
      x: 0,
      y: 0
    };
    if (b.pageX || b.pageY) {
      c.x = b.pageX;
      c.y = b.pageY;
    } else {
      if (b.clientX || b.clientY) {
        c.x = b.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        c.y = b.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }
    }
    a.style.top = c.y + 'px';
    a.style.left = c.x + 'px';
    a.style.display = 'block';
    addClickHandler(document, Interface.hideAllMenus);
    b.cancelBubble = true;
    if (b.stopPropagation) {
      b.stopPropagation();
    }
  }
};

//window.onload = Interface.init;
macros.back.onclick = function(back, steps) {
  var title;
  title = void 0;
  if (back) {
    if (tale.canUndo()) {
      window.history.go(-steps);
      return;
    }
    while (steps-- >= 0 && state.history.length > 1) {
      title = state.history[0].passage.title;
      state.history.shift();
    }
    state.loadLinkVars();
    tale.get(title, (page) => {
      state.saveVariables(page);
      return state.display(title, null, 'back');
    });
  } else {
    state.display(state.history[steps].passage.title);
  }
};

window.onpopstate = function(e) {
  var hist, i, s, steps, title;
  title = void 0;
  hist = void 0;
  steps = void 0;
  i = void 0;
  s = e && e.state;
  if (s && s.id && s.length !== null) {
    hist = recompile(JSON.parse(sessionStorage.getItem('Twine.History' + s.id)));
    if (hist) {
      steps = hist.length - s.length;
    }
  }
  if (steps !== null) {
    state.history = hist;
    // Shift the position of history to match how far back we've gone
    while (steps-- >= 0 && state.history.length > 1) {
      title = state.history[0].passage.title;
      state.history.shift();
    }
    state.loadLinkVars();
    tale.get(title, (page) => {
      state.saveVariables(page);
      return state.display(title, null, 'back');
    });
  }
};

// ---
// generated by js2coffee 2.2.0

//# sourceMappingURL=sugarcane.js.map
