import os
from buildtools.maestro import BuildMaestro
from buildtools.maestro.web import UglifyJSTarget, DartSCSSBuildTarget
from buildtools.maestro.coffeescript import CoffeeBuildTarget

SUGARCANE_DIR = os.path.dirname(__file__)

def addToBuild(maestro: BuildMaestro, coffee_path, uglifyjs_path, sass_path, deps=[]):
    sugarcane = maestro.add(CoffeeBuildTarget(
        target=os.path.join(SUGARCANE_DIR, 'dist', 'sugarcane.js'),
        files=[os.path.join(SUGARCANE_DIR, 'src', 'sugarcane.coffee')],
        dependencies=deps,
        make_map=False,
        coffee_opts=['-bcm', '--no-header'],
        coffee_executable=coffee_path))

    minifiedSugarcane = maestro.add(UglifyJSTarget(
        target=os.path.join(SUGARCANE_DIR, 'dist', 'sugarcane.min.js'),
        inputfile=sugarcane.target,
        dependencies=[sugarcane.target],
        uglify_executable=uglifyjs_path))

    sugarcaneMainCSS = maestro.add(DartSCSSBuildTarget(
        target=os.path.join(SUGARCANE_DIR, 'dist', 'css', 'main.min.css'),
        files=[os.path.join(SUGARCANE_DIR, 'src', 'scss', 'main', 'main.scss')],
        output_style='compressed',
        sass_path=sass_path,
        dependencies=deps))
    sugarcaneTransitionCSS = maestro.add(DartSCSSBuildTarget(
        target=os.path.join(SUGARCANE_DIR, 'dist', 'css', 'transition.min.css'),
        files=[os.path.join(SUGARCANE_DIR, 'src', 'scss', 'transition.scss')],
        output_style='compressed',
        sass_path=sass_path,
        dependencies=deps))

    return [minifiedSugarcane.target, sugarcaneMainCSS.target, sugarcaneTransitionCSS.target]

def addToNylon(nylon):
    nylon.addEmbeddedCSS(os.path.join(SUGARCANE_DIR, 'dist', 'css', 'main.min.css'), id='baseCSS')
    nylon.addEmbeddedCSS(os.path.join(SUGARCANE_DIR, 'dist', 'css', 'transition.min.css'), id='transitionCSS')
    nylon.addEmbeddedCSS(None, id='tagCSS')
