# TiddlyWiki

TiddlyWiki is what powers Nylon and Twine.  Unfortunately, Twine appears to have diverged from stock TiddlyWiki a long time ago,
so we can't just slap in TiddlyWiki from npm.

This directory contains the source code for TiddlyWiki that we use in Twine, in CoffeeScript form because that's just how we roll.

Eventually, I'd like to move to our own parsing engine that uses an AST, but this will work, for now.

## Macros

All our macros are stored in coffee/.
