version.extensions.printMacro =
  major: 1
  minor: 1
  revision: 1
macros.print = handler: (place, macroName, params, parser) ->
  args = parser.fullArgs(macroName != 'print')
  output = undefined

  ### Nylon Patch for Twine 1.4.2 Sugarcane ###

  window.twineCurrentEl = place
  window.twineCurrentParser = parser

  ### End patch ###

  try
    # See comment within macros.display
    output = internalEval(args)
    if output != null and (typeof output != 'number' or !isNaN(output))
      new Wikifier(place, '' + output)
  catch e
    nylonErrorHandler place, parser, e, '<<print>> bad expression: ' + params.join(' ')
    throwError place, '<<print>> bad expression: ' + params.join(' '), parser.fullMatch()
  return

# ---
# generated by js2coffee 2.2.0
