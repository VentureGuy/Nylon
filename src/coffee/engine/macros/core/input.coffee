version.extensions.textInputMacro =
  major: 2
  minor: 0
  revision: 0
macros.checkbox = macros.radio = macros.textinput = handler: (A, C, D, parser) ->
  match = undefined
  class_ = C.replace('input', 'Input')
  q = A.querySelectorAll('input')
  id = class_ + '|' + (q and q.length or 0)
  input = insertElement(null, 'input', id, class_)
  input.name = D[0]
  input.type = C.replace('input', '')
  # IE 8 support - delay insertion until now
  A.appendChild input
  if C == 'textinput' and D[1]
    match = new RegExp(Wikifier.linkFormatter.lookahead).exec(parser.fullMatch())
    if match
      Wikifier.linkFormatter.makeLink A, match, macros.button.callback, 'button'
    else
      Wikifier.linkFormatter.makeLink A, [
        0
        D[2] or D[1]
        D[1]
      ], macros.button.callback, 'button'
  else if (C == 'radio' or C == 'checkbox') and D[1]
    input.value = D[1]
    insertElement(A, 'label', '', '', D[1]).setAttribute 'for', id
    if D[2]
      insertElement A, 'br'
      D.splice 1, 1
      macros[C].handler A, C, D

  ### Nylon Patch for Twine 1.4.2 Sugarcane ###

  window.twineCurrentEl = A
  window.twineCurrentParser = parser

  ### End patch ###

  return

# ---
# generated by js2coffee 2.2.0
