version.extensions.SilentlyMacro =
  major: 1
  minor: 1
  revision: 0
macros.nobr = macros.silently = handler: (place, macroName, f, parser) ->
  i = undefined
  h = insertElement(null, 'div')
  k = parser.source.indexOf('>>', parser.matchStart) + 2
  a = parser.source.slice(k)
  d = -1
  c = ''
  l = 0
  i = 0
  while i < a.length
    if a.substr(i, macroName.length + 7) == '<<end' + macroName + '>>'
      if l == 0
        d = k + i + macroName.length + 7
        break
      else
        l--
    else if a.substr(i, macroName.length + 4) == '<<' + macroName + '>>'
      l++
    if macroName == 'nobr' and a.charAt(i) == '\n'
      c += '‌'
      # Zero-width space
    else
      c += a.charAt(i)
    i++
  if d != -1
    new Wikifier((if macroName == 'nobr' then place else h), c)

    ### Nylon Patch for Twine 1.4.2 Sugarcane ###

    window.twineCurrentEl = place
    window.twineCurrentParser = parser

    ### End patch ###

    parser.nextMatch = d
  else
    throwError place, 'can\'t find matching <<end' + macroName + '>>', parser.fullMatch()
  return
macros.endsilently = handler: ->

# ---
# generated by js2coffee 2.2.0
