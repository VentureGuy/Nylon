version.extensions.ifMacros =
  major: 2
  minor: 0
  revision: 0
macros['if'] = handler: (place, macroName, params, parser) ->
  conditions = []
  clauses = []
  rawConds = []
  srcOffset = parser.source.indexOf('>>', parser.matchStart) + 2
  src = parser.source.slice(srcOffset)
  endPos = -1
  rawCond = params.join(' ')
  currentCond = parser.fullArgs()
  currentClause = ''
  t = 0
  nesting = 0
  i = 0
  while i < src.length
    if src.substr(i, 6) == '<<else' and !nesting
      rawConds.push rawCond
      conditions.push currentCond.trim()
      clauses.push currentClause
      currentClause = ''
      t = src.indexOf('>>', i + 6)
      if src.substr(i + 6, 4) == ' if ' or src.substr(i + 6, 3) == 'if '
        rawCond = src.slice(i + 9, t)
        currentCond = Wikifier.parse(rawCond)
      else
        rawCond = ''
        currentCond = 'true'
      i = t + 2
    if src.substr(i, 5) == '<<if '
      nesting++
    if src.substr(i, 9) == '<<endif>>'
      nesting--
      if nesting < 0
        endPos = srcOffset + i + 9
        rawConds.push rawCond
        conditions.push currentCond.trim()
        clauses.push currentClause
        break
    currentClause += src.charAt(i)
    i++
  if endPos != -1
    parser.nextMatch = endPos
    lastIndex = 0
    try
      i = 0
      while i < clauses.length
        lastIndex = i
        if internalEval(conditions[i])
          #console.log(clauses[i]);
          new Wikifier(place, clauses[i])

          ### Nylon Patch for Twine 1.4.2 Sugarcane ###

          window.twineCurrentEl = place
          window.twineCurrentParser = parser

          ### End patch ###

          break
        i++
    catch e
      console.log e

      ### Nylon Patch for Twine 1.4.2 Sugarcane ###

      console.log conditions[lastIndex]
      console.log clauses[lastIndex]
      console.log e.stack

      ### End patch ###

      throwError place, '<<' + (if i then 'else ' else '') + 'if>> bad condition: ' + rawConds[i], if !i then parser.fullMatch() else '<<else if ' + rawConds[i] + '>>'
  else
    throwError place, 'I can\'t find a matching <<endif>>', parser.fullMatch()
  return
macros['else'] = macros.elseif = macros.endif = handler: ->

# ---
# generated by js2coffee 2.2.0
