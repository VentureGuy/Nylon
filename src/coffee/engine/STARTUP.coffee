# @requires: engine/_funcsAndPolyfills.coffee
# @requires: engine/NylonStory.coffee

window.STARTUP = ->
    # Initialise Tale
    console.log "STARTUP.coffee: Starting NylonStory"
    window.tale = new NylonStory(if window.configuration then window.configuration else null)

    # Run all scripts
    console.log "STARTUP.coffee: Running scripts"
    for passage in tale.lookup 'tags', 'scripts'
        console.log "  STARTUP.coffee: passage", passage
        scriptEval passage
    # Init history
    console.log "STARTUP.coffee: Starting history"
    window.state = tale.initHistory()

    console.log "STARTUP.coffee: Starting macros"
    for macroID, macro of macros
        console.log "  STARTUP.coffee: macro", macroID
        if typeof macro.init == 'function'
            macro.init()

    # Collect CSS from stylesheets
    styleText = ''
    for passageID, passage of tale.passages
        if 'stylesheet' not of passage.tags
            continue
        if passage.tags.length == 1 and passage.tags[0] == 'stylesheet'
            styleText += passage.text
        else if passage.tags.length == 2 and passage.tags[1] == 'transition'
            setTransitionCSS passage.text

    #styleText = alertCSS styleText
    storyCSS = document.getElementById('storyCSS')
    if storyCSS.styleSheet
        storyCSS.styleSheet = styleText
    else
        storyCSS.innerHTML = styleText

    state.init()
    tale.load()
    return
