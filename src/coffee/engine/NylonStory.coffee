# @requires: engine/_funcsAndPolyfills.coffee
# @requires: engine/NylonHistory.coffee

###
# Nylon Core/Main Loop
###
class NylonStory
    constructor: (config=null) ->
        @Config={}

        @PassageLoader = null
        @Deobfuscator = null
        @LoadingProgressCallback = @_OnProgress
        @LoadingEndsCallback = @_OnLoadEnds
        @Prerender = []
        @Postrender = []

        @Config = {
            lookup: (a, b) ->
                if a not in @
                    return b
                return @[a]
        }
        if config != null
            for k,v of config
                @Config[k]=v
        else if 'nylonSettings' of window and window.nylonSettings != null
            for k,v of window.nylonSettings
                @Config[k]=v

        if 'obfuscation' not of @Config
            @Config.obfuscation = {
                'id': 'null'
            }
        obfID = @Config.obfuscation.id
        @Deobfuscator = NylonDeobfuscator.CreateByID @, obfID, @Config.obfuscation

        plID = NylonEmbeddedLoader.ID
        if 'loader' of @Config and 'id' of @Config.loader
            plID = @Config.loader.id
        @PassageLoader = NylonPassageLoader.CreateByID @, plID, @Config.loader or {}

    initHistory: ->
        return new NylonHistory(@Config)

    Object.defineProperties @prototype,
        ###
        # Backwards-compatibility with Twine.
        ###
        passages:
            get: ->
                return @PassageLoader.Passages

        ###
        # Backwards-compatibility with Twine.
        ###
        defaultTitle:
            get: ->
                return @Config.title or 'Untitled Story'

        ###
        # Backwards-compatibility with Twine.
        ###
        settings:
            get: ->
                return @Config
        ###
        # Backwards-compatibility with Twine.
        ###
        storysettings:
            get: ->
                return @Config

    load: ->
        @loadingscreen = document.createElement 'section'
        @loadingscreen.setAttribute 'id', 'nylon-loading-screen'
        document.body.prepend @loadingscreen

        @loadingicon = document.createElement 'img'
        @loadingicon.setAttribute 'id', 'nylon-loading-icon'
        @loadingicon.setAttribute 'src', '{LOADICON}' #replaced at compile-time with loadlogo.svg.
        @loadingscreen.appendChild @loadingicon

        @loadingbarouter = document.createElement 'div'
        @loadingbarouter.setAttribute 'id', 'nylon-loading-bar-outer'
        @loadingscreen.appendChild @loadingbarouter

        @loadingbar = document.createElement 'div'
        @loadingbar.setAttribute 'id', 'nylon-loading-bar-inner'
        @loadingbarouter.appendChild @loadingbar

        @loadingtext = document.createElement 'span'
        @loadingtext.setAttribute 'id', 'nylon-loading-text'
        @loadingbar.appendChild @loadingtext

        @loadingmessage = document.createElement 'span'
        @loadingmessage.setAttribute 'id', 'nylon-loading-message'
        @loadingscreen.appendChild @loadingmessage

        return @PassageLoader.load
            progress: (message, step, steps) =>
                @LoadingProgressCallback message, step, steps
            end: =>
                @LoadingEndsCallback()

    _OnProgress: (message, step, steps) ->
        ###
        <section id="loadingscreen">
            <img id="loadingicon" src="blob:application/svg:..." />
            <div id="loadingbar">
                <span id="loadingtext">...</span>
            </div>
            <span id="loadingtext">...</span>
        </section>
        ###
        @loadingscreen.style.display = 'fixed'
        @loadingmessage.innerText = message
        pct=(step/steps)*100.0
        @loadingbar.style.width = "#{pct}%"
        return

    _OnLoadEnds: ->
        @loadingbar = null
        @loadingbarouter = null
        @loadingicon = null
        @loadingmessage = null
        @loadingtext = null
        @loadingscreen.parentNode.removeChild @loadingscreen
        @loadingscreen = null

        state.display 'Start'
        return

    has: (title_or_id) ->
        return @PassageLoader.has title_or_id

    get: (title_or_id, done_callback) ->
        @PassageLoader.get title_or_id, done_callback
        return

    _get: (title_or_id) ->
        return @PassageLoader._get title_or_id

    lookup: (key, value, sortBy='title') ->
        matches = []
        for title, passage of @PassageLoader.Passages
            for i in [0..passage[key].length]
                if passage[key][i] == value
                    matches.push passage
        matches.sort (a, b) ->
            if a[sortBy] == b[sortBy]
                return 0
            else
                if a[sortBy] < b[sortBy]
                    return -1
                else
                    return 1
        return matches

    canUndo: ->
        return @Config['can-undo']

    identity: ->
        if !@identity
            meta = document.querySelector('meta[name=\'identity\']')
            @identity = if meta then meta.getAttribute('content') else 'story'
        return @identity

    forEachStylesheet: (tags=[], callback=null) ->
        if callback == null
            return
        for passage in Object.values @PassageLoader.Passages
            if passage and 'stylesheet' in passage.tags
                for tag in passage.tags
                    if tag in tags
                        callback passage
                        break
        return

    setPageElements: ->
        if window.preSetPageElements != undefined
            if '*' of window.preSetPageElements
                window.preSetPageElements['*']()

        setPageElement 'storyTitle', 'StoryTitle', @defaultTitle
        storyTitle = document.getElementById('storyTitle')
        document.title = @title = storyTitle and (storyTitle.textContent or storyTitle.innerText) or @defaultTitle

        setPageElement 'storySubtitle', 'StorySubtitle', ''

        if tale.has('StoryAuthor')
            setPageElement 'titleSeparator', null, '\n'
            setPageElement 'storyAuthor', 'StoryAuthor', ''

        if tale.has('StoryMenu')
            sm = document.getElementById('storyMenu')
            if sm == null
              sm = document.createElement 'div'
              document.body.appendChild sm
              sm.setAttribute 'id', 'storyMenu'
            sm.setAttribute 'style', ''
            setPageElement 'storyMenu', 'StoryMenu', ''

        if window.postSetPageElements != undefined
            #console.log("Calling postSetPageElements");
            if '*' of window.postSetPageElements
                window.postSetPageElements['*']()
        return

    canBookmark: => no
