###
# Wikifier object
#
# Directly converted from engine.js. Needs more work probably.
###
window.twineCurrentEl = null

class Wikifier
    @textPrimitives:
        upperLetter: '[A-ZÀ-ÞŐŰ]'
        lowerLetter: '[a-zß-ÿ_0-9\\-őű]'
        anyLetter:   '[A-Za-zÀ-Þß-ÿ_0-9\\-ŐŰőű]'
    @textPrimitives.variable = '\\$((?:' + Wikifier.textPrimitives.anyLetter.replace('\\-', '') + '*' + Wikifier.textPrimitives.anyLetter.replace('0-9\\-', '') + '+' + Wikifier.textPrimitives.anyLetter.replace('\\-', '') + '*)+)'
    @textPrimitives.unquoted = '(?=(?:[^"\'\\\\]*(?:\\\\.|\'(?:[^\'\\\\]*\\\\.)*[^\'\\\\]*\'|"(?:[^"\\\\]*\\\\.)*[^"\\\\]*"))*[^\'"]*$)'

    constructor: (place, source) ->
        @source = source
        @output = place

        ### Nylon Patch for Twine 1.4.2 Sugarcane ###
        window.twineCurrentEl = place
        ### End patch ###

        @nextMatch = 0
        @assembleFormatterMatches Wikifier.formatters
        @subWikify @output

    assembleFormatterMatches: (formatters) ->
        @formatters = []
        pattern = []
        for formatter in formatters
            pattern.push '(' + formatter.match + ')'
            @formatters.push formatter
        @formatterRegExp = new RegExp(pattern.join('|'), 'mg')
        return

    subWikify: (output, terminator) ->
        # Temporarily replace the output pointer
        terminatorMatch = undefined
        formatterMatch = undefined
        oldOutput = @output
        @output = output
        # Prepare the terminator RegExp
        terminatorRegExp = if terminator then new RegExp('(' + terminator + ')', 'mg') else null
        loop
            # Prepare the RegExp match positions
            @formatterRegExp.lastIndex = @nextMatch
            if terminatorRegExp
                terminatorRegExp.lastIndex = @nextMatch
            # Get the first matches
            formatterMatch = @formatterRegExp.exec(@source)
            terminatorMatch = if terminatorRegExp then terminatorRegExp.exec(@source) else null
            # Check for a terminator match
            if terminatorMatch and (!formatterMatch or terminatorMatch.index <= formatterMatch.index)
                # Output any text before the match
                if terminatorMatch.index > @nextMatch
                    @outputText @output, @nextMatch, terminatorMatch.index
                # Set the match parameters
                @matchStart = terminatorMatch.index
                @matchLength = terminatorMatch[1].length
                @matchText = terminatorMatch[1]
                @nextMatch = terminatorMatch.index + terminatorMatch[1].length
                # Restore the output pointer and exit
                @output = oldOutput
                return
            else if formatterMatch
                # Output any text before the match
                if formatterMatch.index > @nextMatch
                    @outputText @output, @nextMatch, formatterMatch.index
                # Set the match parameters
                @matchStart = formatterMatch.index
                @matchLength = formatterMatch[0].length
                @matchText = formatterMatch[0]
                @nextMatch = @formatterRegExp.lastIndex
                # Figure out which formatter matched
                matchingFormatter = -1
                t = 1
                while t < formatterMatch.length
                    if formatterMatch[t]
                        matchingFormatter = t - 1
                        break
                    t++
                # Call the formatter
                if matchingFormatter != -1
                    @formatters[matchingFormatter].handler this
            unless terminatorMatch or formatterMatch
                break
        # Output any text after the last match
        if @nextMatch < @source.length
            @outputText @output, @nextMatch, @source.length
            @nextMatch = @source.length
        # Restore the output pointer
        @output = oldOutput
        return

    outputText: (place, startPos, endPos) ->
        if place and isNode(place) and !(place.nodeType of FORBIDDEN_INSERT_NODETYPES)
            # VG - Sanity checking, or we get really weird errors.
            try
                insertText place, @source.substring(startPos, endPos)
            catch ex
                console.log 'outputText(place.nodeType=%d, startPos=%d, endPos=%d) = "%s"', place.nodeType, startPos, endPos, @source.substring(startPos, endPos)
                console.log ex
                #throw ex; And sometimes you just don't have a choice.
        return

    fullMatch: ->
        return @source.slice @matchStart, @source.indexOf('>>', @matchStart) + 2

    fullArgs: (includeName) ->
        source = @source.replace(/\u200c/g, ' ')
        endPos = @nextMatch - 2
        startPos = source.indexOf((if includeName then '<<' else ' '), @matchStart)
        if ! ~startPos or ! ~endPos or endPos <= startPos
            return ''
        return Wikifier.parse source.slice(startPos + (if includeName then 2 else 1), endPos).trim()

    @parse: (input) ->
        m = undefined
        re = undefined
        b = input
        found = []
        g = Wikifier.textPrimitives.unquoted
        # Extract all the variables, and set them to 0 if undefined.

        alter = (from, to) ->
            b = b.replace(new RegExp(from + g, 'gim'), to)
            return alter

        re = new RegExp(Wikifier.textPrimitives.variable + g, 'gi')
        while m = re.exec(input)
            if ! ~found.indexOf(m[0])
                # This deliberately contains a 'null or undefined' check
                b = m[0] + ' == null && (' + m[0] + ' = 0);' + b
                found.push m[0]
            alter(Wikifier.textPrimitives.variable, 'state.history[0].variables.$1')('[ \u0009]eq[ \u0009]', ' == ')('[ \u0009]neq[ \u0009]', ' != ')('[ \u0009]gt[ \u0009]', ' > ')('[ \u0009]gte[ \u0009]', ' >= ')('[ \u0009]lt[ \u0009]', ' < ')('[ \u0009]lte[ \u0009]', ' <= ')('[ \u0009]and[ \u0009]', ' && ')('[ \u0009]or[ \u0009]', ' || ')('[ \u0009]not[ \u0009]', ' ! ')('[ \u0009]is exactly[ \u0009]', ' === ')('[ \u0009]is[ \u0009]', ' == ') '[ \u0009]to[ \u0009]', ' = '
        return b

Wikifier.formatHelpers =
  charFormatHelper: (a) ->
    b = insertElement(a.output, @element)
    a.subWikify b, @terminator
    return
  inlineCssHelper: (w) ->
    s = undefined
    v = undefined
    lookaheadMatch = undefined
    gotMatch = undefined
    styles = []
    lookahead = Wikifier.styleByCharFormatter.lookahead
    lookaheadRegExp = new RegExp(lookahead, 'mg')
    hadStyle = false

    unDash = (str) ->
      `var s`
      s = str.split('-')
      if s.length > 1
        t = 1
        while t < s.length
          s[t] = s[t].substr(0, 1).toUpperCase() + s[t].substr(1)
          t++
      s.join ''

    styles.className = ''
    loop
      lookaheadRegExp.lastIndex = w.nextMatch
      lookaheadMatch = lookaheadRegExp.exec(w.source)
      gotMatch = lookaheadMatch and lookaheadMatch.index == w.nextMatch
      if gotMatch
        hadStyle = true
        if lookaheadMatch[5]
          styles.className += lookaheadMatch[5].replace(/\./g, ' ') + ' '
        else if lookaheadMatch[1]
          s = unDash(lookaheadMatch[1])
          v = lookaheadMatch[2]
        else
          s = unDash(lookaheadMatch[3])
          v = lookaheadMatch[4]
        switch s
          when 'bgcolor'
            s = 'backgroundColor'
          when 'float'
            s = 'cssFloat'
        styles.push
          style: s
          value: v
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
      unless gotMatch
        break
    styles
  monospacedByLineHelper: (w) ->
    lookaheadRegExp = new RegExp(@lookahead, 'mg')
    lookaheadRegExp.lastIndex = w.matchStart
    lookaheadMatch = lookaheadRegExp.exec(w.source)
    if lookaheadMatch and lookaheadMatch.index == w.matchStart
      insertElement w.output, 'pre', null, null, lookaheadMatch[1]
      w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
    return
Wikifier.formatters = [
  {
    name: 'table'
    match: '^\\|(?:[^\\n]*)\\|(?:[fhc]?)$'
    lookahead: '^\\|([^\\n]*)\\|([fhc]?)$'
    rowTerminator: '\\|(?:[fhc]?)$\\n?'
    cellPattern: '(?:\\|([^\\n\\|]*)\\|)|(\\|[fhc]?$\\n?)'
    cellTerminator: '(?:\\x20*)\\|'
    rowTypes:
      'c': 'caption'
      'h': 'thead'
      '': 'tbody'
      'f': 'tfoot'
    handler: (w) ->
      rowContainer = undefined
      rowElement = undefined
      lookaheadMatch = undefined
      matched = undefined
      table = insertElement(w.output, 'table')
      lookaheadRegExp = new RegExp(@lookahead, 'mg')
      currRowType = null
      nextRowType = undefined
      prevColumns = []
      rowCount = 0
      w.nextMatch = w.matchStart
      loop
        lookaheadRegExp.lastIndex = w.nextMatch
        lookaheadMatch = lookaheadRegExp.exec(w.source)
        matched = lookaheadMatch and lookaheadMatch.index == w.nextMatch
        if matched
          nextRowType = lookaheadMatch[2]
          if nextRowType != currRowType
            rowContainer = insertElement(table, @rowTypes[nextRowType])
          currRowType = nextRowType
          if currRowType == 'c'
            if rowCount == 0
              rowContainer.setAttribute 'align', 'top'
            else
              rowContainer.setAttribute 'align', 'bottom'
            w.nextMatch = w.nextMatch + 1
            w.subWikify rowContainer, @rowTerminator
          else
            rowElement = insertElement(rowContainer, 'tr')
            @rowHandler w, rowElement, prevColumns
          rowCount++
        unless matched
          break
      return
    rowHandler: (w, e, prevColumns) ->
      cellMatch = undefined
      matched = undefined
      col = 0
      currColCount = 1
      cellRegExp = new RegExp(@cellPattern, 'mg')
      loop
        cellRegExp.lastIndex = w.nextMatch
        cellMatch = cellRegExp.exec(w.source)
        matched = cellMatch and cellMatch.index == w.nextMatch
        if matched
          if cellMatch[1] == '~'
            last = prevColumns[col]
            if last
              last.rowCount++
              last.element.setAttribute 'rowSpan', last.rowCount
              last.element.setAttribute 'rowspan', last.rowCount
              last.element.valign = 'center'
            w.nextMatch = cellMatch.index + cellMatch[0].length - 1
          else if cellMatch[1] == '>'
            currColCount++
            w.nextMatch = cellMatch.index + cellMatch[0].length - 1
          else if cellMatch[2]
            w.nextMatch = cellMatch.index + cellMatch[0].length
            break
          else
            spaceLeft = false
            spaceRight = false
            lastColCount = undefined
            lastColElement = undefined
            styles = undefined
            cell = undefined
            t = undefined
            w.nextMatch++
            styles = Wikifier.formatHelpers.inlineCssHelper(w)
            while w.source.substr(w.nextMatch, 1) == ' '
              spaceLeft = true
              w.nextMatch++
            if w.source.substr(w.nextMatch, 1) == '!'
              cell = insertElement(e, 'th')
              w.nextMatch++
            else
              cell = insertElement(e, 'td')
            prevColumns[col] =
              rowCount: 1
              element: cell
            lastColCount = 1
            lastColElement = cell
            if currColCount > 1
              cell.setAttribute 'colSpan', currColCount
              cell.setAttribute 'colspan', currColCount
              currColCount = 1
            t = 0
            while t < styles.length
              cell.style[styles[t].style] = styles[t].value
              t++
            w.subWikify cell, @cellTerminator
            if w.matchText.substr(w.matchText.length - 2, 1) == ' '
              spaceRight = true
            if spaceLeft and spaceRight
              cell.align = 'center'
            else if spaceLeft
              cell.align = 'right'
            else if spaceRight
              cell.align = 'left'
            w.nextMatch = w.nextMatch - 1
          col++
        unless matched
          break
      return

  }
  {
    name: 'rule'
    match: '^----$\\n?'
    handler: (w) ->
      insertElement w.output, 'hr'
      return

  }
  {
    name: 'emdash'
    match: '--'
    becomes: String.fromCharCode(8212)
    handler: (a) ->
      insertElement(a.output, 'span', null, 'char', @becomes).setAttribute 'data-char', 'emdash'
      return

  }
  {
    name: 'heading'
    match: '^!{1,5}'
    terminator: '\\n'
    handler: (w) ->
      e = insertElement(w.output, 'h' + w.matchLength)
      w.subWikify e, @terminator
      return

  }
  {
    name: 'monospacedByLine'
    match: '^\\{\\{\\{\\n'
    lookahead: '^\\{\\{\\{\\n((?:^[^\\n]*\\n)+?)(^\\}\\}\\}$\\n?)'
    handler: Wikifier.formatHelpers.monospacedByLineHelper
  }
  {
    name: 'quoteByBlock'
    match: '^<<<\\n'
    terminator: '^<<<\\n'
    handler: (w) ->
      e = insertElement(w.output, 'blockquote')
      w.subWikify e, @terminator
      return

  }
  {
    name: 'list'
    match: '^(?:(?:\\*+)|(?:#+))'
    lookahead: '^(?:(\\*+)|(#+))'
    terminator: '\\n'
    handler: (w) ->
      newType = undefined
      newLevel = undefined
      t = undefined
      len = undefined
      bulletType = undefined
      lookaheadMatch = undefined
      matched = undefined
      lookaheadRegExp = new RegExp(@lookahead, 'mg')
      placeStack = [ w.output ]
      currType = null
      currLevel = 0
      w.nextMatch = w.matchStart
      loop
        lookaheadRegExp.lastIndex = w.nextMatch
        lookaheadMatch = lookaheadRegExp.exec(w.source)
        matched = lookaheadMatch and lookaheadMatch.index == w.nextMatch
        if matched
          newLevel = lookaheadMatch[0].length
          if lookaheadMatch[1]
            bulletType = lookaheadMatch[1].slice(-1)
            newType = 'ul'
          else if lookaheadMatch[2]
            newType = 'ol'
          w.nextMatch += newLevel
          if newLevel > currLevel
            t = currLevel
            while t < newLevel
              placeStack.push insertElement(placeStack[placeStack.length - 1], newType)
              t++
          else if newLevel < currLevel
            t = currLevel
            while t > newLevel
              placeStack.pop()
              t--
          else if newLevel == currLevel and newType != currType
            placeStack.pop()
            placeStack.push insertElement(placeStack[placeStack.length - 1], newType)
          currLevel = newLevel
          currType = newType
          t = insertElement(placeStack[placeStack.length - 1], 'li')
          # Currently unused
          if bulletType and bulletType != '*'
            t.setAttribute 'data-bullet', bulletType
          w.subWikify t, @terminator
        unless matched
          break
      return

  }
  Wikifier.urlFormatter =
    name: 'urlLink'
    match: '(?:https?|mailto|javascript|ftp|data):[^\\s\'"]+(?:/|\\b)'
    handler: (w) ->
      e = Wikifier.createExternalLink(w.output, w.matchText)
      w.outputText e, w.matchStart, w.nextMatch
      return
  Wikifier.linkFormatter =
    name: 'prettyLink'
    match: '\\[\\['
    lookahead: '\\[\\[([^\\|]*?)(?:\\|(.*?))?\\](?:\\[(.*?)])?\\]'
    makeInternalOrExternal: (out, title, callback, type) ->
      if title and !tale.has(title) and (title.match(Wikifier.urlFormatter.match, 'g') or ~title.search(/[\.\\\/#]/))
        Wikifier.createExternalLink out, title, callback, type
      else
        Wikifier.createInternalLink out, title, callback, type
    makeCallback: (code, callback, fullmatch) ->
      ->
        # Codegen debugging - VG
        if DEBUG_CODEGEN
          console.log 'Wikifier.linkFormatter.makeCallback(): Full Match', fullmatch, code, Wikifier.parse(code)
        macros.set.run null, Wikifier.parse(code), null, code
        typeof callback == 'function' and callback.call(this)
        return
    makeLink: (out, match, callback2, type) ->
      link = undefined
      title = undefined
      callback = undefined
      # Codegen debugging - VG
      if DEBUG_CODEGEN
        console.log 'Wikifier.linkFormatter.makeLink():', match
      if match[3]
        # Code
        callback = @makeCallback(match[3], callback2, match)
      else
        typeof callback2 == 'function' and (callback = callback2)
      title = Wikifier.parsePassageTitle(match[2] or match[1])
      link = @makeInternalOrExternal(out, title, callback, type)
      setPageElement link, null, if match[2] then match[1] else title
      link
    handler: (w) ->
      lookaheadRegExp = new RegExp(@lookahead, 'mg')
      lookaheadRegExp.lastIndex = w.matchStart
      lookaheadMatch = lookaheadRegExp.exec(w.source)
      if lookaheadMatch and lookaheadMatch.index == w.matchStart
        @makeLink w.output, lookaheadMatch
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
      return
  Wikifier.imageFormatter =
    name: 'image'
    match: '\\[(?:[<]{0,1})(?:[>]{0,1})[Ii][Mm][Gg]\\['
    lookahead: '\\[([<]?)(>?)img\\[(?:([^\\|\\]]+)\\|)?([^\\[\\]\\|]+)\\](?:\\[([^\\]]*)\\]?)?(\\])'
    importedImage: (img, passageName) ->
      imgPassages = undefined
      imgname = undefined
      # Try to parse it as a variable
      try
        imgname = internalEval(Wikifier.parse(passageName))
      catch e
      if !imgname
        imgname = passageName
      # Base64 passage transclusion
      imgPassages = tale.lookup('tags', 'Twine.image')
      j = 0
      while j < imgPassages.length
        if imgPassages[j].title == imgname
          img.src = imgPassages[j].text
          return
        j++
      img.src = img.src or imgname
      return
    handler: (w) ->
      e = undefined
      img = undefined
      j = undefined
      lookaheadMatch = undefined
      lookaheadRegExp = new RegExp(@lookahead, 'mig')
      lookaheadRegExp.lastIndex = w.matchStart
      lookaheadMatch = lookaheadRegExp.exec(w.source)
      if lookaheadMatch and lookaheadMatch.index == w.matchStart
        e = w.output
        title = Wikifier.parsePassageTitle(lookaheadMatch[5])
        if title
          e = Wikifier.linkFormatter.makeInternalOrExternal(w.output, title)
        img = insertElement(e, 'img')
        if lookaheadMatch[1]
          img.align = 'left'
        else if lookaheadMatch[2]
          img.align = 'right'
        if lookaheadMatch[3]
          img.title = lookaheadMatch[3]
        # Setup the image if it's referencing an image passage.
        @importedImage img, lookaheadMatch[4]
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
      return
  {
    name: 'macro'
    match: '<<'
    lookahead: /<<([^>\s]+)(?:\s*)((?:\\.|'(?:[^'\\]*\\.)*[^'\\]*'|"(?:[^"\\]*\\.)*[^"\\]*"|[^'"\\>]|>(?!>))*)>>/mg
    handler: (w) ->
      lookaheadRegExp = new RegExp(@lookahead)
      lookaheadRegExp.lastIndex = w.matchStart
      lookaheadMatch = lookaheadRegExp.exec(w.source.replace(/\u200c/g, '\n'))
      if lookaheadMatch and lookaheadMatch.index == w.matchStart and lookaheadMatch[1]
        params = lookaheadMatch[2].readMacroParams()
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
        name = lookaheadMatch[1]
        try
          macro = macros[name]
          if macro and typeof macro == 'object' and macro.handler
            macro.handler w.output, name, params, w
          else if name[0] == '$'
            macros.print.handler w.output, name, [ name ].concat(params), w
          else if tale.has(name)
            macros.display.handler w.output, name, [ name ].concat(params), w
          else
            throwError w.output, 'No macro or passage called "' + name + '"', w.fullMatch()
        catch e
          throwError w.output, 'Error executing macro ' + name + ': ' + e.toString(), w.fullMatch()
      return

  }
  {
    name: 'html'
    match: '<html>'
    lookahead: '<html>((?:.|\\n)*?)</html>'
    handler: (w) ->
      lookaheadRegExp = new RegExp(@lookahead, 'mg')
      lookaheadRegExp.lastIndex = w.matchStart
      lookaheadMatch = lookaheadRegExp.exec(w.source)
      if lookaheadMatch and lookaheadMatch.index == w.matchStart
        e = insertElement(w.output, 'span')
        e.innerHTML = lookaheadMatch[1]
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
      return

  }
  {
    name: 'commentByBlock'
    match: '/%'
    lookahead: '/%((?:.|\\n)*?)%/'
    handler: (w) ->
      lookaheadRegExp = new RegExp(@lookahead, 'mg')
      lookaheadRegExp.lastIndex = w.matchStart
      lookaheadMatch = lookaheadRegExp.exec(w.source)
      if lookaheadMatch and lookaheadMatch.index == w.matchStart
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
      return

  }
  {
    name: 'boldByChar'
    match: '\'\''
    terminator: '\'\''
    element: 'strong'
    handler: Wikifier.formatHelpers.charFormatHelper
  }
  {
    name: 'strikeByChar'
    match: '=='
    terminator: '=='
    element: 'strike'
    handler: Wikifier.formatHelpers.charFormatHelper
  }
  {
    name: 'underlineByChar'
    match: '__'
    terminator: '__'
    element: 'u'
    handler: Wikifier.formatHelpers.charFormatHelper
  }
  {
    name: 'italicByChar'
    match: '//'
    terminator: '//'
    element: 'em'
    handler: Wikifier.formatHelpers.charFormatHelper
  }
  {
    name: 'subscriptByChar'
    match: '~~'
    terminator: '~~'
    element: 'sub'
    handler: Wikifier.formatHelpers.charFormatHelper
  }
  {
    name: 'superscriptByChar'
    match: '\\^\\^'
    terminator: '\\^\\^'
    element: 'sup'
    handler: Wikifier.formatHelpers.charFormatHelper
  }
  {
    name: 'monospacedByChar'
    match: '\\{\\{\\{'
    lookahead: '\\{\\{\\{((?:.|\\n)*?)\\}\\}\\}'
    handler: (w) ->
      lookaheadRegExp = new RegExp(@lookahead, 'mg')
      lookaheadRegExp.lastIndex = w.matchStart
      lookaheadMatch = lookaheadRegExp.exec(w.source)
      if lookaheadMatch and lookaheadMatch.index == w.matchStart
        e = insertElement(w.output, 'code', null, null, lookaheadMatch[1])
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
      return

  }
  Wikifier.styleByCharFormatter =
    name: 'styleByChar'
    match: '@@'
    terminator: '@@'
    lookahead: '(?:([^\\(@]+)\\(([^\\)\\|\\n]+)(?:\\):))|(?:([^\\.:@]+):([^;\\|\\n]+);)|(?:\\.([^;\\|\\n]+);)'
    handler: (w) ->
      e = insertElement(w.output, 'span', null, null, null)
      styles = Wikifier.formatHelpers.inlineCssHelper(w)
      if styles.length == 0
        e.className = 'marked'
      else
        t = 0
        while t < styles.length
          e.style[styles[t].style] = styles[t].value
          t++
        if typeof styles.className == 'string'
          e.className = styles.className
      w.subWikify e, @terminator
      return
  {
    name: 'lineBreak'
    match: '\\n'
    handler: (w) ->
      insertElement w.output, 'br'
      return

  }
  {
    name: 'continuedLine'
    match: '\\\\\\s*?\\n'
    handler: (a) ->
      a.nextMatch = a.matchStart + 2
      return

  }
  {
    name: 'htmlCharacterReference'
    match: '(?:(?:&#?[a-zA-Z0-9]{2,8};|.)(?:&#?(?:x0*(?:3[0-6][0-9a-fA-F]|1D[c-fC-F][0-9a-fA-F]|20[d-fD-F][0-9a-fA-F]|FE2[0-9a-fA-F])|0*(?:76[89]|7[7-9][0-9]|8[0-7][0-9]|761[6-9]|76[2-7][0-9]|84[0-3][0-9]|844[0-7]|6505[6-9]|6506[0-9]|6507[0-1]));)+|&#?[a-zA-Z0-9]{2,8};)'
    handler: (w) ->
      el = document.createElement('div')
      el.innerHTML = w.matchText
      insertText w.output, el.textContent
      return

  }
  {
    name: 'htmltag'
    match: '<(?:\\/?[\\w\\-]+|[\\w\\-]+(?:(?:\\s+[\\w\\-]+(?:\\s*=\\s*(?:\".*?\"|\'.*?\'|[^\'\">\\s]+))?)+\\s*|\\s*)\\/?)>'
    tagname: '<(\\w+)'
    voids: [
      'area'
      'base'
      'br'
      'col'
      'embed'
      'hr'
      'img'
      'input'
      'link'
      'meta'
      'param'
      'source'
      'track'
      'wbr'
    ]
    tableElems: [
      'table'
      'thead'
      'tbody'
      'tfoot'
      'th'
      'tr'
      'td'
      'colgroup'
      'col'
      'caption'
      'figcaption'
    ]
    cleanupTables: (e) ->
      i = undefined
      name = undefined
      elems = [].slice.call(e.children)
      # Remove non-table child elements that aren't children of <td>s
      i = 0
      while i < elems.length
        if elems[i].tagName
          name = elems[i].tagName.toLowerCase()
          if @tableElems.indexOf(name) == -1
            elems[i].outerHTML = ''
          else if [
              'col'
              'caption'
              'figcaption'
              'td'
              'th'
            ].indexOf(name) == -1
            @cleanupTables.call this, elems[i]
        i++
      return
    handler: (a) ->
      tmp = undefined
      passage = undefined
      setter = undefined
      e = undefined
      isvoid = undefined
      isstyle = undefined
      lookaheadRegExp = undefined
      lookaheadMatch = undefined
      lookahead = undefined
      re = new RegExp(@tagname).exec(a.matchText)
      tn = re and re[1] and re[1].toLowerCase()
      if tn and tn != 'html'
        lookahead = '<\\/\\s*' + tn + '\\s*>'
        isvoid = @voids.indexOf(tn) != -1
        isstyle = tn == 'style' or tn == 'script'
        lookaheadRegExp = new RegExp(lookahead, 'mg')
        lookaheadRegExp.lastIndex = a.matchStart
        lookaheadMatch = lookaheadRegExp.exec(a.source)
        if lookaheadMatch or isvoid
          if isstyle
            e = document.createElement(tn)
            e.type = 'text/css'
            # IE8 compatibility
            tmp = a.source.slice(a.nextMatch, lookaheadMatch.index)
            if e.styleSheet then (e.styleSheet.cssText = tmp) else (e.innerHTML = tmp)
            a.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length
          else
            # Creating a loose <tr> element creates it wrapped inside a <tbody> element.
            e = document.createElement(a.output.tagName)
            e.innerHTML = a.matchText
            while e.firstChild
              e = e.firstChild
            if !isvoid
              a.subWikify e, lookahead
          if !e or !e.tagName
            return
          if e.tagName.toLowerCase() == 'table'
            @cleanupTables.call this, e
          # Special data-setter attribute
          if setter = e.getAttribute('data-setter')
            setter = Wikifier.linkFormatter.makeCallback(setter)
          # Special data-passage attribute
          if passage = e.getAttribute('data-passage')
            if tn != 'img'
              addClickHandler e, Wikifier.linkFunction(Wikifier.parsePassageTitle(passage), e, setter)
              if tn == 'area' or tn == 'a'
                e.setAttribute 'href', 'javascript:;'
            else
              Wikifier.imageFormatter.importedImage e, passage
          a.output.appendChild e
        else
          throwError a.output, 'HTML tag \'' + tn + '\' wasn\'t closed.', a.matchText
      return

  }
]
# Optional - included if the ".char" selector appears anywhere in the story.
Wikifier.charSpanFormatter =
  name: 'char'
  match: '[^\n]'
  handler: (a) ->
    # Line breaks do NOT get their own charspans
    insertElement(a.output, 'span', null, 'char', a.matchText).setAttribute 'data-char', if a.matchText == ' ' then 'space' else if a.matchText == '\u0009' then 'tab' else a.matchText
    return

Wikifier.parsePassageTitle = (title) ->
  if title and !tale.has(title)
    try
      title = (internalEval(@parse(title)) or title) + ''
    catch e
  title

Wikifier.linkFunction = (title, el, callback) ->
  #console.log("Wikifier.linkFunction('"+title+"', ...)");
  ->
    if state.rewindTo
      passage = findPassageParent(el)
      if passage and passage.parentNode.lastChild != passage
        state.rewindTo passage, true
    if title
      state.display title, el, null, callback
    else if typeof callback == 'function'
      callback()
    return

Wikifier.createInternalLink = (place, title, callback, type) ->
  tag = if type == 'button' then 'button' else 'a'
  suffix = if type == 'button' then 'Button' else 'Link'
  el = insertElement(place, tag)
  el.tabIndex = 0
  if tale.has(title)
    el.className = 'internal' + suffix
    #if visited(title)
    #  el.className += ' visited' + suffix
  else
    el.className = 'broken' + suffix
  addClickHandler el, Wikifier.linkFunction(title, el, callback)
  if place
    place.appendChild el
  el

Wikifier.createExternalLink = (place, url, callback, type) ->
  tag = if type == 'button' then 'button' else 'a'
  el = insertElement(place, tag)
  el.href = url
  el.tabIndex = 0
  el.className = 'external' + (if type == 'button' then 'Button' else 'Link')
  el.target = '_blank'
  if typeof callback == 'function'
    addClickHandler el, callback
  if place
    place.appendChild el
  el

# ---
# generated by js2coffee 2.2.0

# Make publically accessible.
window.Wikifier = Wikifier
