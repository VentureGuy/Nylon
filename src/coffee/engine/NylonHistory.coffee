# @requires: engine/_funcsAndPolyfills.coffee
###
# History object
#
# Modified to be a bit more configurable.
#
# CONFIG:
#  max-length: 5
###
class NylonHistory
    constructor: (@Config) ->
        @history = [
            passage: null
            variables: {}
        ]

        # Unique identifier for this game session
        @id = (new Date).getTime() + ''

        # URL of the bookmark link
        @hash = ''

        # Nylon - Max length of @history
        @MaxLength = @Config['max-length'] or 5

    @getter 'title', ->
        @history[0].title
    #@property 'title', ->
    #    @history[0].title


    ###
    Fuck this.
    @encodeHistory: (hIndex, noVars) ->
        ret = '.'
        vars = undefined
        type = undefined
        hist = state.history[hIndex]
        variables = if state.history[hIndex + 1] then delta(state.history[hIndex + 1].variables, hist.variables) else hist.variables

        vtob = (val) ->
            try
                return window.btoa(unescape(encodeURIComponent(JSON.stringify(decompile(val)))))
            catch e
                return '0'
            return
        if !hist.passage or hist.passage.id == null
            return ''
        ret += hist.passage.id.toString(36)
        if noVars
            return ret
        for vars of variables
            type = typeof d[vars]
            if type != 'undefined'
                ret += '$' + vtob(vars) + ',' + vtob(d[vars])
        for vars of hist.linkVars
            type = typeof hist.linkVars[vars]
            if type != 'function' and type != 'undefined'
                ret += '[' + vtob(vars) + ',' + vtob(hist.linkVars[vars])
        return ret

    @decodeHistory: (str, prev, done_callback) ->
        name = undefined
        splits = undefined
        variable = undefined
        c = undefined
        d = undefined
        ret = variables: clone(prev.variables) or {}
        match = /([a-z0-9]+)((?:\$[A-Za-z0-9\+\/=]+,[A-Za-z0-9\+\/=]+)*)((?:\[[A-Za-z0-9\+\/=]+,[A-Za-z0-9\+\/=]+)*)/g.exec(str)

        btov = (str) ->
            try
                return recompile(JSON.parse(decodeURIComponent(escape(window.atob(str)))))
            catch e
                return 0
            return

        if match
            name = parseInt(match[1], 36)
            if !tale.has(name)
                return false
            if match[2]
                ret.variables or (ret.variables = {})
                splits = match[2].split('$')
                c = 0
                while c < splits.length
                    variable = splits[c].split(',')
                    d = btov(variable[0])
                    if d
                        ret.variables[d] = btov(variable[1])
                    c++
            if match[3]
                ret.linkVars or (ret.linkVars = {})
                splits = match[3].split('[')
                c = 0
                while c < splits.length
                    variable = splits[c].split(',')
                    d = btov(variable[0])
                    if d
                        ret.linkVars[d] = btov(variable[1])
                    c++
            tale.get name, (page) =>
                ret.passage = page
                done_callback(ret)
                return
            return
        return
    ###

    save: ->
        hist = undefined
        b = undefined
        a = ''
        b = @history.length - 1
        while b >= 0
            hist = @history[b]
            if !hist
                break
            #a += NylonHistory.encodeHistory(b)
            b--
        return '#' + a

    restore: ->
        a = undefined
        b = undefined
        c = undefined
        vars = undefined
        try
            if !window.location.hash or window.location.hash == '#'
                if testplay and testplay != 'Start'
                    if tale.has('StoryInit')
                        new Wikifier(insertElement(null, 'span'), tale.get('StoryInit').text)
                    @display testplay, null, 'quietly'
                    return true
                return false
            if window.location.hash.substr(0, 2) == '#!'
                c = window.location.hash.substr(2).split('_').join(' ')
                @display c, null, 'quietly'
                return true
            a = window.location.hash.replace('#', '').split('.')
            b = 0
            while b < a.length
                vars = NylonHistory.decodeHistory(a[b], vars or {})
                if vars
                    if b == a.length - 1
                        vars.variables = clone(@history[0].variables)
                        for c of @history[0].linkVars
                            vars.variables[c] = clone(@history[0].linkVars[c])
                        @history.unshift vars
                        @display vars.passage.title, null, 'back'
                    else
                        @history.unshift vars
                b++
            return true
        catch d
            return false
        return

    saveVariables: (c, el, callback) ->
        if typeof callback == 'function'
            callback.call el

        @history.push
            passage: c
            variables: clone(@history[0].variables)

        # Nylon: Snip off excessive frames.
        while @history.length > Math.max(1, @MaxLength)
            @history.pop()

        return

    restart: ->
        if typeof window.history.replaceState == 'function'
            typeof @pushState == 'function' and @pushState(true, window.location.href.replace(/#.*$/, ''))
            window.location.reload()
        else
            window.location.hash = ''
        return



# ---
# generated by js2coffee 2.2.0
