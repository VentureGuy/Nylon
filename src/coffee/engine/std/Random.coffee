
###
# A random integer function
# 1 argument: random int from 0 to a inclusive
# 2 arguments: random int from a to b inclusive (order irrelevant)
###
window.random = (a, b) ->
  from = undefined
  to = undefined
  if !b
    from = 0
    to = a
  else
    from = Math.min(a, b)
    to = Math.max(a, b)
  to += 1
  return ~~(Math.random() * (to - from)) + from

# https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
window.random.float = (min, max) ->
  return Math.random() * (max - min) + min

###
# The maximum is inclusive and the minimum is inclusive
# https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
###
window.random.int = (a, b) ->
  min = undefined
  max = undefined
  if !b
    min = 0
    max = a
  else
    [min, max] = [a, b]
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor Math.random() * (max - min + 1) + min

###
# Random item from list
###
window.random.choice = (items) ->
  return items[items.length * Math.random() | 0]

window.either = window.random.choice
