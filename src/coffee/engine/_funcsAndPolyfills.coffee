###*
# Twine for Nylon v8.4.2019
###

# @requires: engine/_globals.coffee

# Set to debug code generation
# (macros.button.callback, macros.set.run, Wikifier.linkFormatter.makeLink, etc)
DEBUG_CODEGEN = false
# Prints warnings in codegen.
PRINT_CODEGEN_WARNINGS = true
TWINE_CATCH_ERRORS = true

###
**
** Basic utility functions
**
###

ENodeTypes =
  ELEMENT: 1
  ATTR: 2
  TEXT: 3
  CDATASECTION: 4
  ENTITYREFERENCE: 5
  ENTITY: 6
  PROCESSINGINSTRUCTION: 7
  COMMENT: 8
  DOCUMENT: 9
  DOCUMENTTYPE: 10
  DOCUMENTFRAGMENT: 11
  NOTATION: 12
FORBIDDEN_INSERT_NODETYPES = []

nylonErrorHandler = (place, parser, exception, message) ->
  if parser
    console.error 'Nylon caught an exception while parsing ' + parser.fullMatch() + '.'
  else
    console.error 'Nylon caught an exception (parser not present).'
  if message
    console.error message
  console.error exception
  return

# FROM TWINE 2

### Used to eval within Twine, but outside the context of a particular function ###
internalEval = (s) ->
  # eval("{x:1,y:1}") fails miserably unless the obj. literal
  # is not the first expression in the line (hence, "0,").
  eval '0,' + s

window.internalEval = internalEval

###
Returns a deep copy of the given object.

NOTE: 1. `clone()` does not clone functions, however, since function definitions are
         immutable, the only issues are with expando properties and scope.  The former
         really should not be done.  The latter is problematic either way (damned if
         you do, damned if you don't).
      2. `clone()` does not maintain referential relationships (e.g. multiple references
         to the same object will, post-cloning, refer to different equivalent objects;
         i.e. each reference will get its own clone of the original object).
###

clone = (orig) ->

  ###
  	Immediately return non-objects.
  ###

  if typeof orig != 'object' or orig == null
    # lazy equality for null
    return orig

  ###
  	Honor native clone methods.
  ###

  if typeof orig.clone == 'function'
    return orig.clone(true)
  else if orig.nodeType and typeof orig.cloneNode == 'function'
    return orig.cloneNode(true)

  ###
  	Create a copy of the original object.

  	NOTE: 1. Each non-generic object that we wish to support must receive a special
  	         case below.
  	      2. Since we're using the `instanceof` operator to identify special cases,
  	         this may fail to properly identify such cases if the author engages in
  	         cross-<iframe> object manipulation.  The solution to this problem would
  	         be for the author to pass messages between the frames, rather than doing
  	         direct cross-frame object manipulation.  That is, in fact, what they should
  	         be doing in the first place.
  	      3. We cannot use `Object.prototype.toString.call(orig)` to solve #2 because
  	         the shims for `Map` and `Set` return `[object Object]` rather than
  	         `[object Map]` and `[object Set]`.
  ###

  copy = undefined
  if Array.isArray(orig)
    # considering #2, `orig instanceof Array` might be more appropriate
    copy = []
  else if orig instanceof Date
    copy = new Date(orig.getTime())
  else if orig instanceof Map
    copy = new Map
    orig.forEach (val, key) ->
      copy.set key, clone(val)
      return
  else if orig instanceof RegExp
    copy = new RegExp(orig)
  else if orig instanceof Set
    copy = new Set
    orig.forEach (val) ->
      copy.add clone(val)
      return
  else
    # unknown or generic object
    # Try to ensure that the returned object has the same prototype as the original.
    copy = Object.create(Object.getPrototypeOf(orig))

  ###
  	Duplicate the original object's own enumerable properties, which will include expando
  	properties on non-generic objects.

  	NOTE: This does not preserve ES5 property attributes.  Neither does the delta coding
  	      or serialization code, however, so it's not really an issue at the moment.
  ###

  Object.keys(orig).forEach (name) ->
    copy[name] = clone(orig[name])
    return
  copy

# Nylon
# Used in sanity checking in insertElement/insertText
# https://stackoverflow.com/a/384380
#/////////////////////////////////////
#Returns true if it is a DOM node

isNode = (o) ->
  if typeof Node == 'object' then o instanceof Node else o and typeof o == 'object' and typeof o.nodeType == 'number' and typeof o.nodeName == 'string'

#Returns true if it is a DOM element

isElement = (o) ->
  if typeof HTMLElement == 'object' then o instanceof HTMLElement else o and typeof o == 'object' and o != null and o.nodeType == 1 and typeof o.nodeName == 'string'

isCyclic = (obj) ->
  properties = []
  ((obj) ->
    key = undefined
    i = undefined
    ownProps = []
    if obj and typeof obj == 'object'
      if properties.indexOf(obj) > -1
        return true
      properties.push obj
      for key of obj
        `key = key`
        if Object::hasOwnProperty.call(obj, key) and recurse(obj[key])
          return true
    false
  ) obj

insertElement = (a, d, f, c, e) ->
  # a = parent
  # d = tag
  # f = id
  # c = class
  # e = text
  b = document.createElement(d)
  try
    if f
      b.id = f
    if c
      b.className = c
    if e
      insertText b, e
    if a
      a.appendChild b
  catch ex
    console.log 'Failed insertElement(parent=%s, tag="%s", id="%s", class="%s", text="%s")', a, d, f, c, e
    console.log ex
    if !TWINE_CATCH_ERRORS
      throw ex
  b

addClickHandler = (el, fn) ->
  if el.addEventListener
    el.addEventListener 'click', fn
  else if el.attachEvent
    el.attachEvent 'onclick', fn
  return

insertText = (a, b) ->
  try
    return a.appendChild(document.createTextNode(b))
  catch ex
    console.log 'Failed insertText(parent=%s, text="%s")', a, b
    throw ex
  return

removeChildren = (a) ->
  while a.hasChildNodes()
    a.removeChild a.firstChild
  return

findPassageParent = (el) ->
  while el and el != document.body and ! ~el.className.indexOf('passage')
    el = el.parentNode
  if el == document.body then null else el

setPageElement = (c, b, a) ->
  place = undefined
  if place = (if typeof c == 'string' then document.getElementById(c) else c)
    removeChildren place
    if tale.has(b)
      tale.get b, (page) =>
        new Wikifier place, page.processText()
    else
      new Wikifier(place, a)
  return

scrollWindowTo = (e, margin) ->
  d = if window.scrollY then window.scrollY else document.documentElement.scrollTop
  m = if window.innerHeight then window.innerHeight else document.documentElement.clientHeight
  g = k(e)
  j = if d > g then -1 else 1
  b = 0
  c = Math.abs(d - g)

  h = ->
    b += 0.1
    window.scrollTo 0, d + j * c * Math.easeInOut(b)
    if b >= 1
      window.clearInterval scrollWindowInterval
    return

  k = (o) ->
    `var h`
    p = a(o)
    h = o.offsetHeight
    n = d + m
    p = Math.min(Math.max(p + (margin or 0) * (if p < d then -1 else 1), 0), n)
    if p < d
      p
    else
      if p + h > n
        if h < m
          p - (m - h) + 20
        else
          p
      else
        p

  a = (l) ->
    `var m`
    m = 0
    while l.offsetParent
      m += l.offsetTop
      l = l.offsetParent
    m

  scrollWindowInterval and window.clearInterval(scrollWindowInterval)
  if c
    scrollWindowInterval = window.setInterval(h, 25)
  return

# Returns an object containing the properties in neu which differ from old.

delta = (old, neu) ->
  vars = undefined
  ret = {}
  if old and neu
    for vars of neu
      `vars = vars`
      if neu[vars] != old[vars]
        ret[vars] = neu[vars]
  ret

# Convert various exotic objects into JSON-serialisable plain objects.

decompile = (val) ->
  i = undefined
  ret = undefined
  if typeof val != 'object' and typeof val != 'function' or !val
    return val
  else if val.decompile != undefined and typeof val.decompile == 'function'
    #console.log("Decompiling %s...", val.constructor.name);
    return val.decompile()
  else if val instanceof Passage
    return { '[[Passage]]': val.id }
  else if Array.isArray(val)
    ret = []
  else
    ret = {}
  # Deep-copy own properties
  for i of val
    `i = i`
    if Object::hasOwnProperty.call(val, i) and !isCyclic(val[i])
      ret[i] = decompile(val[i])
  # Functions: store the decompiled function body as a property
  # arbitrarily called "[[Call]]".
  if typeof val == 'function' or val instanceof RegExp
    try
      # Check if it can be recompiled (i.e. isn't a bound or native)
      internalEval val + ''
      ret['[[Call]]'] = val + ''
    catch e
      # Silently fail
      ret['[[Call]]'] = 'function(){}'
  ret

# Reverses the effects of decompile()
# Takes recently JSON.parse()d objects and makes them exotic again.

recompile = (val) ->
  i = undefined
  ret = val
  #console.log("Recompile %s", typeof val['_type']);
  if val and typeof val == 'object'
    if typeof val['_type'] == 'string'
      #console.log("Recompiling %s...", val["_type"]);
      ret = new (val['_type'])
      ret.deserialize val
      return ret

    # Passages
    #if typeof val['[[Passage]]'] == 'number'
    #  return tale.get(val['[[Passage]]'])
    # NO.

    # Functions/RegExps
    if typeof val['[[Call]]'] == 'string'
      try
        ret = internalEval(val['[[Call]]'])
      catch e
    # Recursively recompile all properties
    # or, if a function/regexp, deep-copy them to the new function.
    for i of val
      `i = i`
      if Object::hasOwnProperty.call(val, i)
        ret[i] = recompile(val[i])
  ret

addStyle = (b) ->
  if document.createStyleSheet
    document.getElementsByTagName('head')[0].insertAdjacentHTML 'beforeEnd', '&nbsp;<style>' + b + '</style>'
  else
    a = document.createElement('style')
    a.appendChild document.createTextNode(b)
    document.getElementsByTagName('head')[0].appendChild a
  return

alterCSS = (text) ->
  temp = ''
  imgPassages = tale.lookup('tags', 'Twine.image')
  # Remove comments
  text = text.replace(/\/\*(?:[^\*]|\*(?!\/))*\*\//g, '')
  # Replace :link
  text = text.replace(/:link/g, '[class*=Link]')
  # Replace :visited
  text = text.replace(/:visited/g, '.visitedLink')
  # Hoist @import
  text = text.replace(/@import\s+(?:url\s*\(\s*['"]?|['"])[^"'\s]+(?:['"]?\s*\)|['"])\s*([\w\s\(\)\d\:,\-]*);/g, (e) ->
    temp += e
    ''
  )
  text = temp + text
  # Add images
  text.replace new RegExp(Wikifier.imageFormatter.lookahead, 'gim'), (m, p1, p2, p3, src) ->
    i = 0
    while i < imgPassages.length
      if imgPassages[i].title == src
        src = imgPassages[i].text
        break
      i++
    'url(' + src + ')'

setTransitionCSS = (styleText) ->
  styleText = alterCSS(styleText)
  style = document.getElementById('transitionCSS')
  if style.styleSheet then (style.styleSheet.cssText = styleText) else (style.innerHTML = styleText)
  return

throwError = (a, b, tooltip, exception = null) ->
  if a
    elem = insertElement(a, 'span', null, 'marked', b)
    tooltip and elem.setAttribute('title', tooltip)
  else
    alert 'Regrettably, this ' + tale.identity() + '\'s code just ran into a problem:\n' + b + '.\n' + softErrorMessage
  return

rot13 = (s) ->
  s.replace /[a-zA-Z]/g, (c) ->
    String.fromCharCode if (if c <= 'Z' then 90 else 122) >= (c = c.charCodeAt() + 13) then c else c - 26

fade = (f, c) ->
  h = undefined
  e = f.cloneNode(true)
  g = if c.fade == 'in' then 1 else -1

  d = ->
    h += 0.05 * g
    b e, Math.easeInOut(h)
    if g == 1 and h >= 1 or g == -1 and h <= 0
      f.style.visibility = if c.fade == 'in' then 'visible' else 'hidden'
      if e.parentNode
        e.parentNode.replaceChild f, e
      window.clearInterval a
      if c.onComplete
        c.onComplete.call f
    return

  b = (k, j) ->
    l = Math.floor(j * 100)
    k.style.zoom = 1
    k.style.filter = 'alpha(opacity=' + l + ')'
    k.style.opacity = j
    return

  f.parentNode.replaceChild e, f
  if c.fade == 'in'
    h = 0
    e.style.visibility = 'visible'
  else
    h = 1
  b e, h
  a = window.setInterval(d, 25)
  return

window.clone = clone
window.insertElement = insertElement
window.addClickHandler = addClickHandler
window.insertText = insertText
window.setPageElement = setPageElement
# Nylon
scrollWindowInterval = undefined

Math.easeInOut = (a) ->
  1 - ((Math.cos(a * Math.PI) + 1) / 2)

String::readMacroParams = (keepquotes) ->
  exec = undefined
  re = /(?:\s*)(?:(?:"([^"]*)")|(?:'([^']*)')|(?:\[\[((?:[^\]]|\](?!\]))*)\]\])|([^"'\s]\S*))/mg
  params = []
  loop
    val = undefined
    exec = re.exec(this)
    if exec
      if exec[1]
        val = exec[1]
        keepquotes and (val = '"' + val + '"')
      else if exec[2]
        val = exec[2]
        keepquotes and (val = '\'' + val + '\'')
      else if exec[3]
        val = exec[3]
        keepquotes and (val = '"' + val.replace('"', '\"') + '"')
      else if exec[4]
        val = exec[4]
      val and params.push(val)
    unless exec
      break
  params

String::readBracketedList = ->
  c = undefined
  b = '\\[\\[([^\\]]+)\\]\\]'
  a = '[^\\s$]+'
  e = '(?:' + b + ')|(' + a + ')'
  d = new RegExp(e, 'mg')
  f = []
  loop
    c = d.exec(this)
    if c
      if c[1]
        f.push c[1]
      else
        if c[2]
          f.push c[2]
    unless c
      break
  f

###
**
** Polyfills
**
###

Object.create or do ->
  F = ->
  Object.create = (o) ->
    if typeof o != 'object'
      throw TypeError()
    F.prototype = o
    new F
  return
String::trim or
(String::trim = ->
  @replace(/^\s\s*/, '').replace /\s\s*$/, ''
)
Array.isArray or
(Array.isArray = (arg) ->
  Object::toString.call(arg) == '[object Array]'
)
Array::indexOf or
(Array::indexOf = (b, d) ->
  d = if d == null then 0 else d
  a = @length
  c = d
  while c < a
    if @[c] == b
      return c
    c++
  -1
)
Array::forEach or
(Array::forEach = (fun) ->
  if this == null
    throw TypeError()
  t = Object(this)
  len = +t.length
  if typeof fun != 'function'
    throw TypeError()
  thisArg = if arguments.length >= 2 then arguments[1] else undefined
  i = 0
  while i < len
    if i of t
      fun.call thisArg, t[i], i, t
    i++
  return
)

### Polyfill for fade transition ###
hasTransition = 'transition' of document.documentElement.style or '-webkit-transition' of document.documentElement.style

window.arrayBufferToString = (buf) ->
  String.fromCharCode.apply null, new Uint8Array(buf)

window.stringToArrayBuffer = (str) ->
  buf = new ArrayBuffer(str.length * 2)
  # 2 bytes for each char
  bufView = new Uint8Array(buf)
  i = 0
  strLen = str.length
  while i < strLen
    bufView[i] = str.charCodeAt(i)
    i++
  buf

Function::getter = (prop, get) ->
  Object.defineProperty @prototype, prop, {get, configurable: yes}

Function::setter = (prop, set) ->
  Object.defineProperty @prototype, prop, {set, configurable: yes}

Function::property = (prop, desc) ->
  Object.defineProperty @prototype, prop, desc
