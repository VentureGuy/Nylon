###
# Nylon passage object.
#
# Rewritten to no longer need the DOM for init.
###
class NylonPassage
    @unescapeLineBreaks: (a) ->
        if a and typeof a == "string"
            return a.replace(/\\n/mg, "\n").replace(/\\t/mg, "\t").replace(/\\s/mg, "\\").replace(/\\/mg, "\\").replace(/\r/mg, "")
        else
            return ""

    constructor: (@title, @id=-1, @tags=[], @text='', @deobfuscator=null, @config=null) ->
        @element = null
        if @id > -1
            @preload()

    preload: ->
        @text = NylonPassage.unescapeLineBreaks @text
        if @deobfuscator
            @deobfuscator.prepare @id, @config
            @title = @deobfuscate @title
            @text = @deobfuscate @text
            @tags = [@deobfuscate tag for tag in @tags]
        # Preload linked images
        if !@isImage()
            @preloadImages()
        # Check for the .char selector, or the [data-char] selector
        # False positives aren't a big issue.
        if /\.char\b|\[data\-char\b/.exec(@text) and Wikifier.charSpanFormatter
            Wikifier.formatters.push Wikifier.charSpanFormatter
            delete Wikifier.charSpanFormatter

    deobfuscate: (a) ->
        if deobfuscator
            return deobfuscator.deobfuscate a
        else
            return a

    isImage: ->
        return 'Twine.image' in @tags

    preloadImages: ->
        # Don't preload URLs containing '$' - suspect that they are variables.
        u = '\\s*[\'"]?([^"\'$]+\\.(jpe?g|a?png|gif|bmp|webp|svg))[\'"]?\\s*'

        k = (c, e) ->
            i = undefined
            d = undefined
            loop
                d = c.exec(@text)
                if d
                    i = new Image
                    i.src = d[e]
                unless d
                    break
            k
        k.call(this, new RegExp(Wikifier.imageFormatter.lookahead.replace('[^\\[\\]\\|]+', u), 'mg'), 4)
         .call(this, new RegExp('url\\s*\\(' + u + '\\)', 'mig'), 1)
         .call this, new RegExp('src\\s*=' + u, 'mig'), 1
        return


    setTags: (b) ->
        t = if @tags != null and @tags.length then @tags.join(' ') else ''
        if t
            b.setAttribute 'data-tags', @tags.join(' ')
        document.body.setAttribute 'data-tags', t
        document.getElementsByTagName('html')[0].setAttribute 'data-tags', t
        return

    processText: ->
      ret = @text
      if 'nobr' in @tags
        ret = ret.replace(/\n/g, '‌')
      if @isImage()
        ret = '[img[' + ret + ']]'
      return ret
