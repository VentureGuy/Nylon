###
# Does absolutely nothing.
###
class NylonNullDeobfuscator extends NylonDeobfuscator
    @ID: null
    @NAME: 'NULL'
    constructor: (story, config) ->
        super null, story, config

    Deobfuscate: (data) ->
        return data

NylonDeobfuscator.Register NylonNullDeobfuscator
