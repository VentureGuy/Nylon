###
# Changes gibberish into usable code.
###
class NylonDeobfuscator
    @ID: ''
    @NAME: ''

    @_ALL: {}
    @Register: (cls) ->
        @_ALL[cls.ID]=cls

    @CreateByID: (story, id, config) ->
        console and console.log 'Selected deobfuscator', id
        return new @_ALL[id] story, config

    constructor: (@ID, @Story, @Config) ->
        return

    Deobfuscate: (data) ->
        return data
