###
# Basic Vigenere obfuscation
###
class NylonVigenereDeobfuscator extends NylonDeobfuscator
    @ID: 'vigenere'
    @NAME: 'Vigenere Cypher'

    constructor: (story, config) ->
        super 'vigenere', story, config
        @Key = @Config.key

    Deobfuscate: (data) ->
        string = new Uint8Array(data)
        o = new Uint8Array(string.length)
        for i in [0..data.length]
            k = @Key.charCodeAt i % @Key.length
            e = (string[i] - k + 256) % 256
            o[i] = e
        return new ArrayBuffer o

NylonDeobfuscator.Register NylonVigenereDeobfuscator
