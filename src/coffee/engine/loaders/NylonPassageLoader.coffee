class NylonPassageLoader
    @_ALL: {}
    @Register: (cls) ->
        @_ALL[cls.ID] = cls
        return

    @CreateByID: (story, id, config) ->
        console and console.log 'Selected loader', id
        return new @_ALL[id] story, config

    constructor: (@Story, @Config)->
        @Passages = {}
        @Progress = (message, step, steps) ->
            console.log "(#{step}/#{steps}) #{message}"

    has: (passageID) =>
        return passageID of @Passages

    get: (passageID, done) ->
        return

    _get: (passageID) ->
        return @_ALL[passageID]

    load: (opts) =>
        if 'progress' of opts
            @Progress = opts.progress
        return
