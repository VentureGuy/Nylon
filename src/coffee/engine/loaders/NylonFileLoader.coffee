class NylonFileParsingError
    constructor: ->

    toString: ->
        'Failed to load mod file.'

makeRequest = (method, url, binary) ->
  new Promise((resolve, reject) ->
    xhr = new XMLHttpRequest
    #if binary
    #  xhr.responseType = 'arraybuffer'
    xhr.open method, url
    xhr.onload = ->
      if @status >= 200 and @status < 300
        resolve xhr.response
      else
        reject
          status: @status
          statusText: xhr.statusText
      return

    xhr.onerror = ->
      reject
        status: @status
        statusText: xhr.statusText
      return

    xhr.send()
    return
)

syncFetch = (method, url, binary) ->
    console && console.log url
    xhr = new XMLHttpRequest
    xhr.open method, url, false
    #if binary
    #  xhr.responseType = 'arraybuffer'
    xhr.send()
    return xhr

###
# Loads passages from JSON/YAML files in paths relative to the HTML file.
#
# Game data directory would be structured like:
#
# passages/
#   __INDEX__
#   PassageA.json
#   PassageB.json
#   ...
# MODS.txt
# mods/
#   __INDEX__
#   ...
#
# Supports loading mod indices via entries in MODS.txt. Mod passages
# overwrite base files.
#
# CONFIG:
#   lang: yml/(json)
#   paths: ['passages']
#   allow-mods: yes/no
#   index-files: '__INDEX__'
#   obfuscation:
#     id: (null|vigenere|...)
#     key: ...
###
class NylonFileLoader extends NylonLazyLoader
    @ID: 'file'
    @FILE_FORMAT_VERSION: 2

    @STATE_DEAD: 0
    @STATE_INITIALIZING: 1
    @STATE_INITIALIZED: 2

    @STATE: 0

    constructor: (story, config) ->
        super story, config
        @Language = if 'lang' in config then config['lang'] else 'json'
        @ModCount = 0
        @ModPageCount = 0
        @PagesToLoad = []
        @DataPaths = []
        @DataPaths.push [
            if 'path' in config then config['path'] else 'core',
            if 'checksum' in config then config['checksum'] else null
        ]
        @AllowMods = if 'allow-mods' in config then config['allow-mods'] else yes
        @IndexFile = if 'index-file' in config then config['index-file'] else '__INDEX__'

    getExt: ->
        if @Story.Deobfuscator.ID != null
            return '.dat'

        switch @Language
            when 'json'
                return '.json'
            when 'yaml'
                return '.yml'
            else
                alert('Incorrect NylonFileLoader.Language.  Should be either "json" or "yaml".')
                return null

    parseSyncResponse: (response) ->
        console.log "Response:", response
        bytes = new Uint8Array response.response
        bytes = @Story.Deobfuscator.Deobfuscate(bytes)
        data = {}
        switch @Language
            when 'json'
                return JSON.parse arrayBufferToString bytes
            when 'yaml'
                return yaml.load arrayBufferToString bytes
            else
                window.HALT_WAIT = true
                alert('Incorrect NylonFileLoader.Language.  Should be either "json" or "yaml".')
                return null
    parseResponse: (response, cb) ->
        console.log "Response:", response
        response.arrayBuffer()
        .then (bytes) =>
            bytes = @Story.Deobfuscator.Deobfuscate(bytes)
            data = {}
            switch @Language
                when 'json'
                    cb JSON.parse arrayBufferToString bytes
                    return
                when 'yaml'
                    cb yaml.load arrayBufferToString bytes
                    return
                else
                    window.HALT_WAIT = true
                    alert('Incorrect NylonFileLoader.Language.  Should be either "json" or "yaml".')
                    return
        .catch (err) ->
            console.error err
            alert 'Unable to convert body into an ArrayBuffer.'
        return

    objToUrlParams = (dict) ->
        o=[]
        for own k, v of dict
            o.push "#{convertURIComponent(k)}=#{convertURIComponent(v)}"
        return o.join '&'

    getFromServer: (url, data={}) ->
        uriparams = ''
        if Object.keys(data).length > 0
            uriparams = '?'+@objToUrlParams(data)
        # https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
        return fetch url+uriparams,
            method: 'GET'
            mode: 'same-origin'
            cache: 'no-cache' # No caching.
            credentials: 'omit' # We don't expect to need credentials.

    postToServer: (url, data) ->
        # https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
        return fetch url+uriparams,
            method: 'POST'
            mode: 'same-origin'
            cache: 'no-cache' # No caching.
            credentials: 'omit' # We don't expect to need credentials.
            body: JSON.stringify data

    load: (@progress) =>
        @PreloadData={}
        @Passages={}

        if @AllowMods
            #xhr = fetch 'GET', '/mods', false
            @getFromServer '/mods'
            .then (response) =>
                if @checkResponse response
                    response.json().then (data) =>
                        console.log data
                        @DataPaths = []
                        for key of data
                            @DataPaths.push [key, data[key]]
                        return
                return
            .then =>
                @loadFromDataPaths()
                return
            .catch (err) ->
                window.HALT_WAIT = true
                alert "Unable to fetch /mods from the server."
                console.error err
                return
        else
            @loadFromDataPaths()
        return

    loadFromDataPaths: ->
        @ModCount = @DataPaths.length
        @progress and @progress.progress 'Loading mods', 0, @ModCount
        @fetchNextMod()

    fetchNextMod: =>
        if @DataPaths.length
            key = @DataPaths.pop()
            checksum = null
            if typeof key == 'object' and key.constructor.name == 'Array'
                [key, checksum] = key
            msg = if key == 'core' then 'Loading game...' else "Loading mod #{key}..."
            @progress and @progress.progress msg, @ModCount-@DataPaths.length, @ModCount
            @loadMod key, checksum, =>
                a = =>
                    @fetchNextMod()
                    return
                setTimeout a, 10
                return
        else
            @progress and @progress.end()
        return

    checkResponse: (response) ->
        console.log "Received #{response.status} #{response.statusText}"
        return response.status == 200

    loadMod: (mod_id, checksum, cb) ->
        ###
        # Index:
        # {"Title":"MD5Checksum"}
        ###
        indexfile = "/mod/#{mod_id}/#{@IndexFile}"
        @getFromServer indexfile
        .then (response) =>
            if @checkResponse response
                if !@AllowMods and checksum
                    actualchecksum  = CryptoJS.MD5(response.responseText)
                    if actualchecksum != checksum
                        err = "NylonFileLoader: Index #{indexfile} has a checksum mismatch; It may be corrupt, or you may have modified it. Please reinstall.\n\nOriginal: #{checksum}\nCurrent: #{actualchecksum}"
                        alert err
                        console.error err
                        throw new Exception "Checksum mismatch"

                @parseResponse response, (storyIndex) =>
                    len = Object.keys(storyIndex).length
                    step = 0
                    for childname, index in Object.keys(storyIndex)
                        checksum  = storyIndex[childname]
                        switch childname
                            when 'StorySettings'
                                continue
                        @preloadPassage childname, index,
                            mod_id: mod_id
                            checksum: checksum
                    if @LoadLazily
                        cb()
                    return
        return

    ###
    # 2
    # ---
    # [header]
    # ---
    # CONTENT
    ###
    loadPassage: (passageID, cb) ->
        @Passages[passageID] = null
        pd = @PreloadData[passageID]
        #xhr = syncFetch 'GET', "/mod/#{pd.data.mod_id}/#{passageID}#{@getExt()}", @Story.Deobfuscator.ID != null
        @getFromServer "/mod/#{pd.data.mod_id}/#{passageID}#{@getExt()}"
        .then (response) =>
            if @checkResponse response
                console.log "Response checks out!"
                if !@AllowMods and pd.data.checksum
                    checksum=pd.data.checksum
                    actualchecksum  = CryptoJS.MD5(response.response)
                    if actualchecksum != checksum
                        throw new Exception "Passage /mod/#{pd.data.mod_id}/#{passageID}#{@getExt()} has a checksum mismatch; It may be corrupt, or you may have modified it."

                @parseResponse response, (chunk) =>
                    console.log "Received", chunk
                    if chunk[0] != NylonFileLoader.FILE_FORMAT_VERSION
                      throw new NylonFileParsingError('File format version is incorrect')
                    [_, header, text] = chunk
                    header.text = text
                    @Passages[passageID] = new NylonPassage header.id, header.id, header.tags, text, @Story.Deobfuscator, header
                    cb and cb @Passages[passageID]
                    return
                return
        return

NylonPassageLoader.Register NylonFileLoader
