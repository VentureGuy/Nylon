###
# Basically the same thing as the old Twine 1.4.2 passage loader, except lazy-loading.
#
# This, as in the old loader does, just grabs stuff out of HTML elements injected into the bottom of the document.
# config:
#  load-lazily: yes/no
###
class NylonEmbeddedLoader extends NylonLazyLoader
    @ID: 'embedded'

    constructor: (story, config) ->
        super story, config
        @PreloadData={}

    load: ->
        for child, index in document.getElementById("storeArea").children
            childname = child.getAttribute and child.getAttribute("tiddler")
            switch childname
                when 'StorySettings'
                    continue
            @preloadPassage childname, index, child, null

    loadPassage: (passageID, cb) ->
        pd = @PreloadData[passageID]
        c = pd.data
        childname = pd.name
        tags = c.getAttribute and c.getAttribute("tags")
        text = if c.firstChild then c.firstChild.nodeValue else ""
        @Passage[passageID] = new NylonPassage passageID, pd.index, tags.split(' '), text, @Story.Deobfuscator, null # Too old for config objects.
        cb and cb(@Passage[passageID])

NylonPassageLoader.Register NylonEmbeddedLoader
