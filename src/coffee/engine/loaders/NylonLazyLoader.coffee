###
# Provides base lazy-loading capabilities.
###
class NylonLazyLoader extends NylonPassageLoader

    constructor: (story, config) ->
        super story, config
        @PreloadData={}
        @LoadLazily=if 'load-lazily' of config then config['load-lazily'] else true

        @LoadedPassageCount = 0
        @PreloadedPassageCount = 0

    load: ->
        return

    preloadPassage: (id, index, data, done) ->
        @Passages[id] = null
        @PreloadData[id] =
            name: id
            index: index
            data: data
        @PreloadedPassages++

        if not @LoadLazily
            @loadPassage id, ->
                @progress and @progress.progress "Loading #{mod_id} passages", @LoadedPassageCount, @PreloadedPassageCount
                @LoadedPassageCount++
                if @PreloadedPassageCount == @LoadedPassageCount
                    done and done()
        else
            done and done()

    has: (id) ->
        return id of @PreloadData

    loadPassage: (passageID, cb) ->
        return

    get: (id, done) =>
        if id not of @Passages
            return null
        if @Passages[id] == null
            @loadPassage id, done
        else
            done @Passages[id]
