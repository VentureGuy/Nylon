getAllFuncs = (toCheck) ->
  props = []
  obj = toCheck
  loop
    props = props.concat(Object.getOwnPropertyNames(obj))
    unless obj = Object.getPrototypeOf(obj)
      break
  props.sort().filter (e, i, arr) ->
    if e != arr[i + 1] and typeof toCheck[e] == 'function'
      return true
    return
BUILTINS = getAllFuncs this
document.addEventListener 'DOMContentLoaded', ->
    document.getElementById('b').addEventListener 'click', (e) ->
        document.getElementById('a').select()
        document.execCommand 'copy'
        alert 'COPIED'
        return
    UA = navigator.userAgent
    browser = 'UNKNOWN'
    if UA.includes('Safari') and (not UA.includes('Chrome') and not UA.includes('Chromium'))
        browser = 'SAFARI'
    else if UA.includes('Chrome')
        if UA.includes('Chromium')
            browser = 'CHROMIUM'
        else
            browser = 'CHROME'
    else if UA.includes('Firefox') or UA.includes('Iceweasel')
        if UA.includes('Seamonkey')
            browser = 'SEAMONKEY'
        else
            browser = 'FIREFOX'
    else if UA.includes('MSIE') or UA.includes('Trident')
        browser = 'IE'
    else if UA.includes('OPR/') or UA.includes('Opera/')
        browser = 'OPERA'

    data =
        'time':    Date.now()
        'browser': browser
        'UA':      UA
        'this':    BUILTINS
    rmi = (a, i) ->
        idx = a.indexOf i
        if idx > -1
            a.splice idx, 1
        return a
    rmi data.this, 'BUILTINS' # me
    rmi data.this, 'indexOf' # coffeescript
    rmi data.this, 'getAllFuncs' # me
    a = document.getElementById('a')
    a.value = JSON.stringify data, null, 2
    a.readonly = yes
    return
