var tjapi;
(function (tjapi) {
    var JsReserved = [
        'void', 'var', 'const', 'let', 'in', 'of',
        'return', 'break', 'continue', 'goto', 'for', 'if', 'else', 'switch', 'case', 'while', 'do', 'default', 'with',
        'function', 'class', 'typeof', 'delete', 'new'
    ];
    var DecimalRegex = /^([+-]?(\d+\.\d*|\.\d+|\d+)(e[+-]?\d+)?)/;
    var ReswordRegex = /^(if|else|elseif)\b/;
    var IdentifierRegex = /^[a-z_$][a-z_$0-9]*/i;
    var Operators = ['===', '!==', '==', '!=', '<=', '>=', '>', '<',
        '**=',
        '++', '--', '**', '&&', '||',
        '+=', '-=', '*=', '/=', '%=', '^=', '&=', '|=',
        '+', '-', '~', '*', '/', '%', '^', '&', '|',
        '?', ':', '=', ';', '(', ')', ',', '.', '{', '}', '[', ']'];
    var VarInString = /^(["'])(\$[a-z_][a-z0-9]*)\1/i;
    var SBrackets = /^\[|]$/g;
    function tw2js1(fn, code, src, help) {
        var rslt = fn(code, code.slice(1).join(' '), src, help);
        if (typeof rslt == 'string')
            return [rslt, 0, 0];
        else
            return rslt;
    }
    function mangle(s) {
        s = s.replace(/[^a-zA-Z0-9$_]/ig, '_');
        if (s[0] >= '0' && s[0] <= '9')
            s = '_' + s;
        if (JsReserved.indexOf(s) != -1)
            return s + '_';
        return s;
    }
    tjapi.mangle = mangle;
    function fargs(input) {
        return input.join(', ');
    }
    tjapi.fargs = fargs;
    function fargs2(input) {
        return input.map(function (e) { return e + ', '; }).join('');
    }
    tjapi.fargs2 = fargs2;
    function fargs3(input) {
        return JSON.stringify(input).replace(SBrackets, '');
    }
    tjapi.fargs3 = fargs3;
    tjapi.LibStd = {
        callesc: function (arr, rem, loc, help) { return ['f' + arr[0] + '(' + fargs3(arr.slice(1)) + ');']; },
        call: function (arr, rem, loc, help) { return ['f' + arr[0] + '(' + fargs(arr.slice(1)) + ');']; },
        callstr: function (arr, rem, loc, help) { return ['f' + arr[0] + '(' + rem + ');']; },
        ftstart: function (arr, rem, loc, help) { return ['f' + arr[0] + '(' + fargs2(arr.slice(1)) + 'function(){', 0, 1]; },
        fjstart: function (arr, rem, loc, help) { return ['f' + arr[0] + '(' + rem + ',function(){', 0, 1]; },
        fend: function () { return ['});', -1, 0]; }
    };
    tjapi.LibExt = {};
    tjapi.Lib = {
        'set': function (arr, rem) { return [rem + ';', 0, 0]; },
        'unset': function (arr, rem) { return ['delete ' + rem + ';', 0, 0]; },
        'run': function (arr, rem) { return [rem + ';', 0, 0]; },
        'if': function (arr, rem) { return ['if (' + rem + ') {', 0, 1]; },
        'else': function (arr, rem) { return ['} else {', -1, 1]; },
        'elseif': function (arr, rem) { return ['} else if(' + rem + ') {', -1, 1]; },
        'endif': function () { return ['}', -1, 0]; },
        '/if': function () { return ['}', -1, 0]; },
        'print': tjapi.LibStd.callstr,
        'display': tjapi.LibStd.callstr,
        'goto': tjapi.LibStd.callstr,
        'return': tjapi.LibStd.callstr,
        'audio': tjapi.LibStd.call,
        'back': tjapi.LibStd.call,
        'cacheaudio': tjapi.LibStd.call,
        'numberbox': tjapi.LibStd.call,
        'onchange': tjapi.LibStd.call,
        'textbox': tjapi.LibStd.call,
        'click': tjapi.LibStd.fjstart,
        '/click': tjapi.LibStd.fend,
        'link': tjapi.LibStd.fjstart,
        '/link': tjapi.LibStd.fend,
        'nobr': tjapi.LibStd.ftstart,
        '/nobr': tjapi.LibStd.fend,
        'numberpool': tjapi.LibStd.ftstart,
        '/numberpool': tjapi.LibStd.fend,
        'replace': tjapi.LibStd.ftstart,
        '/replace': tjapi.LibStd.fend,
        'silently': tjapi.LibStd.ftstart,
        'endsilently': tjapi.LibStd.fend,
        '/silently': tjapi.LibStd.fend,
        'widget': tjapi.LibStd.ftstart,
        '/widget': tjapi.LibStd.fend,
        __unknown__: function (arr, rem, loc, help) {
            console.warn('unknown_command', loc, help);
            if (arr[0].match(IdentifierRegex))
                return [mangle(arr[0]) + '(' + JSON.stringify(arr.slice(1)).replace(SBrackets, '') + ');', 0, 0];
            return ['fcall(' + JSON.stringify(arr).replace(SBrackets, '') + ');', 0, 0];
        },
        __default__: function (arr, rem, loc, help) { return ['fprint(' + JSON.stringify(arr).replace(SBrackets, '') + ');', 0, 0]; },
        __link__: function (arr, rem, loc, help) { return ['fprintlink(' + JSON.stringify(arr).replace(SBrackets, '') + ');', 0, 0]; },
        __eval__: function (arr, rem, tok) {
            var subs = {
                to: '=',
                gte: '>=',
                lte: '<=',
                gt: '>',
                lt: '<',
                is: '==',
                isnot: '!=',
                eq: '==',
                and: '&&',
                or: '||',
                ndef: '"undefined" === typeof',
                def: '"undefined" !== typeof',
                '[[': 'flink("',
                ']]': '".trim())'
            };
            return subs[tok] || tok;
        }
    };
    function tw2codebase_old(tw, key, idx) {
        return '{\n' +
            tw.map(function (j) {
                return JSON.stringify(j[key]) + ":function(){\n" +
                    tw2js(j[idx], j[key], '\t\t') +
                    "\n}";
            }).join(',\n') + '\n}';
    }
    tjapi.tw2codebase_old = tw2codebase_old;
    function tw2codebase(tw, key, idx) {
        tw = (tw.concat([])).sort(function (a, b) { return (a[key].toUpperCase() > b[key].toUpperCase() ? 1 : a[key].toUpperCase() === b[key].toUpperCase() ? 0 : -1); });
        var names = [];
        var body = tw.map(function (j) {
            var name = j[key];
            var fname = mangle(name);
            names.push([name, fname]);
            return 'function ' + fname + '(){\n' +
                tw2js(j[idx], name, '\t\t') +
                '\n}\n';
            //+'passages[' + JSON.stringify(name) + '] = '+fname+';';
        }).join('\n') + '\n';
        return 'var state={};\n' +
            body + '\n' +
            'var passages={\n' +
            names.map(function (n) { return '\t' + JSON.stringify(n[0]) + ':' + n[1]; }).join(',\n') +
            '\n};\n';
    }
    tjapi.tw2codebase = tw2codebase;
    function tw2js(tw, help, indent) {
        if (help === void 0) { help = '(unknown)'; }
        if (indent === void 0) { indent = ''; }
        return twtok2js(twinelex(tw, help), help, indent);
    }
    tjapi.tw2js = tw2js;
    function twtok2js(tokens, help, indent) {
        if (help === void 0) { help = '(unknown)'; }
        if (indent === void 0) { indent = ''; }
        tokens = tokens.slice();
        var LIB = {};
        for (var k in tjapi.Lib)
            LIB[k] = tjapi.Lib[k];
        for (var k in tjapi.LibExt)
            LIB[k] = tjapi.LibExt[k];
        var dindent = function (s, di) { return (di > 0) ? s + '\t' : (di < 0) ? s.substring(1) : s; };
        var tjeval = LIB.__eval__;
        var rslt = '';
        for (var i = 0; i < tokens.length; i++) {
            var tok = tokens[i];
            try {
                var out = void 0, di0 = void 0, di1 = void 0;
                switch (tok) {
                    case '<<':
                    case '<</':
                        var op = tok;
                        var closing = tok == '<</';
                        var code = [];
                        while ((tok = tokens[++i]) != '>>' && tok !== undefined) {
                            code.push(tw2js1(tjeval, [tok], tok, help)[0]);
                        }
                        var fn = void 0;
                        fn = LIB[closing ? '/' + code[0] : code[0]] || LIB.__unknown__;
                        _a = tw2js1(fn, code, op + code.join(' ') + '>>', help), out = _a[0], di0 = _a[1], di1 = _a[2];
                        break;
                    case '[[':
                        var link = [];
                        while ((tok = tokens[++i]) != ']]' && tok !== undefined) {
                            link.push(tw2js1(tjeval, [tok], tok, help)[0]);
                        }
                        _b = tw2js1(LIB.__link__, link, '[[' + link.join(' ') + ']]', help), out = _b[0], di0 = _b[1], di1 = _b[2];
                        break;
                    default:
                        _c = tw2js1(LIB.__default__, [tok], tok, help + ':' + i), out = _c[0], di0 = _c[1], di1 = _c[2];
                }
                indent = dindent(indent, di0);
                rslt += '\n' + indent + out;
                indent = dindent(indent, di1);
            }
            catch (e) {
                console.error(tok, help + ':' + i, e);
            }
        }
        return rslt;
        var _a, _b, _c;
    }
    tjapi.twtok2js = twtok2js;
    function twinelex(input, help) {
        input = (input || ''); //.replace(/\n/g,'\\n');
        var s = input;
        var rslt = [];
        var buffer = '';
        var stack = ['twine'];
        var pos = 0;
        var _loop_1 = function () {
            var c0 = s[0], c1 = s[1], c01 = c1 ? c0 + c1 : c0, l = void 0, buff = '', token = void 0, x = void 0, y = void 0;
            var state = stack[stack.length - 1];
            switch (state) {
                case 'stringsq':
                case 'stringdq':
                    var op = (state == 'stringsq') ? '\'' : '\"';
                    if (c0 == '\\') {
                        l = (buff = c01).length;
                    }
                    else if (c0 == '$') {
                        l = (y = s.match(IdentifierRegex)[0]).length;
                        token = [op, '+', 'state.' + y.substr(1), '+', op];
                    }
                    else if ((c0 == '"' && state == 'stringdq') || (c0 == "'" && state == 'stringsq')) {
                        token = buffer + c0;
                        buffer = '';
                        l = 1;
                        stack.pop();
                    }
                    else {
                        buff = s.substr(0, (l = s.indexOf(op)));
                    }
                    break;
                case 'twlink':
                    if ((x = s.match(/^[^\]]+/))) {
                        l = (buff = x[0]).length;
                    }
                    else if (c01 == ']]') {
                        l = (token = c01).length;
                        stack.pop();
                    }
                    else if (c0 == ']') {
                        l = (buff = c0).length;
                    }
                    break;
                case 'twexpr':
                    switch (c01) {
                        case '>>':
                            l = (token = c01).length;
                            stack.pop();
                            break;
                        case '[[':
                            l = (token = c01).length;
                            stack.push('twlink');
                            break;
                        default:
                            var s3_1 = s.substr(0, 3);
                            if ((x = s.match(DecimalRegex))) {
                                l = (token = x[0]).length;
                            }
                            else if ((x = s.match(ReswordRegex))) {
                                l = (token = x[0]).length;
                            }
                            else if ((y = Operators.filter(function (i) { return s3_1.indexOf(i) === 0; })[0])) {
                                l = (token = y).length;
                            }
                            else if ((x = s.match(VarInString))) {
                                l = x[0].length;
                                token = 'state.' + x[2];
                            }
                            else if (c0 == '"' || c0 == "'") {
                                l = (buff = c0).length;
                                stack.push((c0 == '"') ? 'stringdq' : 'stringsq');
                            }
                            else if ((x = s.match(/^(\s|\\t|\\n)+/))) {
                                l = x[0].length;
                            }
                            else if ((x = s.match(IdentifierRegex))) {
                                l = (y = x[0]).length;
                                if (y[0] == '$')
                                    y = 'state.' + y.substr(1);
                                token = y;
                            }
                    }
                    break;
                case 'twine':
                    if ((x = s.match(/^[^<>\[\]\\]+/))) {
                        l = (buff = x[0]).length;
                    }
                    else if (c01 == '<<') {
                        if (s[2] == '/')
                            l = (token = '<</').length;
                        else
                            l = (token = c01).length;
                        stack.push('twexpr');
                    }
                    else if (c01 == '[[') {
                        l = (token = c01).length;
                        stack.push('twlink');
                    }
                    else {
                        l = (buff = c0).length;
                    }
                    break;
                default:
                    //console.warn(''+state+' on stack ['+stack.join(' ')+']');
                    stack = ['twine'];
            }
            if (l === undefined) {
                //rslt.push([c0,state]);
                console.warn('unknown_token', c01 + " in " + state + " at ", (input.substring(Math.max(0, pos - 20), pos)), "#>", (s.length > 20 ? s.substr(0, 17) + '...' : s) + "'", help);
                l = 1;
            }
            if (buff)
                buffer += buff;
            if (token) {
                if (buffer.length > 0)
                    rslt.push(buffer.replace(/^\s\s+/, ' ').replace(/\s\s+$/, ' '));
                if (typeof token === 'string')
                    rslt.push(token);
                else
                    rslt = rslt.concat(token);
                buffer = '';
            }
            s = s.slice(l);
            pos += l;
        };
        while (s.length > 0) {
            _loop_1();
        }
        if (buffer.length > 0)
            rslt.push(buffer.replace(/^\s\s+/, ' ').replace(/\s\s+$/, ' '));
        return rslt;
    }
    tjapi.twinelex = twinelex;
    function loadPrev() {
        return JSON.parse(localStorage.getItem("vnext") || '{}');
    }
    tjapi.loadPrev = loadPrev;
    function savePrev() {
        return localStorage.setItem("vnext", JSON.stringify(storeArea()));
    }
    tjapi.savePrev = savePrev;
    function storeArea() {
        var selector = $("tw-passagedata").length ? ['tw-passagedata', 'name'] : ['#store-area > *', 'tiddler'];
        return $(selector[0]).toArray()
            .filter(function (x) { return ["stylesheet", "script"].indexOf(x.getAttribute("tags") || "") < 0; })
            .map(function (x) { return ({
            n: x.getAttribute(selector[1]),
            t: x.textContent
        }); });
    }
    tjapi.storeArea = storeArea;
    function tomap(v, kk, kv) {
        if (kk === void 0) { kk = 'n'; }
        if (kv === void 0) { kv = 't'; }
        return v.reduce(function (r, e) {
            r[e[kk]] = e[kv];
            return r;
        }, {});
    }
    tjapi.tomap = tomap;
})(tjapi || (tjapi = {}));
//var vprev = tjapi.loadPrev();
var vnext = tjapi.storeArea();
var mnext = tjapi.tomap(vnext);
var thiz = tjapi.tw2codebase(vnext, 'n', 't');
window["copy"](thiz);
