import os
import sys
import unittest
from pydoc import locate

import yaml
import logging

from twinehack.ast.lexer import Lexer, TweeLexingError

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))


def fill_tests(cls):
    '''
    Fills the target class with data-driven test cases.
    '''
    def make_test_function(data):
        # Makes test_wrapper.
        def test_wrapper(self):
            # Wraps around actual_test in case expects-exception is set.
            def actual_test():
                # Performs the actual test.
                lexer = self._get_lexer()
                #print('IN:',repr(data.input))
                try:
                    resultfile = 'lexer-test_{}_{}.result.test'.format(ptd.id,ptd.name)
                    expectedfile = 'lexer-test_{}_{}.expected.test'.format(ptd.id,ptd.name)
                    lexer.input(data.input)
                    result = ['%s %s' % (token.type, repr(token.value)) for token in lexer]
                    with open(resultfile, 'w') as f:
                        for line in result:
                            f.write(line+'\n')
                    expected = [x.strip() for x in data.expected.split('\n') if x.strip()!='']
                    with open(expectedfile, 'w') as f:
                        for line in expected:
                            f.write(line+'\n')
                    self.assertListEqual(result, expected)
                    if os.path.isfile(resultfile): os.remove(resultfile)
                    if os.path.isfile(expectedfile): os.remove(expectedfile)
                except TweeLexingError as e:
                    print(data.input.replace('\n',' '))
                    print('-'*(e.lexpos)+'^')
                    raise e
            # END actual_test
            if data.config.get('expects-exception', None) is not None:
                with self.assertRaises(locate(data.config['expects-exception'])):
                    actual_test()
            else:
                actual_test()
        # END test_wrapper
        return test_wrapper

    for root, _, filenames in os.walk(os.path.join(SCRIPT_DIR, 'data', 'lexer')):
        for basefilename in filenames:
            filename = os.path.join(root, basefilename)
            ptd = ParserTestData(filename)
            setattr(cls, 'test_{id}_{name}'.format(id=ptd.id, name=ptd.name), make_test_function(ptd))
    return cls

class ParserTestData(object):
    """
    id: 001
    name: smoke
    expects-exception: 'KeyError'
    ---
    Input to send to the lexer.
    ---
    EMBEDDED_HTML Input to send to the lexer.
    """

    def __init__(self, filename):
        self.config = {}

        self.id = 0
        self.name = ''

        self.header = ''
        self.input = ''
        self.expected = ''

        with open(filename, 'r') as f:
            section = 0
            for line in f:
                if line.strip() == '---':
                    section += 1
                    continue
                if section == 0:
                    self.header += line
                if section == 1:
                    self.input += line
                if section == 2:
                    self.expected += line

        self.config = yaml.load(self.header)
        self.id = self.config['id']
        self.name = self.config['name']


@fill_tests
class LexerTestCase(unittest.TestCase):
    def setUp(self):
        self.lexer = Lexer()
        self.maxDiff = None

    def _get_lexer(self):
        return self.lexer

if __name__ == '__main__':
    tests = unittest.TestSuite()
    tests.addTest(LexerTestCase())
    runner = unittest.TextTestRunner()
    runner.run(tests)
