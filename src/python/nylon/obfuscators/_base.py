from typing import Union

class BaseObfuscator(object):
    ID = ''
    NAME = ''

    def __init__(self, config: dict = {}):
        self.config = config

    def Obfuscate(self, data: bytes) -> bytes:
        pass

    def Deobfuscate(self, data: bytes) -> bytes:
        pass

    def SerializeConfig(self) -> Union[list, dict]:
        o = dict(self.config)
        o['id'] = self.ID
        return o



class NylonObfuscatorFactory(object):
    ALL = {}
    @classmethod
    def Register(cls, subj) -> None:
        cls.ALL[subj.ID] = subj

    @classmethod
    def Create(cls, _id: str, config: dict) -> BaseObfuscator:
        return cls.ALL[_id](config)
