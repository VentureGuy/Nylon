from typing import Union
from ._base import BaseObfuscator, NylonObfuscatorFactory

class NullObfuscator(BaseObfuscator):
    ID = None
    NAME = 'NULL'

    def __init__(self, config: dict = {}):
        super().__init__(config)

    def Obfuscate(self, data: bytes) -> bytes:
        return data

    def Deobfuscate(self, data: bytes) -> bytes:
        return data
NylonObfuscatorFactory.Register(NullObfuscator)
