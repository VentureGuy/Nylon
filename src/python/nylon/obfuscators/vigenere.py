from buildtools import log
from typing import Union
from ._base import BaseObfuscator, NylonObfuscatorFactory
from nylon.utils import randomString

class VigenereObfuscator(BaseObfuscator):
    '''
    Loosely based on https://gist.github.com/gowhari/fea9c559f08a310e5cfd62978bc86a1a
    '''
    ID = 'vigenere'
    NAME = 'Vignere Cypher'

    def __init__(self, config: dict = {}):
        super().__init__(config)
        self.key = self.config.get('key', None)
        if self.key is None:
            keylen = int(self.config.get('keylen', 64))
            with log.info('[%s] Generating key of length %d.', self.NAME, keylen):
                self.key = randomString(keylen)
                log.info('[%s] Key: %s', self.NAME, self.key)

    def Obfuscate(self, inputstr: bytes) -> bytes:
        string = bytearray(inputstr)
        encoded = bytearray()
        for i in range(len(string)):
            k = ord(self.key[i % len(self.key)])
            e = string[i] + k % 256
            encoded.append(e)
        return bytes(encoded)

    def Deobfuscate(self, inputstr: bytes) -> bytes:
        string = bytearray(inputstr)
        encoded = bytearray()
        for i in range(len(string)):
            k = ord(self.key[i % len(self.key)])
            e = (string[i] - k + 256) % 256
            encoded.append(e)
        return bytes(encoded)
NylonObfuscatorFactory.Register(VigenereObfuscator)
