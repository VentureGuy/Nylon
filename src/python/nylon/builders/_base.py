import yaml, os, logging, json, jinja2, enum
from buildtools.bt_logging import IndentLogger
from buildtools.os_utils import get_file_list
from nylon.obfuscators import NylonObfuscatorFactory, NullObfuscator, BaseObfuscator
from nylon.consts import NAME, VERSION, TEMPLATE_DIR, ROOT_DIR
from lxml import etree

from bs4.element import Tag

# Cheating with BS4.
def Element(tag_name, **kwargs):
    return Tag(name=tag_name,attrs=kwargs)
class NylonBuilderFactory(object):
    ALL = {}
    @classmethod
    def Register(cls, subj) -> None:
        cls.ALL[subj.ID] = subj

    @classmethod
    def Create(cls, _id: str, root_dir: str, warn_as_error: bool = False) -> None:
        return cls.ALL[_id](root_dir, warn_as_error)

class ESkin(enum.Enum):
    SUGARCANE = 'sugarcane'

class NylonBaseBuilder(object):
    def __init__(self, root_dir, warn_as_error=False, skin=ESkin.SUGARCANE):
        if isinstance(skin, ESkin):
            skin=skin.value
        self.log = IndentLogger(logging.getLogger(self.__class__.__name__))
        self.root_dir = root_dir
        self.warn_as_error = warn_as_error
        self.config={}
        self.outfile = 'BUILT.html'
        self.start_at = '' # "Start", by default.
        self.indent: bool = False

        self.minify_js: bool = False

        self.hide_stats = False

        self.obfuscation: BaseObfuscator = NullObfuscator()

        self._element_lambdas = []
        self._element_ids = {}

        self.LoadConfig(os.path.join(root_dir, 'story.yml'))

        self.addEmbeddedJS(os.path.join(ROOT_DIR, 'src', 'engine2.min.js' if self.minify_js else 'engine2.js')) # Should overwrite stuff.
        self.addEmbeddedJS(os.path.join(ROOT_DIR, 'skins', skin, 'dist', f'{skin}.min.js' if self.minify_js else f'{skin}.js')) # Should overwrite stuff.


    def LoadConfig(self, filename):
        self.config = {}
        with open(filename, 'r') as f:
            self.config = yaml.safe_load(f)

        obfcfg = self.config.get('obfuscator', {'id': None})
        self.obfuscator = NylonObfuscatorFactory.Create(obfcfg['id'], obfcfg)

    def SerializeConfig(self) -> dict:
        return {}

    def _addElementLambda(self, x, **kwargs):
        idn = len(self._element_lambdas)
        assetid = kwargs.get('id', f'auto-{idn}')
        self._element_lambdas += [x]
        self._element_ids[assetid] = idn

    def addEmbeddedJS(self, filename, **kwargs):
        '''
        Tell Nylon to inject JS as embedded code.
        :param filename str:
            Filename to inject.
        :param kwargs:
            Attributes to add to the <script> tag.
        '''

        kwargs['type']='application/javascript'
        kwargs['data-filename']=filename
        def x():
            el = Element('script', **kwargs)
            with open(filename, 'r', encoding='utf-8-sig') as f:
                el.string = f.read().replace('</', '</')
            return el
        self._addElementLambda(x, **kwargs)

    def linkJS(self, uri, **kwargs):
        '''
        Tell Nylon to inject JS a linked JS file via <script src=""></script>.
        :param filename str:
            Filename to link.
        :param kwargs:
            Attributes to add to the <script> tag.
        '''
        kwargs['src']=uri
        kwargs['type']='application/javascript'
        def x():
            return Element('script', **kwargs)
        self._addElementLambda(x, **kwargs)

    def addEmbeddedCSS(self, filename, **kwargs):
        '''
        Tell Nylon to inject CSS as an embedded stylesheet.
        :param filename str:
            Filename to inject.
        :param kwargs:
            Attributes to add to the <style> tag.
        '''
        kwargs['rel']='stylesheet'
        kwargs['type']='text/css'
        kwargs['data-filename']=filename
        def x():
            el = Element('style', **kwargs)
            with open(filename, 'r', encoding='utf-8-sig') as f:
                el.string = f.read().replace('&', '&amp;').replace('<', '&lt;').replace('>','&gt;')
            return el
        self._addElementLambda(x, **kwargs)

    def linkCSS(self, uri, **kwargs):
        '''
        Tell Nylon to inject CSS as a <link href=""></link> tag.
        :param filename str:
            Filename to link.
        :param kwargs:
            Attributes to add to the <link> tag.
        '''
        kwargs['href']=uri
        kwargs['rel']='stylesheet'
        kwargs['type']='text/css'
        def x():
            return Element('link', **kwargs)
        self._addElementLambda(x, **kwargs)

    def genJSSetup(self) -> str:
        config = {}
        config['obfuscation'] = self.obfuscator.SerializeConfig()
        config['loader']=self.SerializeConfig()
        config = json.dumps(config, separators=(',',':'))
        o = 'var tale, nylon;'
        o += f'tale = nylon = new NylonStory({config});'
        #o += f'nylon.load();'
        return o

    def DoCompile(self, tprops: dict) -> None:
        return

    def Compile(self) -> None:
        tprops = {}
        js=''
        for elL in self._element_lambdas:
            element = elL()
            #js += f'<!-- {type(element)} -->\n'
            # lxml has issues with <script> tags, so I use BS4 instead.
            if etree.iselement(element):
                js += etree.tostring(element).decode('utf-8')+'\n'
            else:
                js += str(element)+'\n'
        tprops['headers']=js

        self.DoCompile(tprops)

    def writeHTMLTo(self, filename, tprops: dict):
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATE_DIR))
        t = env.get_template('tale.tmpl.html')
        tprops['engine'] = {
            'name': NAME,
            'version': VERSION
        }

        tprops['config'] = self.config
        tprops['start_at'] = self.start_at
        tprops['js_setup'] = self.genJSSetup()

        # Insert CSP because NoScript are dicks.
        csp = [
            "default-src * 'unsafe-inline' 'unsafe-eval'",
            "script-src * 'unsafe-inline' 'unsafe-eval'",
            "connect-src * 'unsafe-inline'",
            "img-src * data: blob: 'unsafe-inline'",
            "frame-src *; style-src * 'unsafe-inline'",
        ]
        tprops['csp'] = ';'.join(csp)
        with open(filename, 'w') as f:
            f.write(t.render(**tprops))
