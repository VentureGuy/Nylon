import os, yaml, re, jinja2, json, base64, tempfile, io, logging
from lxml import etree
from typing import List
#from nylon._twineimports import TiddlyWiki, TweeHeader, Tiddler, TWINE_DIR
from nylon.utils import img_to_data
from nylon.consts import NAME, VERSION, TEMPLATE_DIR, ROOT_DIR
from nylon.obfuscators import BaseObfuscator, NullObfuscator
from nylon.exceptions import NylonException, WarningAsError
from nylon.verifylexer import HeadlessVerifyLexer
from ._base import NylonBaseBuilder, NylonBuilderFactory
from buildtools import log, os_utils
from buildtools.utils import md5sum
from buildtools.os_utils import get_file_list
import html

from bs4.element import Tag

# Cheating with BS4.
def Element(tag_name, **kwargs):
    return Tag(name=tag_name,attrs=kwargs)

class NylonFileBuilder(NylonBaseBuilder):
    '''
    Builds a Nylon tale using the file heirarchy.
    '''
    ID = 'files'
    DO_NOT_INDEX=('id','tags','file','created','modifier','twine')
    VERSION = 2
    def __init__(self, root_dir, warn_as_error=False, server_executable: str = 'GameServer'):

        self.language: str = 'json'
        self.data_path: str = 'passages'
        self.allow_mods: bool = True
        self.index_file: str = '__INDEX__'
        self.server_executable: str = server_executable

        self.out_py = '.py'
        self.out_exe = '.exe'
        self.out_elf = ''
        self.out_dir = ''

        #self.obfuscator = NullObfuscator()

        super().__init__(root_dir, warn_as_error)

        self.addEmbeddedCSS(os.path.join(ROOT_DIR, 'src', 'css', 'loading-style.css'), id='nylon-loading-style')

    def LoadConfig(self, filename):
        '''
        @Language = if 'lang' in config then config['lang'] else 'json'
        @DataPath = if 'path' in config then config['path'] else 'core'
        @AllowMods = if 'allow-mods' in config then config['allow-mods'] else yes
        @IndexFile = if 'index-file' in config then config['index-file'] else '__INDEX__'
        '''
        super().LoadConfig(filename)
        self.language = self.config.get('lang', 'yaml')
        self.data_path = self.config.get('path', 'core')
        self.allow_mods = self.config.get('allow-mods', True)
        self.load_lazily = self.config.get('load-lazily', True)
        self.index_file = self.config.get('index-file', '__INDEX__')
        self.indent = self.config.get('indent', False)
        self.server_executable = self.config.get('server-executable', self.server_executable)
        #print(f'TEMPLATE_DIR={TEMPLATE_DIR}')

    def SerializeConfig(self) -> dict:
        self.out_dir = os.path.dirname(self.outfile)
        return {
            'id': 'file',
            'lang': self.language,
            'path': self.data_path,
            'checksum': md5sum(os.path.join(self.out_dir, self.data_path, self.index_file)),
            'allow-mods': self.allow_mods,
            'index-file': self.index_file,
            'load-lazily': self.load_lazily,
            'server-executable': self.server_executable,
        }

    def addEmbeddedJS(self, filename, **kwargs):
        '''
        Tell Nylon to inject JS as embedded code.
        :param filename str:
            Filename to inject.
        :param kwargs:
            Attributes to add to the <script> tag.
        '''
        kwargs['type']='application/javascript'
        kwargs['data-filename']=os.path.relpath(filename) if filename is not None else 'N/A'
        def x():
            el = Element('script', **kwargs)
            if filename is not None:
                with open(filename, 'r', encoding='utf-8-sig') as f:
                    el.string = f.read().replace('</', '<\/')
            return el
        self._element_lambdas += [x]

    def linkJS(self, uri, **kwargs):
        '''
        Tell Nylon to inject JS a linked JS file via <script src=""></script>.
        :param filename str:
            Filename to link.
        :param kwargs:
            Attributes to add to the <script> tag.
        '''
        kwargs['src']=uri
        kwargs['type']='application/javascript'
        def x():
            return Element('script', **kwargs)
        self._element_lambdas += [x]

    def addEmbeddedCSS(self, filename, **kwargs):
        '''
        Tell Nylon to inject CSS as an embedded stylesheet.
        :param filename str:
            Filename to inject.
        :param kwargs:
            Attributes to add to the <style> tag.
        '''
        kwargs['rel']='stylesheet'
        kwargs['type']='text/css'
        kwargs['data-filename']=os.path.relpath(filename) if filename is not None else 'N/A'
        def x():
            el = Element('style', **kwargs)
            if filename is not None:
                with open(filename, 'r', encoding='utf-8-sig') as f:
                    el.string = f.read().replace('&', '&amp;').replace('<', '&lt;').replace('>','&gt;')
            return el
        self._element_lambdas += [x]

    def linkCSS(self, uri, **kwargs):
        '''
        Tell Nylon to inject CSS as a <link href=""></link> tag.
        :param filename str:
            Filename to link.
        :param kwargs:
            Attributes to add to the <link> tag.
        '''
        kwargs['href']=uri
        kwargs['rel']='stylesheet'
        kwargs['type']='text/css'
        def x():
            return Element('link', **kwargs)
        self._element_lambdas += [x]

    def genJSSetup(self) -> str:
        self.out_dir = os.path.dirname(self.outfile)
        config = {}
        config['obfuscation'] = self.obfuscator.SerializeConfig()
        config['loader']={
            'id': 'file',
            'lang': self.language,
            'path': self.data_path,
            'checksum': md5sum(os.path.join(self.out_dir, self.data_path, self.index_file)),
            'allow-mods': self.allow_mods,
            'index-file': self.index_file,
            'server-executable': self.server_executable,
            'load-lazily': self.load_lazily
        }
        config = json.dumps(config, separators=(',',':'))
        #o = 'var tale, nylon;'
        #o += f'tale = nylon = new NylonStory({config});'
        #o += f'nylon.load();'
        o = f'window.configuration = {config};window.onload = function(){{window.STARTUP()}};'
        return o

    def _replaceIn(self, gsdata: str, varid: str, replacement: str) -> str:
        return gsdata.replace(f'@@{varid}@@', replacement)

    def _installGameServerFile(self, infile, outfile):
        with open(infile, 'r') as rf:
            with open(outfile, 'w') as wf:
                gsdata = rf.read()
                gsdata = self._replaceIn(gsdata, 'ENGINE_NAME', NAME)
                gsdata = self._replaceIn(gsdata, 'ENGINE_VERSION', str(VERSION))
                gsdata = self._replaceIn(gsdata, 'GAME_NAME', self.config.get('title', 'UNTITLED'))
                gsdata = self._replaceIn(gsdata, 'GAME_VERSION', self.config.get('version', '(version not set)'))
                gsdata = self._replaceIn(gsdata, 'FILENAME', os.path.basename(outfile))
                wf.write(gsdata)

    def DoCompile(self, tprops: dict) -> None:
        self.out_dir = os.path.dirname(self.outfile)
        outbase, _ = os.path.splitext(self.outfile)
        self.out_py = outbase+'.py'
        self.out_exe = outbase+'.exe'
        self.out_elf = outbase

        index_ext = '.yml' if self.language == 'yaml' else '.json'
        if self.obfuscator.ID is not None:
            index_ext = '.dat'
        index = {}
        for basefn in get_file_list(self.root_dir):
            filename = os.path.join(self.root_dir, basefn)
            if filename.endswith('.header.yml'):
                basepath = os.path.dirname(filename)
                tiddler_data = {}
                filename_override = None
                with open(filename, 'r') as f:
                    try:
                        tiddler_data = yaml.safe_load(f)
                    except Exception as e:
                        with log.error('%s:',filename):
                            log.exception(e)
                        continue


                ext = tiddler_data.get('ext', 'tw')
                tags = tiddler_data.get('tags')
                ftype = 'tw'
                if 'stylesheet' in tags:
                    ext = 'css'
                    ftype = 'css'
                elif 'script' in tags:
                    ext = 'js'
                    ftype = 'js'
                elif 'Twine.image' in tags:
                    ext = tiddler_data.get('ext', 'png')
                    ftype = 'img'

                filepath = os.path.join(basepath, tiddler_data['id'] + '.' + ext)
                if filename_override is not None:
                    filepath = filename_override if os.path.isfile(filename_override) else os.path.join(basepath, filename_override)

                header = {
                    'id': tiddler_data['id'],
                    'type': ftype,
                    'tags': tiddler_data['tags'],
                    'path': os.path.relpath(filepath, self.root_dir)
                }
                for k,v in tiddler_data.items():
                    if k not in self.DO_NOT_INDEX:
                        header[k]=v
                data: bytes = b''
                with open(filepath, 'rb') as f:
                    data = f.read()
                content = ''
                try:
                    content = data.decode(header.get('encoding', 'utf-8'))
                    content=content.strip()
                except UnicodeDecodeError:
                    content = base64.b64encode(data).decode('ascii')
                    header['encoding']='b64'

                if 'nylon:strip-lines' in tags:
                    newtext = ''
                    for line in content.split('\n'):
                        newtext += line.strip()+'\n'
                    content = newtext
                    tags.remove('nylon:strip-lines')

                if 'nobr' in tags:
                    content = re.sub(r'[\r\n \t]+', ' ', content)

                fdata=[self.VERSION, header, content]
                findex = os.path.join(self.out_dir, self.data_path, tiddler_data['id']+index_ext)
                os_utils.ensureDirExists(os.path.dirname(findex))
                with io.StringIO() as f:
                    if self.language == 'json':
                        if self.indent:
                            json.dump(fdata, f, indent=2)
                        else:
                            json.dump(fdata, f, separators=(',',':'))
                    elif self.language == 'yaml':
                        if self.indent:
                            yaml.safe_dump_all(fdata, f, default_flow_style=False)
                        else:
                            yaml.safe_dump_all(fdata, f, default_flow_style=True)

                    with open(findex, 'wb') as of:
                        of.write(self.obfuscator.Obfuscate(f.getvalue().encode('utf-8')))

                index[tiddler_data['id']]=md5sum(findex)
        indexfile = os.path.join(self.out_dir, self.data_path, self.index_file)
        os_utils.ensureDirExists(os.path.dirname(indexfile))
        log.info('Writing %s...', indexfile)
        with open(indexfile, 'w') as f:
            if self.language == 'json':
                json.dump(index, f, separators=(',',':'))
            elif self.language == 'yaml':
                yaml.safe_dump(index, f, default_flow_style=True)
            else:
                log.critical('self.language = %r', self.language)
                sys.exit(1)

        #os_utils.single_copy(os.path.join(ROOT_DIR, 'scripts', 'game_server.py'), self.out_py, verbose=True)
        self._installGameServerFile(os.path.join(ROOT_DIR, 'scripts', 'game_server.py'), self.out_py)
        self.writeHTMLTo(os.path.join(self.out_dir, 'core', 'index.template.html'), tprops)
        try:
            import PyInstaller.__main__
            print(self.out_dir)
            print(self.out_py)
            PyInstaller.__main__.run([
                '--onefile',
                '--console',
                '--distpath', self.out_dir,
                '--workpath', os.path.join('tmp', os.path.basename(outbase)+'.pyi'),
                self.out_py
            ])
        except Exception as e:
            log.warning("Exception when running pyinstaller.")
            raise e


NylonBuilderFactory.Register(NylonFileBuilder)
