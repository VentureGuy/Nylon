import os, yaml, re
from nylon._twineimports import TiddlyWiki, TweeHeader, Tiddler, TWINE_DIR
from nylon.utils import img_to_data
from nylon.exceptions import NylonException, WarningAsError
from nylon.verifylexer import HeadlessVerifyLexer
from ._base import NylonBaseBuilder, NylonBuilderFactory
from buildtools import log
from buildtools.os_utils import get_file_list

TARGET_PATH = os.path.join(TWINE_DIR, 'targets')

# TODO: Load dynamically
AVAILABLE_TARGETS = {}

def installDefaultIncludedTargets():
    with open(os.path.join(TARGET_PATH, 'manifest.yml'), 'r') as f:
        AVAILABLE_TARGETS.update({k:os.path.join(TARGET_PATH, v) for k,v in yaml.safe_load(f).items()})

def addTargetFrom(target_id, dirname):
    '''
    Add defined template target to AVAILABLE_TARGETS.

    Example:
        nylon.addTargetFrom('sugarcube-2', os.path.join(TARGET_PATH, 'hacked-sugarcube-2'))
    '''
    AVAILABLE_TARGETS[target_id]=dirname

def get_all_story_files(root_dir):
    files= []
    for basefn in get_file_list(root_dir):
        filename = os.path.join(root_dir, basefn)
        if filename.endswith('.header.yml'):
            tiddler = None
            basepath = os.path.dirname(filename)
            tiddler_data = {}
            filename_override = None
            with open(filename, 'r') as f:
                try:
                    tiddler_data = yaml.safe_load(f)
                    tiddler = Tiddler('')
                    tiddler.title = tiddler_data['id']
                    tiddler.tags = tiddler_data['tags']
                    filename_override = tiddler_data.get('file', None)
                    tiddler.pos = [int(x) for x in tiddler_data.get('twine', {}).get('pos', '0,0').split(',')]
                    files.append(filename)
                except Exception as e:
                    log.error('%s:',filename)
                    log.error(e)
                    continue

            ext = tiddler_data.get('ext', 'tw')
            if 'stylesheet' in tiddler.tags:
                ext = 'css'
            elif 'script' in tiddler.tags:
                ext = 'js'
            elif 'Twine.image' in tiddler.tags:
                ext = tiddler_data.get('ext', 'png')
            filepath = os.path.join(basepath, tiddler.title + '.' + ext)
            if filename_override is not None:
                filepath = filename_override if os.path.isfile(filename_override) else os.path.join(basepath, filename_override)
            files.append(filepath)
    return files

class DummyApp(object):
    '''
    ¯\_(ツ)_/¯
    '''
    NAME = 'Nylon'
    VERSION = '0.2.0'
    def __init__(self):
        self.builtinTargetsPath = TARGET_PATH + os.sep

    def displayError(self, message, stacktrace=True):
        log.error(message)

class NylonLegacyBuilder(NylonBaseBuilder):
    ID = 'legacy'
    def __init__(self, root_dir, warn_as_error=False):
        if len(AVAILABLE_TARGETS.keys()) == 0:
            installDefaultIncludedTargets()
        self.tw = TiddlyWiki()
        self._target: str = ''
        self._target_path: str = ''
        self._target_base: str = ''
        super().__init__(root_dir)
        self.n_passages=0
        self.n_images=0
        self.n_stylesheets=0
        self.n_scripts=0
        self.n_headers=0

    def setTarget(self, target: str, target_path: str = None, target_base: str = None) -> None:
        self._target = target
        self._target_path:str = target_path or AVAILABLE_TARGETS[target]
        self._target_base:str = target_base or TARGET_PATH

    def LoadConfig(self, filename):
        #tw.storysettings = {}
        for k, v in self.config.get('story-settings', {}).items():
            self.tw.storysettings[k] = 'on' if v else 'off'
        self.setTarget(self.config.get('target', 'sugarcane'))
        self.outfile = self.config.get('output', 'BUILT.html')

    def addEmbeddedJS(self, filename, **kwargs):
        self.tw.addEmbeddedJS(filename, **kwargs)
    def linkJS(self, uri, **kwargs):
        self.tw.linkJS(uri, **kwargs)
    def addEmbeddedCSS(self, filename, **kwargs):
        self.tw.addEmbeddedCSS(filename, **kwargs)
    def linkCSS(self, uri, **kwargs):
        self.tw.linkCSS(uri, **kwargs)

    def _getOrder(self):
        order = []
        passageMap = {}
        self.n_passages=0
        self.n_images=0
        self.n_stylesheets=0
        self.n_scripts=0
        self.n_headers=0
        for basefn in get_file_list(self.root_dir):
            filename = os.path.join(self.root_dir, basefn)
            if filename.endswith('.header.yml'):
                tiddler = None
                basepath = os.path.dirname(filename)
                tiddler_data = {}
                with open(filename, 'r') as f:
                    try:
                        tiddler_data = yaml.safe_load(f)
                        tiddler = Tiddler('')
                        tiddler.title = tiddler_data['id']
                        tiddler.tags = tiddler_data['tags']
                        tiddler.pos = [int(x) for x in tiddler_data.get('twine', {}).get('pos', '0,0').split(',')]
                        if tiddler.title in passageMap:
                            log.warn("%s: Passage %s already exists (in %s)!", filename, tiddler.title, passageMap[tiddler.title])

                        passageMap[tiddler.title]=filename
                        self.n_headers+=1
                    except Exception as e:
                        log.error('%s:',filename)
                        log.error(e)
                        continue

                ext = tiddler_data.get('ext', 'tw')
                blob = False
                if 'stylesheet' in tiddler.tags:
                    ext = 'css'
                    self.n_stylesheets+=1
                elif 'script' in tiddler.tags:
                    ext = tiddler_data.get('ext', 'js')
                    self.n_scripts+=1
                elif 'Twine.image' in tiddler.tags:
                    ext = tiddler_data.get('ext', 'png')
                    blob = True
                    self.n_images+=1
                else:
                    self.n_passages+=1
                filepath = os.path.join(basepath, tiddler.title + '.' + ext)
                if not blob:
                    with open(filepath, 'r', encoding='utf-8-sig') as f:
                        for line in f:
                            tiddler.text += line
                    tiddler.text=tiddler.text.strip()
                    if 'nylon:strip-lines' in tiddler.tags:
                        newtext = ''
                        for line in tiddler.text.split('\n'):
                            newtext += line.strip()+'\n'
                        tiddler.text = newtext
                        tiddler.tags.remove('nylon:strip-lines')
                    if 'nobr' in tiddler.tags:
                        tiddler.text = re.sub(r'[\r\n \t]+', ' ', tiddler.text);
                else:
                    tiddler.text += img_to_data(filepath)
                self.tw.addTiddler(tiddler)
                tiddler.update()
                order.append(tiddler.title)
        return order

    def Compile(self):
        order=self._getOrder()
        hdr = TweeHeader.factory(self._target, self._target_path + os.sep, self._target_base+os.sep)

        with log.info('Scanning for errors...'):
            errors=0
            for key, tiddler in self.tw.tiddlers.items():
                for warning, replace in HeadlessVerifyLexer(tiddler, hdr, self.tw).check():
                    start='???'
                    end = '???'
                    sub = ''
                    if replace:
                        start, sub, end = replace
                    log.warn('%s[%s:%s]: Twee: %s', tiddler.title, start, end, warning)
                    errors += 1
            if errors > 0 and self.warn_as_error:
                raise WarningAsError('{} warnings handled as error (warn_as_error=True)'.format(errors))
                return False
            log.info('%d errors found.', errors)
        with log.info('Writing %s (legacy)...', self.outfile):
            if self.start_at != '' and self.start_at != 'Start':
                log.info('testplay=%r', self.start_at)
            #tw.additional_header_elements=self.additional_header_elements
            #def toHtml(self, app, header = None, order = None, startAt = '', defaultName = '', metadata = {}):
            output = self.tw.toHtml(DummyApp(), hdr, startAt=self.start_at)
            if output is None:
                return
            with open(self.outfile, 'w', encoding='utf-8-sig') as f:
                f.write(output)
            if not self.hide_stats:
                with log.info('Nylon Parsing Statistics:'):
                    log.info('Headers....: %d', self.n_headers)
                    log.info('Images.....: %d', self.n_images)
                    log.info('Passages...: %d', self.n_passages)
                    log.info('Scripts....: %d', self.n_scripts)
                    log.info('Stylesheets: %d', self.n_stylesheets)

NylonBuilderFactory.Register(NylonLegacyBuilder)
