'''
Not currently used.
'''
class Tale(object):
    def __init__(self):

    def LoadHTML(self):
        ALL_WRITTEN = []
        if not skip_unfuck:
            unfuckFile(compiled_file)
        parser = html.HTMLParser(remove_blank_text=True)
        xml = html.parse(compiled_file, parser)
        outfile_base = os.path.dirname(out_file)
        projectfile = os.path.join(outfile_base, 'story.yml')
        project = collections.OrderedDict([
            ('title', 'UNTITLED'),
            ('output', 'BUILT.html'),
            ('story-settings', {}),
        ])
        for element in xml.iter():
            element.tail = None
            if element.tag == 'title':
                project['title'] = element.text.strip()
            if element.tag == "div" and 'tags' in element.attrib:
                if element.attrib.get('tiddler', '') == 'StorySettings':
                    for line in element.text.split('\\n'):
                        if line.strip() == '':
                            continue
                        log.info(line)
                        k, v = line.strip().split(':')
                        project['story-settings'][k] = v == 'on'
                export_tiddler(outfile_base, element, skip_yml)
        with open(out_file, 'wb') as output:
            output.write(html.tostring(xml, pretty_print=True))
        ALL_WRITTEN.append(out_file)
        with open(projectfile, 'w') as f:
            ordered_dump(project, f, default_flow_style=False)
        ALL_WRITTEN.append(projectfile)
        return ALL_WRITTEN
