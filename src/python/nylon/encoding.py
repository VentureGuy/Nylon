'''
This entire file's purpose is to unfuck various encoding problems.
'''

import codecs
import logging
import os
import shutil

# https://gist.github.com/JakeWharton/981486

CRLF_OFFSET_ERROR = '\r\0\r\n\0'
CRLF_OFFSET_FIX = '\r\0\n\0'


def unfuckByteString(content=None, log=None):
    if not content:
        raise ArgumentException('Content must not be empty.')

    if content.startswith(codecs.BOM_UTF16):
        if log: log.info('Detected UTF-16 BOM.')

        if CRLF_OFFSET_ERROR in content:
            if log: log.error('Byte shift due to improper line ending conversion!')
            if log: log.info('Correcting line endings...')
            content = content.replace(CRLF_OFFSET_ERROR, CRLF_OFFSET_FIX)

        if log: log.info('Converting to UTF-8...')
        return content.decode("utf16").encode("utf8")

    if content.startswith(codecs.BOM_UTF8):
        if log: log.warn('Detected unneccessary UTF-8 BOM.')
        if log: log.info('Removing BOM...')

        return content[len(codecs.BOM_UTF8):]

    if log: log.info('No action required.')
    return content

def detect_encoding(filename, detect_only=False):
    '''
    My own ugly concoction for detecting encoding first via BOM and then via chardet.
    '''
    # Only import chardet when absolutely needed.
    import chardet, ftfy
    toread = min(32, os.path.getsize(filename))
    raw = b''
    with open(filename, 'rb') as f:
        raw = f.read(toread)

    # BOM-based detection.
    # -sig suffix means to load without the BOM included.
    encoding = 'utf-8-sig'
    if raw.startswith(codecs.BOM_UTF8):
        encoding = 'utf-8-sig'
    elif raw.startswith(codecs.BOM_UTF16):
        encoding = 'utf-16-sig'
    else:
        result = chardet.detect(raw)
        encoding = result['encoding']
        if encoding in ('utf-8', 'ascii'):
            encoding = 'utf-8-sig'

    if encoding in ('utf-8-sig', 'cp1252') and not detect_only:
        with codecs.open(filename, 'r', encoding=encoding) as inf:
            with codecs.open(filename + '.utf8', 'w', encoding='utf-8-sig') as outf:
                for line in ftfy.fix_file(inf, fix_entities=False, fix_latin_ligatures=False, fix_character_width=False, uncurl_quotes=False):
                    outf.write(line)
            shutil.move(filename + '.utf8', filename)
    return encoding


def unfuckFile(filepath, newpath=None, log=None):
    import ftfy
    if log is None:
        log = logging.getLogger(filepath)
    guessed_encoding = detect_encoding(filepath, True)
    success = False
    try_n = 0
    if newpath is None:
        newpath = filepath
    for enctry in [guessed_encoding, 'utf-8-sig', 'utf-8', 'cp1252']:
        try:
            if try_n <= 0:
                log.info('Unfucking %s (%s)...', filepath, enctry)
            else:
                log.info('%s...?', enctry)
            with codecs.open(filepath, 'r', enctry) as inf:
                with codecs.open(filepath + '.utf8', 'w', encoding='utf-8-sig') as outf:
                    for line in ftfy.fix_file(inf, encoding=enctry, fix_entities=False, fix_latin_ligatures=False, fix_character_width=False, uncurl_quotes=False):
                        outf.write(line)
            add_bom = False
            with open(filepath + '.utf8', 'rb') as f:
                add_bom = f.read(3) != codecs.BOM_UTF8
            if add_bom:
                with open(filepath + '.utf8b', 'wb') as outf:
                    outf.write(codecs.BOM_UTF8)
                    with open(filepath + '.utf8', 'rb') as inf:
                        outf.write(inf.read())
                shutil.move(filepath + '.utf8b', filepath + '.utf8')
            shutil.move(filepath + '.utf8', newpath)
            success = True
            break
        except UnicodeDecodeError:
            if try_n <= 0:
                log.error('Couldn\'t decode using %s! Trying another...', enctry)
                if hasattr(log, 'indent'):
                    log.indent += 1
            try_n += 1
            continue
    if try_n > 0 and hasattr(log, 'indent'):
        log.indent -= 1
    return success
