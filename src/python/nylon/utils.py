import random
import string
import codecs
import os
import re
import sys
import platform
import json
from typing import List
from buildtools import log, os_utils
from buildtools.maestro.utils import SerializableLambda
# Regular expression used to find instantiations of classes.
DETECT_NEW = re.compile(r'new ([A-Za-z0-9]+)')

# Regular expression used to detect color tags for colorize()
COLOR_SECTION = re.compile(r'<(red|green|yellow|blue|magenta|cyan|white)>([^<]+)</\1>')

REG_FUNCTION_DEF = re.compile(r'^(?:(?:window|global)\.)?(?P<name>[a-zA-Z0-9\$_\.]+) *= *\([^\)]+\) *(?:[=-])>$')

# Color stuff (ANSI color codes)
CONSOLE_COLORS = {
    'red':     '[31m',
    'green':   '[32m',
    'yellow':  '[33m',
    'blue':    '[34m',
    'magenta': '[35m',
    'cyan':    '[36m',
    'white':   '[37m',
}

def randomString(stringLength=10, alphabet: str = ''):
    """Generate a random string of fixed length """
    if alphabet == '':
        alphabet = string.ascii_letters+string.digits+' .,/:[]{}|!@#$%^&*()'
    return ''.join(random.choice(alphabet) for i in range(stringLength))


def colorize(txt):
    '''
    Color console output on Linux using ANSI control codes.

    Formatting:
    '<color>Colored text</color>'

    Color can be any of the keys presented in CONSOLE_COLORS.
    :param txt:
        Text to be colorized.
    :returns:
        Colorized text, or just plaintext on other platforms.
    '''

    # Yes, Python lets you put functions within functions.
    # This one is a callback used in sub.  m is the match result.
    def _colorize(m):
        # We can only use ANSI on Linux.  Windows CAN use ANSI, but requires a bunch of
        # bullshit most people won't bother with.
        if platform.system() == 'Linux':
            # Get the string in the "tag" and look it up in CONSOLE_COLORS.
            # Throws an error if it can't be found.
            color = CONSOLE_COLORS[m.group(1)]

            # Formats the text with the color start sequence, the captured string, and finally,
            # the reset sequence.
            return "\033{START}{CONTENT}\033[0m".format(START=color, CONTENT=m.group(2))\

        # Return plaintext on Windows/Mac/BSD
        return m.group(2)
    # END of _colorize().

    # Find regex matches, then replace them using the _colorize callback.
    return COLOR_SECTION.sub(_colorize, txt)


def changeExt(path, ext):
    '''
    Change the extension of a file.

    :param path:
        Filename/path to change the extension of.
    :param ext:
        New extension, period included.
    '''
    basename, _ = os.path.splitext(path)
    return basename + ext


def escapeCoffeeString(input):
    return input.replace("\"", "\\\"")

def img_to_data(path):
    """Convert a file (specified by a path) into a data URI."""
    if not os.path.exists(path):
        raise FileNotFoundError(path)
    mime, _ = mimetypes.guess_type(path)
    with open(path, 'rb') as fp:
        data64 = binascii.b2a_base64(fp.read())
    return 'data:%s;base64,%s' % (mime, data64.decode('ascii'))

def remove_bom(path, buffer_size=4096, bom_len=3):
    with open(path, "r+b") as fp:
        chunk = fp.read(buffer_size)
        if chunk.startswith(codecs.BOM_UTF8):
            i = 0
            chunk = chunk[bom_len:]
            while chunk:
                fp.seek(i)
                fp.write(chunk)
                i += len(chunk)
                fp.seek(bom_len, os.SEEK_CUR)
                chunk = fp.read(buffer_size)
            fp.seek(-bom_len, os.SEEK_CUR)
            fp.truncate()


def CoffeeAcquireAndDepSort(coffeedir, manual_load_order=[], additional_files=[], additional_depscanners={}):
    requires = {}
    ClassesRequired = {}
    ClassRegistry = {}
    coffee_files = []
    coffee_changed = False
    BUILTINS = []

    def scan_file(ClassRegistry, ClassesRequired, coffee_files, coffee_changed, path, outpath):
        requires[path] = []
        ClassesRequired[path] = []

        with codecs.open(path, 'r', encoding='utf-8-sig') as f:
            ln = 0
            for line in f:
                ln += 1
                oline = line
                line = line.strip()

                if line.endswith('# @depdetect: ignore'):
                    log.info('%s:%d: ignoring line.', path, ln)
                    continue

                # if line.startswith('#'): print(oline)
                if line.startswith('# %skip compile') or line.startswith('# @depdetect: skip-compile'):
                    log.info('%s:%d: Skipped (skip compile)', path, ln)
                    return False

                if line.startswith('# @depdetect:'):
                    dtchunks=line.split(':', 1)
                    if len(dtchunks) != 2:
                        log.error('%s:%d: Ignoring directives, invalid @depdetect structure.', path, ln)
                        return False
                    for directive in dtchunks[1].strip().split(';'):
                        directive=directive.strip()
                        args = directive.split(' ')
                        command=args[0]
                        args=args[1:] if len(args)>1 else []
                        if command in ('classdef', 'enumdef'):
                            className = args[0]
                            log.debug("Found class %s in %s", className, path)
                            ClassRegistry[className] = path
                            if len(args)==3 and args[1] == 'extends':
                                superClass=args[2]
                                if superClass not in BUILTINS:
                                    ClassesRequired[path] += [superClass]
                                    log.debug('%s requires class %s', path, superClass)
                        if command in ('classref', 'enumref'):
                            superClass = args[0]
                            if superClass not in BUILTINS:
                                ClassesRequired[path] += [superClass]
                                log.debug('%s requires class %s', path, superClass)

                        if command == 'funcdef':
                            className = 'f:'+args[0]
                            log.debug("Found function %s in %s", args[0], path)
                            ClassRegistry[className] = path

                        if command in ('detect', 'recognize'):
                            # detect function blah by regex
                            regType=args[0]
                            regName=args[1]
                            regex = ' '.join(args[3:])
                            prefix = ''
                            if regType=='function':
                                prefix='f:'
                            elif regType in ('class', 'enum'):
                                prefix=''
                            else:
                                log.error('%s:%d: Ignoring directives, invalid @depdetect %s structure. (regType is invalid)', path, ln, command)
                                return False
                            clsID = prefix+regName.strip()
                            additional_depscanners[regex.strip()]=clsID
                            ClassRegistry[className] = clsID
                            log.debug('Will now recognize %r as %s %s', regex, regType, regName)
                    continue
                if line.startswith('# @enumdef:'):
                    dtchunks=line.split(':')
                    if len(dtchunks) != 2:
                        log.error('%s:%d: Ignoring directives, invalid @enumdef structure.', path, ln)
                    for enumName in dtchunks[1].strip().split(';'):
                        className=enumName.strip()
                        log.debug("Found enum %s in %s", className, path)
                        ClassRegistry[className] = path
                    continue
                if line.startswith('# @enumref:'):
                    dtchunks=line.split(':')
                    if len(dtchunks) != 2:
                        log.error('%s:%d: Ignoring directives, invalid @depdetect structure.', path, ln)
                    for enumName in dtchunks[1].strip().split(';'):
                        superClass=enumName.strip()
                        if superClass not in BUILTINS:
                            ClassesRequired[path] += [superClass]
                            log.debug('%s requires enum %s', path, superClass)
                    continue

                if line.startswith('class '):
                    classParts = line.split(' ')
                    className = classParts[1]
                    ClassRegistry[className] = path
                    log.debug("Found class %s in %s", className, path)
                    if 'extends' in line:
                        superClass = classParts[3]
                        if superClass not in BUILTINS:
                            ClassesRequired[path] += [superClass]
                            log.debug('%s requires class %s', path, superClass)

                m = REG_FUNCTION_DEF.match(line)
                if m is not None:
                    funcname = m.group('name')
                    ClassRegistry['f:'+funcname] = path
                    log.debug("Found function %s in %s", funcname, path)

                if line.startswith('# @classref:'):
                    dtchunks=line.split(':')
                    if len(dtchunks) != 2:
                        log.error('%s:%d: Ignoring directives, invalid @classref structure.', path, ln)
                    else:
                        className = dtchunks[1].strip()
                        if className not in BUILTINS:
                            ClassesRequired[path] += [className]
                            log.debug('%s requires enum %s', path, className)

                for expr, superClass in additional_depscanners.items():
                    m = re.search(expr, line)
                    if m is not None:
                        # function: functionSignature
                        if superClass.startswith('function:') or superClass.startswith('f:'):
                            _, funcName = superClass.split(':', 1)
                            ClassesRequired[path] += [f'f:{funcName.strip()}']
                            log.debug('%s requires function %s', path, funcName)
                        elif superClass.startswith('class:'):
                            _, clsName = superClass.split(':', 1)
                            if clsName not in BUILTINS:
                                ClassesRequired[path] += [clsName]
                                log.debug('%s requires class %s', path, superClass)
                        else:
                            if superClass not in BUILTINS:
                                ClassesRequired[path] += [superClass]
                                log.debug('%s requires class %s', path, superClass)

                m = DETECT_NEW.search(line)
                if m is not None:
                    superClass = m.group(1)
                    if superClass not in BUILTINS:
                        ClassesRequired[path] += [superClass]
                        log.debug('%s requires class %s', path, superClass)

                if line.startswith('# requires:'):
                    filename = os.path.normpath(os.path.join(coffeedir, line[11:].strip()))
                    requires[path] += [filename]
                    with log.debug('%s requires file %s', path, filename):
                        if not os.path.isfile(filename):
                            log.error("%s:%r: File %r doesn't exist!", path, line, filename)
                            sys.exit(1)

        # if os_utils.canCopy(path,outpath):
        #    #os_utils.cmd(['coffee','-bc','--output',os.path.dirname(outpath),path],critical=True,echo=True,show_output=True)
        #    coffee_compile(args,path,os.path.dirname(outpath))
        #    coffee_changed=True
        coffee_files += [path]
        # return coffee_changed
        #import yaml
        #with open('DEP_TREE.yml', 'w') as w:
        #    yaml.dump(ClassesRequired, w)
        return True

    DATA_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..', 'data'))
    BUILTINS = []
    for filename in os.listdir(os.path.join(DATA_DIR, 'builtins')):
        fn = os.path.join(DATA_DIR, 'builtins', filename)
        if os.path.isfile(fn) and fn.endswith('.json'):
            with open(fn, 'r') as f:
                BUILTINS += json.load(f)['this']
    BUILTINS = list(set(BUILTINS))

    with log.info('Acquiring files...'):
        for root, dirs, files in os.walk(coffeedir):
            with log.info('Entering %s', root):
                for filename in files:
                    if filename.endswith('.coffee'):
                        path = os.path.join(root, filename)
                        #out_path = changeExt(path.replace('src'+os.sep,'tmp'+os.sep),'.js')
                        out_path = ''
                        # out_dir=os.path.dirname(out_path)
                        # os_utils.ensureDirExists(out_dir)
                        if path in manual_load_order:
                            continue
                        # if os_utils.canCopy(path,out_path):
                        #    coffee_compile(args,path,out_dir)
                        #    coffee_changed=True
                        if scan_file(ClassRegistry, ClassesRequired, coffee_files, coffee_changed, path, out_path):
                            coffee_changed = True
    for path in additional_files:
        coffee_changed = scan_file(ClassRegistry, ClassesRequired, coffee_files, coffee_changed, path, '')
    with log.info('Resolving classes...'):
        for path, classlist in ClassesRequired.items():
            for requirement in classlist:
                if requirement not in ClassRegistry:
                    if requirement.startswith('f:'):
                        continue
                    log.warn('Unknown class %s instantiated in %s', requirement, path)
                    continue
                requirementFile = ClassRegistry[requirement]
                if requirementFile not in requires[path] and requirementFile != path:
                    requires[path] += [requirementFile]
                    log.debug('%s: Class %s resolves to %s.', path, requirement, requirementFile)
    with log.info('Sorting dependencies...'):
        ordered_files = []
        unordered_files = [] + sorted(coffee_files)
        passnum = 0
        while len(unordered_files) > 0:
            passnum += 1
            for path in unordered_files:
                defer = False
                if len(requires[path]) > 0:
                    for requirement in requires[path]:
                        if requirement not in coffee_files:
                            log.error("%s: Required file %s doesn't exist!", path, requirement)
                            sys.exit(1)
                        if requirement not in ordered_files:
                            defer = True
                            #print(path+': DEFER: '+requirement)
                            break
                if not defer:
                    ordered_files += [path]
                    unordered_files.remove(path)
                    if False:  # args.verbose:
                        with log.info('%d: %s', passnum, path):
                            for requirement in requires[path]:
                                log.info(requirement)
        coffee_files = ordered_files
    return coffee_files

class CoffeeAcquireFilesLambda(SerializableLambda):
    YAML_TAG='!coffeefiles'

    def __init__(self, coffeedir, manual_load_order=[], additional_files=[], additional_depscanners={}, files_before: List[str] = [], files_after: List[str] = []):
        self.coffeedir=coffeedir
        self.manual_load_order=manual_load_order
        self.additional_files=additional_files
        self.additional_depscanners=additional_depscanners
        self.files_before=files_before
        self.files_after=files_after


    def __call__(self):
        return self.files_before+\
        CoffeeAcquireAndDepSort(
            coffeedir=self.coffeedir,
            manual_load_order=self.manual_load_order,
            additional_files=self.additional_files,
            additional_depscanners=self.additional_depscanners)+\
        self.files_after


ALPHA_LOWER = 'abcdefghijklmnopqrstuvwxyz'
ALPHA_UPPER = ALPHA_LOWER.upper()
ALPHA = ALPHA_LOWER+ALPHA_UPPER
NUMERIC = '0123456789'
ALPHANUM = ALPHA + NUMERIC

def CamelCasify(inputstr, keep_caps=True):
    o = ''
    inWord = False
    for c in inputstr:
        if not inWord:
            if c in ALPHA:
                inWord = True
                o += c.upper()
            else:
                inWord = False
                continue
        else:
            if c not in ALPHA:
                inWord = False
                continue
            elif c in ALPHA_UPPER and keep_caps:
                o += c.upper()
            else:
                o += c.lower()
    return o
