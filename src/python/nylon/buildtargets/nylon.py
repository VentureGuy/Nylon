import os
import sys
import yaml
from datetime import datetime
from buildtools import log, os_utils
from buildtools.maestro.base_target import SingleBuildTarget
from nylon.builders import NylonBuilderFactory
from nylon.builders.legacy import get_all_story_files

from bs4.element import Tag

class GenNylonHeaderTarget(SingleBuildTarget):
    BT_TYPE = "GenNylonHeader"
    BT_LABEL = 'HEADER'
    def __init__(self, target, name, tags, pos=[0, 0], ext=None, dependencies=[]):
        super(GenNylonHeaderTarget, self).__init__(target, dependencies=dependencies, files=[os.path.abspath(__file__)])
        self.passage_name = name
        self.tags = tags
        self.ext = ext
        self.pos = str(pos[0]) + ',' + str(pos[1])

    def get_config(self):
        return (self.passage_name, self.tags, self.pos, self.ext)

    def build(self):
        os_utils.ensureDirExists(os.path.dirname(self.target), noisy=True)
        with open(self.target, 'w') as f:
            header = {
                'id': self.passage_name,
                'created': datetime.strftime(datetime.now(), '%Y%m%d%H%M'),
                'modifier': 'twee',
                'tags': self.tags,
                'twine': {
                    'pos': self.pos
                }
            }
            if self.ext is not None:
                header['ext']=self.ext
            yaml.dump(header, f, default_flow_style=False)


class BuildNylonProjectTarget(SingleBuildTarget):
    BT_TYPE='CompileNylonStory'
    BT_LABEL='NYLON'

    def __init__(self, builder: str, target: str, project_file: str, warn_as_error=False, dependencies=[]):
        self.project_file = project_file
        self.all_files = []

        with log.info('Configuring Nylon BuildTarget...'):
            self.all_files = get_all_story_files(os.path.dirname(project_file))
        super(BuildNylonProjectTarget, self).__init__(target, dependencies=dependencies, files=[self.project_file]+self.all_files)
        self.nylon_builder = NylonBuilderFactory.Create(builder, os.path.dirname(self.project_file), warn_as_error=warn_as_error)

    def get_config(self):
        return [self.project_file]

    def is_stale(self):
        return self.checkMTimes(self.files+self.dependencies, self.provides(), config=self.get_config())

    def addEmbeddedJS(self, filename, **kwargs):
        self.nylon_builder.addEmbeddedJS(filename, **kwargs)
    def linkJS(self, uri, **kwargs):
        self.nylon_builder.linkJS(uri, **kwargs)
    def addEmbeddedCSS(self, filename, **kwargs):
        self.nylon_builder.addEmbeddedCSS(filename, **kwargs)
    def linkCSS(self, uri, **kwargs):
        self.nylon_builder.linkCSS(uri, **kwargs)
    def setStartPassage(self, passage_id):
        self.nylon_builder.start_at = passage_id


    def setTarget(self, target_path):
        '''
        Useful for building with Sugarcube 2 without modding Twine itself.
        :param target_path: Path of the target directory.
        '''
        self.nylon_builder.target_path = target_path

    def build(self):
        self.nylon_builder.outfile = self.target
        self.nylon_builder.Compile()

class CleanSugarTarget(SingleBuildTarget):
    BT_TYPE='CleanSugar'
    BT_LABEL='CLEAN'
    def __init__(self, target, filename, dependencies=[], para_style='none', strip_nobr=False, split_conditions=False):
        self.infile=filename

        self.para_style=para_style
        self.strip_nobr=strip_nobr
        self.split_conditions=split_conditions
        super(CleanSugarTarget, self).__init__(target, dependencies=dependencies, files=[filename,os.path.abspath(__file__)])

    def get_config(self):
        return [self.para_style,self.strip_nobr,self.split_conditions]

    def build(self):
        cmd = [sys.executable, 'prettify_passage.py']
        cmd += ['-p='+self.para_style]
        if self.strip_nobr: cmd += ['--strip-nobr']
        if self.split_conditions: cmd+=['--split-conditions']
        cmd += [self.infile,self.target]
        os_utils.cmd(cmd, echo=True, critical=True, show_output=False)
