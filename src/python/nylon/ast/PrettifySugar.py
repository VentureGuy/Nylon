import codecs
import re
from enum import IntEnum
from buildtools import log
from buildtools.bt_logging import NullIndenter
from nylon.ast import nodes
from buildtools.indentation import IndentWriter

from slimit4twine import ast
from slimit4twine.visitors import nodevisitor, ecmavisitor
REG_COMP = re.compile(r'\b(eq|neq|is|isnt|lt|lte|gt|gte)\b')
COMP_TRANS = {
    'eq':   '==',
    'neq':  '!=',
    'is':   '==',
    'isnt': '!=',
    'lt':   '<',
    'lte':  '<=',
    'gt':   '>',
    'gte':  '>='
}


def _fix_comps(m):
    return COMP_TRANS[m.group(1)]


class EParaStyle(IntEnum):
    NONE = 0
    P = 1
    BR = 2


class PrettifySugar(object):

    def __init__(self, _ast):
        self.ast = _ast
        self.out = None
        self.split_conditions = False
        self.paragraph_style = EParaStyle.NONE
        self.strip_nobr = False
        self._buffer = ''
        self._inLineNode = False

    def prettifyToFile(self, tofilename, encoding='utf-8-sig', indentchars='\t', split_conditions=False, strip_nobr=False, paragraph_style=EParaStyle.NONE):
        if split_conditions:
            log.warn('split_conditions=True will result in a passage Sugarcane cannot parse.')
        with codecs.open(tofilename, 'w', encoding=encoding) as f:
            self.prettifyToHandle(f, indentchars=indentchars, split_conditions=split_conditions, strip_nobr=strip_nobr, paragraph_style=paragraph_style)

    def prettifyToHandle(self, f, indentchars='\t', split_conditions=False, strip_nobr=False, paragraph_style=EParaStyle.NONE):
        self.prettifyToIndenter(IndentWriter(f, indent_chars=indentchars), split_conditions=split_conditions, strip_nobr=strip_nobr, paragraph_style=paragraph_style)

    def prettifyToIndenter(self, out, split_conditions=False, strip_nobr=False, paragraph_style=EParaStyle.NONE):
        self.split_conditions = split_conditions
        self.strip_nobr = strip_nobr
        self.paragraph_style = paragraph_style
        self.out = out
        # Standardize strings.
        # And yes, this DOES have to be done here.
        for child in self.ast.GetAllChildren():
            if isinstance(child, nodes.TweeTextNode):
                #child.value = re.sub(r'\\\n',   '\n'   ,child.value)
                child.value = re.sub(r'\\\r?\n', '', child.value)
                child.value = re.sub(r'[ \t]+',  ' ', child.value)
                child.value = re.sub(r'[\r\n]',  '\n', child.value)
                child.value = re.sub(r'\n{3,}',  '\n\n', child.value)
                if '\n' in child.value and (child.value.strip() == '' or child.value.strip() == '\\'):
                    if child in child.parent._children_list:
                        child.parent._children_list.remove(child)
        for child in self.ast.children():
            self.write(child)

    def replaceVars(self, string):
        #o=REG_COMP.sub(_fix_comps, string)
        return string

    def _writeline(self, string):
        return self.out.writeline(string)

    def _flush(self):
        self.out.writeline(self._buffer)
        self._buffer = ''

    def write(self, child):
        #print(repr(child))
        if isinstance(child, nodes.TweeIfNode):
            for ifbn in child.children():
                condition = ''
                tree = nodes.fixCompsInTree(ifbn.condition) if ifbn.condition is not None else ast.Null('')
                condition = nodes.treeToString(tree)
                label = ''
                if ifbn.tagName == 'if':
                    label = 'if '
                elif ifbn.tagName == 'elseif':
                    label = 'elseif '
                elif ifbn.tagName == 'else':
                    label = 'else'
                    condition = ''
                proposed = '<<{}{}>>'.format(label, condition)
                if self.split_conditions:
                    prop_split = re.sub(r'\) (and|or) \(', ') \g<1>\\\n (', proposed).split('\n')
                    with self._writeline(prop_split[0]):
                        if len(prop_split) > 1:
                            for splitcond in prop_split[1:]:
                                self._writeline(splitcond)
                        for grandchild in ifbn.children():
                            self.write(grandchild)
                else:
                    with self._writeline(proposed):
                        for grandchild in ifbn.children():
                            self.write(grandchild)
                if ifbn == child.children()[-1]:
                    self._writeline('<<endif>>')

        elif isinstance(child, nodes.TweeSetNode):
            #print(repr(child))
            for el in child.source_elements:
                #print(el.to_ecma())
                #if isinstance(child, ast.Node):
                #    nodes.dump_slimit_struct(child.source_elements)
                self._writeline('<<set {}>>'.format(nodes.treeToString(el)))

        elif isinstance(child, nodes.TweeNoBRNode):
            if len(child.children()) > 0:
                if self.strip_nobr:
                    for grandchild in child.children():
                        self.write(grandchild)
                else:
                    with self._writeline('<<nobr>>'):
                        for grandchild in child.children():
                            self.write(grandchild)
                    self._writeline('<<endnobr>>')

        elif isinstance(child, nodes.TweePrintNode):
            self._writeline('<<print {}>>'.format(child.expression.to_ecma()))

        elif isinstance(child, nodes.TweeTextNode):
            o = self.replaceVars(' '.join(child.value))
            if o == '' or o == '\\\n':
                return
            lastTextEmitted = None
            for chunk in child.value.split('\n'):
                tooutput = chunk.strip()
                if tooutput == '':
                    continue
                if tooutput != '' or (lastTextEmitted != '' and self.paragraph_style == EParaStyle.NONE):
                    if self.paragraph_style == EParaStyle.P:
                        tooutput = '<p>{}</p>'.format(tooutput)
                    if self.paragraph_style == EParaStyle.BR:
                        tooutput = '{}<br>'.format(tooutput)
                    self._writeline(tooutput)
                    lastTextEmitted = chunk.strip()

        elif isinstance(child, nodes.TweeLineNode):
            found = False
            for _child in child.GetAllChildren():
                if isinstance(_child, nodes.TweeTextNode) and _child.value.strip() != '':
                    found = True
            self._inLineNode = found
            for _child in child.children():
                self.write(_child)
            if found:
                if self.paragraph_style == EParaStyle.P:
                    self._buffer = '<p>{}</p>'.format(self._buffer)
                if self.paragraph_style == EParaStyle.BR:
                    self._buffer = '{}<br>'.format(self._buffer)
                #self._flush()
                self._inLineNode = False

        elif isinstance(child, nodes.TweeDisplayNode):
            self._writeline('<<display {}>>'.format(child.args))

        elif isinstance(child, nodes.TweeButtonNode):
            self._writeline('<<button {}>>'.format(child.link))

        elif isinstance(child, nodes.TweeWikiLinkNode):
            self._writeline(str(child))

        elif isinstance(child, nodes.TweeCommentNode):
            self._writeline('{}'.format(child.value.strip()))

        elif isinstance(child, nodes.TweeAddTagNode):
            self._writeline('<<addtag {}>>'.format(child.expr.to_ecma()))

        elif isinstance(child, nodes.TweeToggleTagNode):
            self._writeline('<<toggletag {}>>'.format(child.expr.to_ecma()))

        elif isinstance(child, nodes.TweeRemoveTagNode):
            self._writeline('<<removetag {}>>'.format(child.expr.to_ecma()))

        elif isinstance(child, nodes.TweeTextInputNode):
            self._writeline('<<textinput {}>>'.format(child.identifier.to_ecma()))

        elif isinstance(child, nodes.TweeRadioNode):
            expr = '' if child.expr is None else child.expr.to_ecma()
            identifier = '' if child.identifier is None else child.identifier.to_ecma()
            self._writeline('<<radio {} {}>>'.format(identifier, expr))

        elif isinstance(child, nodes.TweeNewlineNode):
            return
        else:
            self._writeline('/% PrettifySugar: UNHANDLED TYPE {} %/'.format(type(child)))
            self._writeline(repr(child))
