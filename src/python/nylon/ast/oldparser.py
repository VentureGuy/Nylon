from twinehack.ast.enums import EParserEventResponse, EParserEventType, EParserState, ParserEvent, TweeEvent
from twinehack.ast.nodes import *
import codecs
from buildtools import log

class Passage(object):

    def __init__(self):
        self.Title = ''
        self.Contents = []




WHITESPACE='\t \r\n'
class PassageParser(object):
    KNOWN_TAG_NODES = {
        'if': TweeIfNode,
        'nobr': TweeNoBRNode,
        'set': TweeSetNode,
        'display': TweeDisplayNode,
        'print': TweePrintNode,
        'button': TweeButtonNode
    }
    def __init__(self):
        self.passage = None

        self._buffer = ''
        self._inString = False
        self._inComment = False
        self._currentNode = None
        self._line = 1
        self._column = 0
        self._filename = None
        self._nodeState = None
        self._skip_until_end = False

    def reset(self):
        self.passage = TweeSupertagNode()
        self.passage.tagName = '__passage__'

        self._buffer = ''
        self._inString = False
        self._inComment = False
        self._currentNode = self.passage
        self._line = 1
        self._column = 0
        self._filename = None
        self._nodeState = EParserState.DEFAULT
        self._skip_until_end = False

    def parseFile(self, filename):
        self._filename = filename
        self.reset()
        with codecs.open(filename, 'r', encoding='utf-8-sig') as f:
            while True:
                c = f.read(1)
                if not c:
                    #self.dump(self.passage)
                    return
                self._handle_char(c)

    def dump(self, node):
        with log.debug(type(node).__name__+": "+repr(node)):
            if isinstance(node, (TweeIfNode)):
                for n in node.children():
                    self.dump(n)
                for n in node._children_list:
                    self.dump(n)
            elif isinstance(node, (TweeSupertagNode, TweeIfBlockNode)):
                for n in node._children_list:
                    self.dump(n)
            else:
                log.warn('UNHANDLED TYPE %s', type(node).__name__)
                #log.warn(dir(node))
                #for n in node._children_list:
                #    self.dump(n)


    def emitTextNode(self, string):
        tn = TweeTextNode()
        tn.FromParser(self)
        tn.value=string
        self.emitEvent(EParserEventType.CONTENT, node=tn)
    def emitCommentNode(self, string):
        tn = TweeCommentNode()
        tn.FromParser(self)
        tn.value=string
        self.emitEvent(EParserEventType.CONTENT, node=tn)

    def emitEvent(self, etype, **kwargs):
        e = TweeEvent(etype, **kwargs)
        return self._currentNode.ReceiveParserEvent(e)

    def emitEOL(self):
        if self._nodeState == EParserState.DEFAULT and self._currentNode.hasTextNode():
            line = TweeLineNode()
            for child in list(self._currentNode._children_list):
                if not isinstance(child, TweeLineNode):
                    line._children_list.append(child)
                    child.parent = line
                    self._currentNode._children_list.remove(child)
            self._currentNode._children_list.append(line)


    def createSupertag(self, name):
        st = None
        if name in PassageParser.KNOWN_TAG_NODES:
            st = self.KNOWN_TAG_NODES[name]()
        else:
            st = TweeSupertagNode()
            st.tagName = name
            st.self_closing=True
        st.FromParser(self)

        st.parent = self._currentNode
        self._currentNode=st

    def _lastChar(self):
        if len(self._buffer) > 0:
            return self._buffer[-1]
        return ''

    def isStop(self, response):
        return (response & EParserEventResponse.STOP) == EParserEventResponse.STOP

    def isSkip(self, response):
        return (response & EParserEventResponse.SKIP) == EParserEventResponse.SKIP

    def _handle_char(self, c):
        if c == '\n':
            self._line+=1
            self._column=0
        self._column += 1
        log.info('st8=%s, c=%r, lc=%r', self._nodeState, c, self._lastChar())
        if self._inComment:
            # %/
            if c == '/' and self._lastChar() == '%':
                self._inComment = False
                self.emitCommentNode(self._buffer[:-1])
                self._buffer = ''
                log.debug('END COMMENT')
                return
            self._buffer += c
            return
        else:
            # /%
            if c == '%' and self._lastChar() == '/':
                self._inComment = True
                self._buffer = ''
                log.debug('START COMMENT')
                return
        if self._currentNode is not None:
            if self._inComment:
                if c == '"' and self._lastChar() != '\\':
                    self._inComment = False
                    #self.emitTextNode(self._buffer[:-1])
                    #self._buffer = ''
                    log.debug('END STRING')
                    #return
                self._buffer += c
                return
            else:
                if c == '"':
                    self._inComment = True
                    self._buffer = '"'
                    log.debug('START STRING')
                    return
        if c == '\n':
            if self._lastChar() != '\\':
                self.emitEOL()
        '''
        if c == '"' and self._lastChar() == '\\':
            if self._inString:
                self._inString = False
                self.emitTextNode(self._buffer[:-1])
                self._buffer = ''
                log.debug('END STRING')
            elif self._currentNode is not None:
                self._inString = True
                self._buffer = ''
                log.debug('START STRING')
        '''
        if self._nodeState == EParserState.DEFAULT:  # Not in a node
            if c == '<' and self._lastChar() == '<' and not self._inString:
                self.emitTextNode(self._buffer[:-1])
                self._buffer = ''
                self._nodeState = EParserState.PARSING_NAME
                log.debug('PARSING_NAME')
                return
            self._buffer += c
        immediate_end = False
        if self._nodeState == EParserState.PARSING_NAME:
            immediate_end = (c == '>' and self._lastChar() == '>') and not self._inString
            if c in WHITESPACE or immediate_end:
                if immediate_end:
                    self._buffer=self._buffer[:-1]
                response = self.emitEvent(EParserEventType.SUPERTAG_START, tag=self._buffer)
                log.debug('got %r', response)
                if self.isStop(response):
                    finished = self._currentNode
                    self._currentNode = self._currentNode.parent
                    log.debug('_currentNode == %r',self._currentNode)
                    self.emitEvent(EParserEventType.CONTENT, node=finished)
                    #self._skip_until_end = True
                    self._buffer=''
                    self._nodeState=EParserState.DEFAULT
                    return

                if not self.isSkip(response):
                    self.createSupertag(self._buffer)
                    self.emitEvent(EParserEventType.SUPERTAG_START, tag=self._buffer)
                    #self._skip_until_end = True
                    self._buffer=''

                self._buffer=''
                self._nodeState = EParserState.PARSING_ARGS
                log.debug('PARSING_ARGS')

            self._buffer += c
        if self._nodeState == EParserState.PARSING_ARGS or immediate_end:
            if immediate_end or (c == '>' and self._lastChar() == '>' and not self._inString):
                response = EParserEventResponse.CONTINUE
                if not self._skip_until_end:
                    response = self.emitEvent(EParserEventType.ARGUMENTS, args=self._buffer[:-1].split())
                else:
                    self._skip_until_end = False
                log.debug('got %r', response)
                if self.isStop(response):
                    finished = self._currentNode
                    self._currentNode = self._currentNode.parent
                    log.debug('_currentNode == %r',self._currentNode)
                    self.emitEvent(EParserEventType.CONTENT, node=finished)
                    #self._skip_until_end = True
                    self._buffer=''
                    self._nodeState=EParserState.DEFAULT
                    return
                self._buffer = ''
                self._nodeState = EParserState.DEFAULT
                log.debug('DEFAULT')
                return
            self._buffer += c
