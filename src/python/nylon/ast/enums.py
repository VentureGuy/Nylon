from enum import auto, IntEnum, IntFlag


class EParserEventType(IntEnum):
    # <<if
    SUPERTAG_START = auto()
    # hurr == durr>>
    ARGUMENTS = auto()
    # Everything inside this tag. INCLUDING end tag.
    CONTENT = auto()


class EParserEventResponse(IntFlag):
    STOP = 1
    SKIP = 2
    ERROR = 4

    CONTINUE = 0
    STOP_AND_SKIP = STOP | SKIP


class ParserEvent(object):

    def __init__(self, _type, **kwargs):
        self.type = _type
        self.args = kwargs


class TiddlyEvent(object):

    def __init__(self, t, **args):
        self.type = t
        self.args = args


class EParserState(IntEnum):
    DEFAULT = auto()
    PARSING_NAME = auto()
    PARSING_ARGS = auto()
