import codecs, re
from nylon.ast import nodes
from buildtools.indentation import IndentWriter
from slimit4twine import ast
from slimit4twine.visitors import nodevisitor

class Sugar2Coffee(object):

    def __init__(self, ast, **kwargs):
        '''
        :param ast:
            AST node
        :param emit_nobr:
            Whether to emit <<nobr>> (False)
        :param emit_newlines:
            Emit newlines? (True)
        :param emit_escaped_newlines:
            Emit escaped newlines? (False)
        '''
        self.ast = ast
        self.out = None

        self.config=kwargs

    def transcodeToFile(self, tofilename, encoding='utf-8-sig', indentchars='\t'):
        with codecs.open(tofilename, 'w', encoding=encoding) as f:
            self.transcodeToHandle(f, indentchars=indentchars)

    def transcodeToHandle(self, f, indentchars='\t'):
        self.transcodeToIndenter(IndentWriter(f, indent_chars=indentchars))

    def transcodeToIndenter(self, out):
        self.out = out
        for child in nodes.prepTreeForCoffee(self.ast).children():
            self.write(child)

    def writeToIndenter(self, out):
        self.out=out
        self.write(nodes.prepTreeForCoffee(self.ast))

    def write(self, child):
        if child is None:
            return
        if isinstance(child, nodes.TweeIfNode):
            for ifbn in child.children():
                label = ''
                condition = nodes.treeToString(nodes.prepTreeForCoffee(ifbn.condition)) if ifbn.condition is not None else ''
                if ifbn.tagName == 'if':
                    label = 'if '
                elif ifbn.tagName == 'elseif':
                    label = 'else if '
                elif ifbn.tagName == 'else':
                    label = 'else'
                with self.out.writeline(label + condition):
                    for grandchild in ifbn.children():
                        self.write(grandchild)

        elif isinstance(child, nodes.TweeSetNode):
            for node in child:
                self.write(node)

        elif isinstance(child, ast.ExprStatement):
            self.write(child.expr)

        elif isinstance(child, ast.Node):
            self.out.writeline(nodes.treeToString(child))

        elif isinstance(child, nodes.TweeNoBRNode):
            for grandchild in child.children():
                self.write(grandchild)

        elif isinstance(child, nodes.TweePrintNode):
            t = child.expression
            t = nodes.fixCompsInTree(t)
            #t = nodes.twinifyVariables(t)
            o = nodes.treeToString(t).replace('\r\n','\n')
            if o.strip() in ('','\\'): return
            o = o.replace('\n','\\n')
            self.out.writeline('window.twinePrint \'"\'+{}+\'"\''.format(o))

        elif isinstance(child, nodes.TweeTextNode):
            o = child.value.replace("\r\n",'\n')
            if o.strip() in ('','\\'): return
            o = o.replace('\n','\\n')
            self.out.writeline('window.twinePrint {!r}'.format("\""+o+"\""))

        elif isinstance(child, nodes.TweeCommentNode):
            value = child.value[2:-2]
            for line in value.split('\n'):
                self.out.writeline('#'+line.replace('###','# # #'))

        elif isinstance(child, nodes.TweeDisplayNode):
            self.out.writeline('window.twineDisplayInline {!r}'.format(child.args))

        elif isinstance(child, nodes.TweeAddTagNode):
            self.out.writeline('window.twineCallMacro "addtag", [{!r}]'.format(child.expr))

        elif isinstance(child, nodes.TweeRemoveTagNode):
            self.out.writeline('window.twineCallMacro "removetag", [{!r}]'.format(child.expr))

        elif isinstance(child, nodes.TweeToggleTagNode):
            self.out.writeline('window.twineCallMacro "toggletag", [{!r}]'.format(child.expr))

        elif isinstance(child, nodes.TweeButtonNode):
            args = []
            if child.link.label is not None:
                args += ['"'+child.link.label+'"']
            if child.link.passage is not None:
                args += [child.link.passage.to_ecma()]
            if child.link.callback is not None:
                args += ['->']
            with self.out.writeline('twineButton '+(', '.join(args))):
                if child.link.callback is not None:
                    for stmt in child.link.callback:
                        self.out.writeline(stmt.to_ecma().strip(';'))

        elif isinstance(child, nodes.TweeLineNode):
            #self.out.writeline('# LINE START')
            for grandchild in child.children():
                self.write(grandchild)
            #self.out.writeline('# LINE END')

        elif isinstance(child, nodes.TweeNewlineNode):
            if self.config.get('emit_newlines',True):
                if child.value == '\n':
                    self.out.writeline('twineNewline()')
                if child.value == '\\\n' and self.config.get('emit_escaped_newlines', False):
                    self.out.writeline('# ESCAPED NEWLINE')
        else:
            self.out.writeline('### Sugar2Coffee: UNHANDLED TYPE {}'.format(type(child)))
            self.out.writeline(repr(child))
            self.out.writeline('###')
