###############################################################################
#
# Copyright (c) 2011 Ruslan Spivak
# Horribly mutilated by VentureGuy for Twee passages, (c)2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
###############################################################################

__author__ = 'VentureGuy <friedfrogs888@yahoo.com>'

import ply.lex
from slimit4twine.unicode_chars import (COMBINING_MARK, CONNECTOR_PUNCTUATION,
                                        DIGIT, LETTER)

#from buildtools import log
class TweeLexingError(Exception):
    def __init__(self):
        self.line_start = 0
        self.line_parsed = ''
        self.line_raw = ''
        self.char = ''
        self.filename = ''
        self.line_number = 0
        self.lexpos = 0
        self.lineno = 0
        self.prev_token = None
        self.lexstate = ''

    def log(self, log):
        log.critical('%s:%d:%d: Illegal character %r at %d after %s (lexstate=%r)',
                     '<string>' if self.filename is None else self.filename,
                     self.line_number,
                     self.lexpos - self.line_start,
                     self.char,
                     self.lexpos,
                     self.prev_token,
                     self.lexstate)

        log.error(self.line_raw)
        log.error("-" * (self.lexpos - self.line_start + 1) + "^")

    def __str__(self):
        return ('%s:%d:%d: Illegal character %r at %d after %s (lexstate=%r)' %
                ('<string>' if self.filename is None else self.filename,
                self.line_number,
                self.lexpos - self.line_start,
                self.char,
                self.lexpos,
                self.prev_token,
                self.lexstate))


# See "Regular Expression Literals" at
# http://www.mozilla.org/js/language/js20-2002-04/rationale/syntax.html
TOKENS_THAT_IMPLY_DIVISON = frozenset([
    'ID',
    'NUMBER',
    'STRING',
    'REGEX',
    'TRUE',
    'FALSE',
    'NULL',
    'THIS',
    'PLUSPLUS',
    'MINUSMINUS',
    'RPAREN',
    'RBRACE',
    'RBRACKET',
])


class Lexer(object):
    """A JavaScript lexer.

    >>> from slimit4twine.lexer import Lexer
    >>> lexer = Lexer()

    Lexer supports iteration:

    >>> lexer.input('a = 1;')
    >>> for token in lexer:
    ...     print token
    ...
    LexToken(ID,'a',1,0)
    LexToken(EQ,'=',1,2)
    LexToken(NUMBER,'1',1,4)
    LexToken(SEMI,';',1,5)

    Or call one token at a time with 'token' method:

    >>> lexer.input('a = 1;')
    >>> while True:
    ...     token = lexer.token()
    ...     if not token:
    ...         break
    ...     print token
    ...
    LexToken(ID,'a',1,0)
    LexToken(EQ,'=',1,2)
    LexToken(NUMBER,'1',1,4)
    LexToken(SEMI,';',1,5)

    >>> lexer.input('a = 1;')
    >>> token = lexer.token()
    >>> token.type, token.value, token.lineno, token.lexpos
    ('ID', 'a', 1, 0)

    For more information see:
    http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-262.pdf
    """

    def __init__(self, filename='<string>', **kwargs):
        self.prev_token = None
        self.cur_token = None
        self.next_tokens = []
        self.filename = filename
        self.build(**kwargs)

        self.line_start = 0

    def build(self, **kwargs):
        """Build the lexer."""
        self.lexer = ply.lex.lex(object=self, **kwargs)

    def input(self, text):
        self.lexer.input(text)

    def token(self):
        if self.next_tokens:
            return self.next_tokens.pop()

        lexer = self.lexer
        while True:
            pos = lexer.lexpos
            try:
                char = lexer.lexdata[pos]
                while char in ' \t':
                    pos += 1
                    char = lexer.lexdata[pos]
                next_char = lexer.lexdata[pos + 1]
            except IndexError:
                tok = self._get_update_token()
                if tok is not None and tok.type == 'LINE_TERMINATOR':
                    continue
                else:
                    #print(repr(tok))
                    return tok

            if char != '/' or (char == '/' and next_char in ('/', '*')):
                tok = self._get_update_token()
                if tok.type in ('LINE_TERMINATOR',
                                'LINE_COMMENT', 'BLOCK_COMMENT'):
                    continue
                else:
                    #print(repr(tok))
                    return tok

            # current character is '/' which is either division or regex
            cur_token = self.cur_token
            is_division_allowed = (
                cur_token is not None and
                cur_token.type in TOKENS_THAT_IMPLY_DIVISON
            )
            if self.lexer.current_state() != 'js' or is_division_allowed:
                o = self._get_update_token()
                #print(repr(o))
                return o
            else:
                self.prev_token = self.cur_token
                self.cur_token = self._read_regex()
                return self.cur_token

    def auto_semi(self, token):
        if token is None or token.type == 'RBRACE' or self._is_prev_token_lt():
            if token:
                self.next_tokens.append(token)
            return self._create_semi_token(token)
        else:
            return None

    def _is_prev_token_lt(self):
        return self.prev_token and self.prev_token.type == 'LINE_TERMINATOR'

    def _read_regex(self):
        self.lexer.begin('regex')
        token = self.lexer.token()
        self.lexer.begin('js')
        return token

    def _get_update_token(self):
        self.prev_token = self.cur_token
        self.cur_token = self.lexer.token()
        # insert semicolon before restricted tokens
        # See section 7.9.1 ECMA262
        if (self.cur_token is not None
                    and self.cur_token.type == 'LINE_TERMINATOR'
                    and self.prev_token is not None
                    and self.prev_token.type in ['BREAK', 'CONTINUE',
                                                 'RETURN', 'THROW']
                ):
            return self._create_semi_token(self.cur_token)
        return self.cur_token

    def _create_semi_token(self, orig_token):
        token = ply.lex.LexToken()
        token.type = 'SEMI'
        token.value = ';'
        if orig_token is not None:
            token.lineno = orig_token.lineno
            token.lexpos = orig_token.lexpos
        else:
            token.lineno = 0
            token.lexpos = 0
        return token

    # iterator protocol
    def __iter__(self):
        return self

    def __next__(self):
        token = self.token()
        if not token:
            raise StopIteration
        return token

    states = (
        ('regex',    'exclusive'),
        ('supertag', 'exclusive'),
        ('wikilink', 'exclusive'),
        ('js',       'exclusive')
    )

    ################
    # INITIAL
    #
    # Enters:
    #  js::CLOSE_SUPERTAG
    # Exits:
    #  OPEN_SUPERTAG -> supertag
    ################
    def t_INITIAL_TWEE_ENDIF(self, t):
        r'<<endif>>'  # Needed or yacc.py flips out trying to reduce.
        return t
    def t_INITIAL_TWEE_ELSEIF(self, t):
        r'<<elseif'  # Needed or yacc.py flips out trying to reduce.
        t.lexer.push_state('js')
        return t
    def t_INITIAL_TWEE_IF(self, t):
        r'<<if'  # Needed or yacc.py flips out trying to reduce.
        t.lexer.push_state('js')
        return t
    def t_INITIAL_TWEE_ELSE(self, t):
        r'<<else'  # Needed or yacc.py flips out trying to reduce.
        t.lexer.push_state('js')
        return t

    def t_INITIAL_OPEN_SUPERTAG(self, t):
        r'<<(?!if|elseif|endif|else)'
        t.lexer.push_state('supertag')
        return t

    def t_ANY_error(self, t: ply.lex.LexToken):
        ex = TweeLexingError()
        ex.line_start = self.line_start
        ex.line_parsed = self.lexer.lexdata[self.line_start - 1:t.lexpos]
        ex.line_raw = self.lexer.lexdata[self.line_start - 1:self.lexer.lexdata.find("\n", self.line_start - 1)]
        ex.char = self.lexer.lexdata[t.lexpos]
        ex.filename = self.filename or '<string>'
        ex.lexpos = t.lexpos
        ex.lineno = self.lexer.lineno
        ex.prev_token = self.prev_token
        ex.lexstate = self.lexer.lexstate
        #print(self.lexer.current_state())
        raise ex

    def t_INITIAL_INLINE_HTML(self, t):
        r'([^<\n/\[\\]|<(?!<)|/(?!%)|\[(?!\[)|\\(?!\n))+'
        t.lexer.lineno += t.value.count("\n")
        self.line_start = t.lexpos+t.value.rfind('\n') + 1
        return t

    #t_INITIAL_BOLD = r"''"
    #t_INITIAL_ITALIC = r"//"
    #t_INITIAL_UNDERLINE = r"__"
    def t_COMMENT(self, t):
        r'/%([^%]|\n|%(?!/))*%/'
        t.lexer.lineno += t.value.count("\n")
        self.line_start = t.lexpos+t.value.rfind('\n') + 1
        return t

    def t_INITIAL_NEWLINE_ESCAPED(self, t):
        r"\\[\r\n]"
        self.line_start = t.lexpos+1
        t.lexer.lineno += 1
        return t

    def t_INITIAL_NEWLINE(self, t):
        r"(?<!\\)[\r\n]"
        self.line_start = t.lexpos+1
        t.lexer.lineno += 1
        return t

    def t_js_supertag_wikilink_NEWLINE(self, t):
        r"[\r\n]"
        self.line_start = t.lexpos+1
        t.lexer.lineno += 1

    ignored = r"[ \t]"

    #################
    # supertag
    #
    # Enters:
    #   INITIAL::OPEN_SUPERTAG
    # Exits:
    #   supertag::TWEE_BUTTON -> button
    #   supertag::TWEE_SET -> js
    #   supertag::TWEE_PRINT -> js
    #   supertag::TWEE_DISPLAY -> js
    #   supertag::TWEE_NOBR -> js
    #################
    def t_supertag_TWEE_BUTTON(self, t):
        r'button'
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_NOBR(self, t):
        r'nobr'
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_ENDNOBR(self, t):
        r'endnobr'
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_SET(self, t):
        r'set'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_PRINT(self, t):
        r'print'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_DISPLAY(self, t):
        r'display'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_ADDTAG(self, t):
        r'addtag'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_REMOVETAG(self, t):
        r'removetag'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_TOGGLETAG(self, t):
        r'toggletag'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_TEXTINPUT(self, t):
        r'textinput'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t

    def t_supertag_TWEE_RADIO(self, t):
        r'radio'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t

    '''
    def t_supertag_SUPERTAG_IDENTIFIER(self, t):
        r'[a-z]+'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t
    '''

    #################
    # wikilink
    #
    # Enters:
    #  js::WIKILINK_OPEN
    # Exits:
    #  button::BUTTON_PIPE -> js
    #  button::BUTTON_CLOSE -> js
    #################
    t_wikilink_ignore = ' \t'
    # [[label]]>>
    # [[label|passage]]>>

    def t_wikilink_WIKILINK_CLOSE(self, t):
        r'\]\](?!>>)'
        #print('ENTERING js')
        t.lexer.pop_state()
        #assert t.lexer.lexstate == 'INITIAL', t.lexer.lexstate
        #log.info('Now in lexstate: %s',t.lexer.lexstate)
        return t

    def t_wikilink_CLOSE_WIKILINK_AND_SUPERTAG(self, t):
        r'\]\]>>'
        #print('ENTERING INITIAL')
        while t.lexer.lexstate in ('js', 'supertag', 'wikilink'):
            t.lexer.pop_state() # End wikilink
        assert t.lexer.lexstate == 'INITIAL', t.lexer.lexstate
        return t

    def t_wikilink_WIKILINK_PIPE(self, t):
        r'\|'
        #print('ENTERING js')
        t.lexer.push_state('js')
        return t
    t_wikilink_WIKILINK_LABEL = r'(?<=\[\[)([^\|\]])+'

    #################
    # js
    #
    # Enters:
    #   INITIAL::SUPERTAG_IDENTIFIER
    # Exits:
    #   js::CLOSE_SUPERTAG -> INITIAL
    #################

    def t_js_CLOSE_SUPERTAG(self, t):
        r'>>'
        #print('ENTERING INITIAL')
        while t.lexer.lexstate in ('js', 'supertag', 'wikilink'):
            t.lexer.pop_state() # End wikilink
        assert t.lexer.lexstate == 'INITIAL', t.lexer.lexstate
        return t

    def t_js_CLOSE_WIKILINK_AND_SUPERTAG(self, t):
        r'\]\]>>'
        #print('ENTERING INITIAL')
        while t.lexer.lexstate in ('js', 'supertag', 'wikilink'):
            t.lexer.pop_state() # End wikilink
        assert t.lexer.lexstate == 'INITIAL', t.lexer.lexstate
        return t

    def t_INITIAL_js_WIKILINK_OPEN(self, t):
        r'\[\['
        t.lexer.push_state('wikilink')
        return t

    t_js_WIKILINK_START_CALLBACK = r'\]\['

    def t_js_WIKILINK_CLOSE(self, t):
        r'\]\](?!>>)'
        t.lexer.pop_state() # JS
        t.lexer.pop_state() # Wikilink
        return t

    _js_keywords = [
        'BREAK', 'CASE', 'CATCH', 'CONTINUE', 'DEBUGGER', 'DEFAULT', 'DELETE',
        'DO', 'ELSE', 'FINALLY', 'FOR', 'FUNCTION', 'IF', 'IN',
        'INSTANCEOF', 'NEW', 'RETURN', 'SWITCH', 'THIS', 'THROW', 'TRY',
        'TYPEOF', 'VAR', 'VOID', 'WHILE', 'WITH', 'NULL', 'TRUE', 'FALSE',
        # future reserved words - well, it's uncommented now to make
        # IE8 happy because it chokes up on minification:
        # obj["class"] -> obj.class
        # Not needed in Twee, since there's no method of specifying them. - VG
        #'CLASS', 'CONST', 'ENUM', 'EXPORT', 'EXTENDS', 'IMPORT', 'SUPER',
    ]
    _twine_keywords = {
        'and': 'OLD_AND',
        'or': 'OLD_OR',
        'lte': 'OLD_LE',
        'gte': 'OLD_GE',
        'lt': 'OLD_LT',
        'gt': 'OLD_GT',
        'eq': 'OLD_EQEQ',
        'is': 'IS',
        'isnt': 'ISNT',
        'neq': 'OLD_NE',
        # SUPERTAGS
        'print':      'TWEE_PRINT',
        'set':        'TWEE_SET',
        'display':    'TWEE_DISPLAY',
        'button':     'TWEE_BUTTON',
        'addtag':     'TWEE_ADDTAG',
        'removetag':  'TWEE_REMOVETAG',
        'toggletag':  'TWEE_TOGGLETAG',
        'textinput':  'TWEE_TEXTINPUT',
        'radio':      'TWEE_RADIO',
        'nobr':       'TWEE_NOBR',
        'endnobr':    'TWEE_ENDNOBR',
    }
    keywords = [x.upper() for x in _js_keywords + list(_twine_keywords.keys())]
    keywords_dict = dict(dict((key.lower(), key) for key in _js_keywords), **_twine_keywords)

    tokens = [
        ######################
        # TWINE SUPERTAG SHIT
        ######################
        'OPEN_SUPERTAG', 'CLOSE_SUPERTAG', 'NEWLINE', 'NEWLINE_ESCAPED',
        'INLINE_HTML',
        'WIKILINK_OPEN',
        'WIKILINK_PIPE',
        'WIKILINK_LABEL',
        'WIKILINK_START_CALLBACK',
        'WIKILINK_CLOSE',
        'CLOSE_WIKILINK_AND_SUPERTAG',  # HACK around some parsing issues.
        'COMMENT',
        'TWEE_IF',
        'TWEE_ELSEIF',
        'TWEE_ELSE',
        'TWEE_ENDIF',
        # Disabled
        #'ITALIC', 'UNDERLINE', 'BOLD', 'SUPERTAG_IDENTIFIER',

        # Punctuators
        'PERIOD', 'COMMA', 'SEMI', 'COLON',     # . , ; :
        'PLUS', 'MINUS', 'MULT', 'DIV', 'MOD',  # + - * / %
        'BAND', 'BOR', 'BXOR', 'BNOT',          # & | ^ ~
        'CONDOP',                               # conditional operator ?
        'NOT',                                  # !
        'LPAREN', 'RPAREN',                     # ( and )
        'LBRACE', 'RBRACE',                     # { and }
        'LBRACKET', 'RBRACKET',                 # [ and ]
        'EQ', 'EQEQ', 'NE',                     # = == !=
        #'OLD_EQEQ', 'OLD_NE', 'IS', 'ISNT',     # eq neq is isnt
        'STREQ', 'STRNEQ',                      # === and !==
        'LT', 'GT',  # 'OLD_LT', 'OLD_GT',         # < > lt gt
        'LE', 'GE',  # 'OLD_LE', 'OLD_GE',         # <= >= lte gte
        'OR', 'AND',  # 'OLD_AND', 'OLD_OR',       # || && and or
        'PLUSPLUS', 'MINUSMINUS',               # ++ --
        'LSHIFT',                               # <<
        'RSHIFT', 'URSHIFT',                    # >> and >>>
        'PLUSEQUAL', 'MINUSEQUAL',              # += and -=
        'MULTEQUAL', 'DIVEQUAL',                # *= and /=
        'LSHIFTEQUAL',                          # <<=
        'RSHIFTEQUAL', 'URSHIFTEQUAL',          # >>= and >>>=
        'ANDEQUAL', 'MODEQUAL',                 # &= and %=
        'XOREQUAL', 'OREQUAL',                  # ^= and |=

        # Terminal types
        'NUMBER', 'STRING', 'ID', 'REGEX',

        # Properties
        'GETPROP', 'SETPROP',

        # Comments
        #'LINE_COMMENT', 'BLOCK_COMMENT',
        #'LINE_TERMINATOR',
    ] + _js_keywords + list(_twine_keywords.values())

    # adapted from https://bitbucket.org/ned/jslex
    t_regex_REGEX = r"""(?:
        /                       # opening slash
        # First character is..
        (?: [^*\\/[]            # anything but * \ / or [
        |   \\.                 # or an escape sequence
        |   \[                  # or a class, which has
                (?: [^\]\\]     # anything but \ or ]
                |   \\.         # or an escape sequence
                )*              # many times
            \]
        )
        # Following characters are same, except for excluding a star
        (?: [^\\/[]             # anything but \ / or [
        |   \\.                 # or an escape sequence
        |   \[                  # or a class, which has
                (?: [^\]\\]     # anything but \ or ]
                |   \\.         # or an escape sequence
                )*              # many times
            \]
        )*                      # many times
        /                       # closing slash
        [a-zA-Z0-9]*            # trailing flags
        )
        """

    t_regex_ignore = ' \t'

    def t_regex_error(self, token):
        raise TypeError(
            "Error parsing regular expression '%s' at %s" % (
                token.value, token.lineno)
        )

    # Punctuators
    t_js_PERIOD = r'\.'
    t_js_COMMA = r','
    t_js_SEMI = r';'
    t_js_COLON = r':'
    t_js_PLUS = r'\+'
    t_js_MINUS = r'-'
    t_js_MULT = r'\*'
    t_js_DIV = r'/'
    t_js_MOD = r'%'
    t_js_BAND = r'&'
    t_js_BOR = r'\|'
    t_js_BXOR = r'\^'
    t_js_BNOT = r'~'
    t_js_CONDOP = r'\?'
    t_js_NOT = r'!'
    t_js_LPAREN = r'\('
    t_js_RPAREN = r'\)'
    t_js_LBRACE = r'{'
    t_js_RBRACE = r'}'
    t_js_LBRACKET = r'\['
    t_js_RBRACKET = r'\]'
    t_js_EQ = r'='
    t_js_EQEQ = r'=='
    # t_js_OLD_EQEQ      = r'eq' # Twine: Old eq syntax
    # t_js_IS            = r'is' # Twine: Newer is syntax
    t_js_NE = r'!='
    # t_js_OLD_NE        = r'neq' # Twine: Old neq syntax
    # t_js_ISNT          = r'isnt' # Twine: Newer isnt syntax
    t_js_STREQ = r'==='
    t_js_STRNEQ = r'!=='
    t_js_LT = r'<'
    # t_js_OLD_LT        = r'lt' # Twine
    t_js_GT = r'>'
    # t_js_OLD_GT        = r'gt' # Twine
    t_js_LE = r'<='
    # t_js_OLD_LE        = r'lte' # Twine
    t_js_GE = r'>='
    # t_js_OLD_GE        = r'gte' # Twine
    t_js_OR = r'\|\|'
    # t_js_OLD_OR        = r'or' # Twine
    t_js_AND = r'&&'
    # t_js_OLD_AND        = r'and' # Twine
    t_js_PLUSPLUS = r'\+\+'
    t_js_MINUSMINUS = r'--'
    t_js_LSHIFT = r'<<'
    t_js_RSHIFT = r'>>'
    t_js_URSHIFT = r'>>>'
    t_js_PLUSEQUAL = r'\+='
    t_js_MINUSEQUAL = r'-='
    t_js_MULTEQUAL = r'\*='
    t_js_DIVEQUAL = r'/='
    t_js_LSHIFTEQUAL = r'<<='
    t_js_RSHIFTEQUAL = r'>>='
    t_js_URSHIFTEQUAL = r'>>>='
    t_js_ANDEQUAL = r'&='
    t_js_MODEQUAL = r'%='
    t_js_XOREQUAL = r'\^='
    t_js_OREQUAL = r'\|='

    #t_js_LINE_COMMENT = r'//[^\r\n]*'
    #t_js_BLOCK_COMMENT = r'/\*[^*]*\*+([^/*][^*]*\*+)*/'

    #t_js_LINE_TERMINATOR = r'[\n\r]+'

    t_js_ignore = ' \t'

    t_js_NUMBER = r"""
    (?:
        0[xX][0-9a-fA-F]+              # hex_integer_literal
     |  0[0-7]+                        # or octal_integer_literal (spec B.1.1)
     |  (?:                            # or decimal_literal
            (?:0|[1-9][0-9]*)          # decimal_integer_literal
            \.                         # dot
            [0-9]*                     # decimal_digits_opt
            (?:[eE][+-]?[0-9]+)?       # exponent_part_opt
         |
            \.                         # dot
            [0-9]+                     # decimal_digits
            (?:[eE][+-]?[0-9]+)?       # exponent_part_opt
         |
            (?:0|[1-9][0-9]*)          # decimal_integer_literal
            (?:[eE][+-]?[0-9]+)?       # exponent_part_opt
         )
    )
    """

    string = r"""
    (?:
        # double quoted string
        (?:"                               # opening double quote
            (?: [^"\\\n\r]                 # no \, line terminators or "
                | \\[a-zA-Z!-\/:-@\[-`{-~] # or escaped characters
                | \\x[0-9a-fA-F]{2}        # or hex_escape_sequence
                | \\u[0-9a-fA-F]{4}        # or unicode_escape_sequence
            )*?                            # zero or many times
            (?: \\\n                       # multiline ?
              (?:
                [^"\\\n\r]                 # no \, line terminators or "
                | \\[a-zA-Z!-\/:-@\[-`{-~] # or escaped characters
                | \\x[0-9a-fA-F]{2}        # or hex_escape_sequence
                | \\u[0-9a-fA-F]{4}        # or unicode_escape_sequence
              )*?                          # zero or many times
            )*
        ")                                 # closing double quote
        |
        # single quoted string
        (?:'                               # opening single quote
            (?: [^'\\\n\r]                 # no \, line terminators or '
                | \\[a-zA-Z!-\/:-@\[-`{-~] # or escaped characters
                | \\x[0-9a-fA-F]{2}        # or hex_escape_sequence
                | \\u[0-9a-fA-F]{4}        # or unicode_escape_sequence
            )*?                            # zero or many times
            (?: \\\n                       # multiline ?
              (?:
                [^'\\\n\r]                 # no \, line terminators or '
                | \\[a-zA-Z!-\/:-@\[-`{-~] # or escaped characters
                | \\x[0-9a-fA-F]{2}        # or hex_escape_sequence
                | \\u[0-9a-fA-F]{4}        # or unicode_escape_sequence
              )*?                          # zero or many times
            )*
        ')                                 # closing single quote
    )
    """  # "

    @ply.lex.TOKEN(string)
    def t_js_STRING(self, token):
        # remove escape + new line sequence used for strings
        # written across multiple lines of code
        token.value = token.value.replace('\\\n', '')
        return token

    # XXX: <ZWNJ> <ZWJ> ?
    identifier_start = r'(?:' + r'[a-zA-Z_$]' + r'|' + LETTER + r')+'
    identifier_part = (
        r'(?:' + COMBINING_MARK + r'|' + r'[0-9a-zA-Z_$]' + r'|' + DIGIT +
        r'|' + CONNECTOR_PUNCTUATION + r')*'
    )
    identifier = (identifier_start + identifier_part).replace(']|[', '')

    getprop = r'get' + r'(?=\s' + identifier + r')'

    @ply.lex.TOKEN(getprop)
    def t_js_GETPROP(self, token):
        return token

    setprop = r'set' + r'(?=\s' + identifier + r')'

    @ply.lex.TOKEN(setprop)
    def t_js_SETPROP(self, token):
        return token

    @ply.lex.TOKEN(identifier)
    def t_js_ID(self, token):
        token.type = self.keywords_dict.get(token.value, 'ID')
        return token

    def t_js_error(self, token):
        print('Illegal character %r at %s:%s after %s' % (
            token.value[0], token.lineno, token.lexpos, self.prev_token))
        token.lexer.skip(1)
