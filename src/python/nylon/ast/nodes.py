import re
import typing
from buildtools import log

import yaml
from slimit4twine import ast
from slimit4twine.parser import Parser
from slimit4twine.visitors import ecmavisitor, nodevisitor

BINOP_REPLACEMENTS = {
    'gt': '>',
    'gte': '>=',
    'lt': '<=',
    'lte': '<=',
    'eq': '==',
    'neq': '!=',
    'and': '&&',
    'or': '||',
    'is': '==',
    'isnt': '!=',
    'to': '='
}
REG_BUTTON_FORMAT = re.compile(r'\[\[(?P<text>[^\|]+)\|(?P<passage>[^\]]+)(\]\[(?P<action>[^\]]+))\]\]')


class NodeVisitor(object):
    def visit(self, node, first=True):
        if first:
            yield node
        for child in node:
            if not isinstance(child, (ast.Node, TweeNode)) or child is None:
                log.warn('Non-node type %s (%s) in %s', type(child), child, type(node))
                continue
            if node == child:
                raise Exception('{}.{}: INFINITE RECURSION', type(node), find_attr_of_child_in(node, child))
            yield child
            for grandchild in self.visit(child, False):
                yield grandchild


class NodeReplacementVisitor(object):
    def visit(self, node, callback):
        for key, child in node.dict().items():
            #if not isinstance(child, (ast.Node, TweeNode)) or child is None:
            #    log.warn('Non-node type %s (%s) in %s',type(child), child, type(node))
            #    continue
            if node == child:
                continue
            #key = find_attr_of_child_in(node, child)
            #if key is None:
            #    continue
            #log.info('Visiting %s, type %s (%s) in %s',key, type(child), child, type(node))
            if isinstance(child, list):
                for i in range(len(child)):
                    newlist = []
                    if isinstance(child[i], (ast.Node, TweeNode)):
                        entry = callback(child, i, child[i])
                        if child is not None:
                            newlist.append(entry)
                            self.visit(child[i], callback)
            elif isinstance(child, (ast.Node, TweeNode)):
                setattr(node, key, callback(node, key, child))
                self.visit(child, callback)


class RawTJSWrapper(yaml.YAMLObject):
    yaml_tag = '!raw'

    def __init__(self, val):
        self.val = val

    @classmethod
    def to_yaml(cls, dumper, data):
        node = dumper.represent_scalar('!raw', treeToString(data.val))
        return node

    @classmethod
    def from_yaml(cls, loader, node):
        rawjs = loader.construct_scalar(node)
        val = parse_twinescript(rawjs)
        return RawTJSWrapper(val)


def find_attr_of_child_in(parent, child):
    for k in dir(parent):
        v = getattr(parent, k, None)
        if v == child:
            return k
        if isinstance(v, list) and child in v:
            return k
    return None


def build_parents(node):
    for child in node:
        attr = find_attr_of_child_in(node, child)
        if not isinstance(child, (ast.Node, TweeNode)):
            log.error('Detected child of type %s (%r) in %s.%s!', type(child), child, type(node), attr)
        child.parent = node
        child.parent_attr = attr
        build_parents(child)


def fixCompsInTree(tree):
    for node in NodeVisitor().visit(tree):
        if isinstance(node, ast.BinOp):
            node.op = BINOP_REPLACEMENTS.get(node.op, node.op)
    return tree


def replaceVariablesInTree(tree, replacements):
    for node in NodeVisitor().visit(tree):
        if isinstance(node, ast.Identifier):
            newValue = replacements.get(node.value, None)
            if newValue is not None:
                if callable(newValue):
                    newValue(node)
                else:
                    node.value = newValue
    return tree


def twinifyVariables(tree):
    for node in NodeVisitor().visit(tree):
        if isinstance(node, ast.Identifier):
            if node.value.startswith('$'):
                node.value = 'getVariables().' + node.value[1:]
    return tree


def detwinifyVariables(tree):
    for node in NodeVisitor().visit(tree):
        if isinstance(node, ast.Identifier):
            if node.value.startswith('getVariables().'):
                node.value = '$' + node.value[15:]
        for attr, child in node.dict().items():
            if isinstance(child, ast.DotAccessor) and isinstance(child.node, ast.FunctionCall):
                if child.node.identifer.value == 'getVariables':
                    child.identifer.value = '$' + child.identifer.value
                    setattr(node, attr, child.identifer)

    return tree


def replace_old_comparisons(oldstr: str) -> str:
    #oldstr = re.sub(r'\bgt\b', '>', oldstr)
    #oldstr = re.sub(r'\bgte\b', '>=', oldstr)
    #oldstr = re.sub(r'\blt\b', '<=', oldstr)
    #oldstr = re.sub(r'\blte\b', '<=', oldstr)
    #oldstr = re.sub(r'\beq\b', '==', oldstr)
    #oldstr = re.sub(r'\bneq\b', '!=', oldstr)
    return oldstr


def treeToString(tree, skip_final_semicolon=True):
    out = ecmavisitor.ECMAVisitor().visit(tree)
    if skip_final_semicolon and out.endswith(';'):
        out = out[:-1]
    return out


def dump_slimit_struct(node):
    with log.info(str(node)):
        for child in node.children():
            dump_slimit_struct(child)


def dump_ast_to_list(node, indent='\t', level=0):
    txt = (indent * level)
    txt += type(node).__name__
    txt += '\t'
    ctxt = []
    if hasattr(node, 'dict'):
        for k, v in node.dict().items():
            if isinstance(v, list):
                v = str(len(v))
            elif isinstance(v, dict):
                v = str(len(v))
            elif isinstance(v, str):
                v = repr(v)
            else:
                v = type(v).__name__
            ctxt.append('{}={}'.format(k, v))
    else:
        for k in dir(node):
            v = getattr(node, k)
            if isinstance(v, str):
                ctxt.append('{}={!r}'.format(k, v))

    txt += ' '.join(ctxt)
    o = [txt]
    for child in node:
        o += dump_ast_to_list(child, indent, level + 1)
    return o


def get_first_of(node, nodetype, skip_root=False):
    if not skip_root and isinstance(node, nodetype):
        return node
    for child in NodeVisitor().visit(node):
        if isinstance(child, nodetype):
            return child
    return None


def prepTreeForCoffee(tree, replaceVars={}):
    tree = fixCompsInTree(tree)
    tree = replaceVariablesInTree(tree, replaceVars)
    return twinifyVariables(tree)


def simplify(x: ast.Node, fail_to_raw=False, strip_strings=True) -> str:
    if isinstance(x, ast.String):
        if strip_strings:
            return x.value.strip('"\'')
        else:
            return x.value
    elif isinstance(x, ast.Number):
        try:
            return int(x.value)
        except:
            return float(x.value)
    elif isinstance(x, ast.Null):
        return None
    elif isinstance(x, ast.Identifier):
        return x.value
    else:
        return treeToString(x) if not fail_to_raw else RawTJSWrapper(x)


def args2list(tree, strip_strings=False):
    o = []
    # dump_slimit_struct(tree)
    for node in [tree] + list(nodevisitor.visit(tree)):
        if isinstance(node, ast.FunctionCall):
            for x in node.args:
                if isinstance(x, ast.String):
                    if strip_strings:
                        o += [x.value.strip('"\'')]
                    else:
                        o += [x.value]
                elif isinstance(x, ast.Number):
                    o += [int(x.value)]
                elif isinstance(x, ast.Null):
                    o += [None]
                elif isinstance(x, ast.Identifier):
                    o += [x.value]
                else:
                    o += ['{!r}'.format(x)]
    return o


either2list = args2list

slimit_parser = None


def parse_twinescript(tws: str) -> ast.Program:
    '''
    This is for shit like '$wow gte 0'
    '''
    global slimit_parser
    if slimit_parser is None:
        log.warn('Starting slimit4twine parser.  Ignore any warnings below, it\'s old and in need of TLC.')
        slimit_parser = Parser()
    return slimit_parser.parse(replace_old_comparisons(tws))


def getAllAssignmentsAndReferencesIn(tree):
    assignments = set()
    references = set()
    for node in nodevisitor.visit(tree):
        if isinstance(node, ast.Assign):
            if isinstance(node.left, ast.Identifier):
                assignments.add(node.left.value)
        if isinstance(node, ast.Identifier):
            references.add(node.value)
    return assignments, references


class TweeNode(object):

    def __init__(self):
        self.Name = type(self).__name__
        self.parser = None
        self.parent = None
        self.line = 0
        self.column = 0
        self._children_list = []

    def __iter__(self):
        for child in self._children_list:
            if child is not None:
                yield child

    def dict(self):
        return {}

    def children(self):
        return self._children_list

    def ReceiveParserEvent(self, event):
        pass

    def FromParser(self, parser):
        self.parser = parser
        self.line = parser._line
        self.column = parser._column
        self.parent = parser._currentNode

    def GetAllChildren(self):
        return self._children_list

    def hasTextNode(self):
        return False

    def __str__(self):
        return self.Name

    def __repr__(self):
        return self.Name


class TweeNewlineNode(TweeNode):
    def __init__(self, value=''):
        super(TweeNewlineNode, self).__init__()
        self.value = value

    def dict(self):
        return {
            'value': self.value
        }

    def __str__(self):
        return 'TweeNewlineNode({!r})'.format(self.value)

    def __repr__(self):
        return 'TweeNewlineNode({!r})'.format(self.value)


class TweeTextNode(TweeNode):
    def __init__(self, value=''):
        super(TweeTextNode, self).__init__()
        self.value = value

    def dict(self):
        return {'value': self.value}

    def __str__(self):
        return 'TweeTextNode({!r})'.format(self.value)

    def __repr__(self):
        return 'TweeTextNode({!r})'.format(self.value)


class TweeCommentNode(TweeNode):
    def __init__(self, value=''):
        super(TweeCommentNode, self).__init__()
        self.value = value

    def dict(self):
        return {'value': self.value}

    def __str__(self):
        return 'TweeCommentNode({!r})'.format(self.value)

    def __repr__(self):
        return 'TweeCommentNode({!r})'.format(self.value)


class TweeLineNode(TweeNode):

    def __init__(self):
        super(TweeLineNode, self).__init__()

    def GetAllChildren(self):
        o = []
        for c in self._children_list:
            o += [c] + c.GetAllChildren()
        return o

    def __iter__(self):
        for child in self._children_list:
            yield child

    def dict(self):
        return {'children': self._children_list}

    def ReceiveParserEvent(self, event):
        return EParserEventResponse.CONTINUE

    def __str__(self):
        return 'TweeLineNode(children={!r})'.format(len(self._children_list))

    def __repr__(self):
        return 'TweeLineNode(children={!r})'.format(len(self._children_list))


class TweeSupertagNode(TweeNode):

    def __init__(self):
        super(TweeSupertagNode, self).__init__()
        self.tagName = None
        self.arguments = None
        self._children_list = []
        self._js_tree = None
        self._has_text_node = False
        self.self_closing = True

    def GetAllChildren(self):
        o = []
        for n in self._children_list:
            o += [n] + n.GetAllChildren()
        return o

    def __iter__(self):
        for child in self.children():
            yield child

    def GetArgsAsStr(self, start=0, end=0):
        return ' '.join(self.arguments[start:len(self.arguments) - end])

    def GetArgsAsTree(self, start=0, end=0):
        if self._js_tree is None:
            jst = self.GetArgsAsStr(start, end)
            self._js_tree = parse_twinescript(jst)
            if self._js_tree is None:
                raise Exception("Failed to parse {} as TJS!".format(jst))
        return self._js_tree

    def hasTextNode(self):
        return self._has_text_node

    def __str__(self):
        return '<<{}>>'.format(' '.join([self.tagName] + ['{}={}'.format(k, v) for k, v in self.dict().items()]))

    def __repr__(self):
        return '<<{}>>'.format(' '.join([self.tagName] + ['{}={}'.format(k, v) for k, v in self.dict().items()]))


def extract_button_contents(buttonnode: TweeSupertagNode) -> typing.Tuple[str, str, str]:
    m = REG_BUTTON_FORMAT.match(' '.join(buttonnode.arguments))
    return m.group('text'), m.group('passage'), m.group('action')


class TweeSelfClosingNode(TweeSupertagNode):
    def __init__(self, tag):
        super(TweeSelfClosingNode, self).__init__()
        self.tagName = tag
        self.self_closing = True


class TweeSetNode(TweeSelfClosingNode):

    def __init__(self, source_elements=None):
        super(TweeSetNode, self).__init__('set')
        self.source_elements = source_elements if isinstance(source_elements, list) else [source_elements]

    def dict(self):
        return {'source_elements': self.source_elements}

    def __iter__(self):
        for el in self.source_elements:
            yield el


class TweeAddTagNode(TweeSelfClosingNode):

    def __init__(self, expr=None):
        super(TweeAddTagNode, self).__init__('addtag')
        self.expr = expr

    def dict(self):
        return {'expr': self.expr}

    def __iter__(self):
        yield self.expr


class TweeToggleTagNode(TweeSelfClosingNode):

    def __init__(self, expr=None):
        super(TweeToggleTagNode, self).__init__('toggletag')
        self.expr = expr

    def dict(self):
        return {'expr': self.expr}

    def __iter__(self):
        yield self.expr


class TweeRemoveTagNode(TweeSelfClosingNode):

    def __init__(self, expr=None):
        super(TweeRemoveTagNode, self).__init__('removetag')
        self.expr = expr

    def dict(self):
        return {'expr': self.expr}

    def __iter__(self):
        yield self.expr


class TweeDisplayNode(TweeSelfClosingNode):

    def __init__(self, args=None):
        super(TweeDisplayNode, self).__init__('display')
        self.args = args

    def dict(self):
        return {'args': self.args}


class TweePrintNode(TweeSelfClosingNode):

    def __init__(self, expression=None):
        super(TweePrintNode, self).__init__('print')
        self.expression = expression

    def dict(self):
        return {'expression': self.expression}

    def __iter__(self):
        yield self.expression


class TweeTextInputNode(TweeSelfClosingNode):

    def __init__(self, identifier=None):
        super(TweeTextInputNode, self).__init__('textinput')
        self.identifier = identifier

    def dict(self):
        return {'identifier': self.identifier}

    def __iter__(self):
        yield self.identifier


class TweeRadioNode(TweeSelfClosingNode):

    def __init__(self, identifier=None, expr=None):
        super(TweeRadioNode, self).__init__('radio')
        self.identifier = identifier
        self.expr = expr

    def dict(self):
        return {
            'identifier': self.identifier,
            'expr': self.expr
        }

    def __iter__(self):
        yield self.identifier
        yield self.expr


class TweeButtonNode(TweeSelfClosingNode):

    def __init__(self, link=None):
        super(TweeButtonNode, self).__init__('button')
        self.link = link

    def dict(self):
        return {'link': self.link}

    def __iter__(self):
        if self.link is not None:
            yield self.link


class TweeWikiLinkNode(TweeNode):

    def __init__(self, label=None, passage=None, callback=None):
        super(TweeWikiLinkNode, self).__init__()
        self.label = label
        self.passage = passage
        self.callback = callback

    def __iter__(self):
        if self.passage is not None:
            yield self.passage
        if self.callback is not None:
            for statement in self.callback:
                yield statement

    def dict(self):
        return {
            'label': self.label,
            'passage': self.passage,
            'callback': self.callback,
        }

    def __str__(self):
        o = '[['
        o += '{}'.format(self.label)
        if self.passage is not None:
            o += '|{}'.format(self.passage.to_ecma())
        if self.callback is not None:
            o += '][{}'.format('; '.join([x.to_ecma().strip(';') for x in self.callback]))
        o += ']]'
        return o


class TweeBlockNode(TweeSupertagNode):
    def __init__(self, tag):
        super(TweeBlockNode, self).__init__()
        self.tagName = tag
        self.self_closing = False

    def __iter__(self):
        for child in self._children_list:
            yield child

    def GetAllChildren(self):
        o = []
        for n in self._children_list:
            o += [n] + n.GetAllChildren()
        return o

    def dict(self):
        return {'children': self._children_list}


class TweeNoBRNode(TweeBlockNode):

    def __init__(self, children=[]):
        super(TweeNoBRNode, self).__init__('nobr')
        self._children_list = children


class TweeIfBlockNode(TweeNode):

    def __init__(self, tagName='', condition=None, children=None):
        super(TweeIfBlockNode, self).__init__()
        self.tagName = tagName
        self.condition = condition
        self._children_list = children

    def dict(self):
        o = {
            '_children_list': self._children_list
        }
        if self.condition is not None:
            o['condition'] = self.condition
        return o

    def __iter__(self):
        if self.condition is not None:
            yield self.condition
        for child in self._children_list:
            if child is not None:
                yield child

    def GetConditionAsStr(self):
        return 'None' if self.condition is None else self.condition.to_ecma()

    def GetConditionAsTree(self, start=0, end=0):
        return self.condition

    def __str__(self):
        return '<<{} {!r}>>'.format(self.tagName, self.GetConditionAsStr())

    def __repr__(self):
        return 'TweeIfBlockNode({!r}, {!r})'.format(self.tagName, self.GetConditionAsStr())


class TweeIfNode(TweeSupertagNode):

    def __init__(self):
        super(TweeIfNode, self).__init__()
        self.tagName = 'if (root)'

    def GetAllChildren(self):
        o = []
        for n in self._children_list:
            if n is not None:
                o += [n] + n.GetAllChildren()
        return o

    def dict(self):
        return {'_children_list': self._children_list}

    def __iter__(self):
        for n in self._children_list:
            if n is not None:
                yield n

    def GetFirstConditionAsStr(self):
        return self._children_list[0].GetConditionAsStr()

    def GetFirstConditionAsTree(self):
        return self._children_list[0].GetConditionAsTree()

    def __str__(self):
        return '/% TweeIfNode(conditions={}) %/'.format(len(self._children_list))

    def __repr__(self):
        return 'TweeIfNode(conditions={})'.format(len(self._children_list))


class TweePassageNode(TweeBlockNode):
    def __init__(self, children=[]):
        super(TweePassageNode, self).__init__('__PASSAGE__')
        self._children_list = children or []

    def GetAllChildren(self):
        o = []
        for n in self._children_list:
            o += [n] + n.GetAllChildren()
        return o

    def dict(self):
        return {'_children_list': self._children_list}

    def __iter__(self):
        for n in self._children_list:
            if n is not None:
                yield n

    def children(self):
        for n in self._children_list:
            # if n is not None:
            yield n

    def __str__(self):
        return '/% TweePassageNode(children={}) %/'.format(len(self._children_list))

    def __repr__(self):
        return 'TweePassageNode(children={})'.format(len(self._children_list))


class TweeSyntaxError(Exception):
    def __init__(self):
        self.filename = None
        self.token = None
        self.token_before = None
        self.token_after = None
        self.line_raw = None
        self.line_parsed = None
        self.line_start = 0
        self.lexstate = '???'

    def log(self, log):
        log.error('%s:%d:%s: Unexpected token (%s, %r) at between %s and %s (lexstate=%s)',
                  '<string>' if self.filename is None else self.filename,
                  self.token.lineno,
                  self.token.lexpos - self.line_start,
                  self.token.type,
                  self.token.value,
                  self.token_before,
                  self.token_after,
                  self.lexstate)
        log.error(self.line_raw)
        log.error("-" * (self.token.lexpos - self.line_start + 1) + "^")

    def __str__(self):
        return '{}:{}:{}: Unexpected token ({}, {!r}) at between {} and {} (lexstate={!r})'.format(
            '<string>' if self.filename is None else self.filename,
            self.token.lineno,
            self.token.lexpos - self.line_start,
            self.token.type,
            self.token.value,
            self.token_before,
            self.token_after,
            self.lexstate)
