import mimetypes
import re
import codecs
import os
import sys
import ftfy
import chardet
import shutil
import binascii
import collections

import yaml
import html as libhtml
from lxml import html
from buildtools import log, os_utils
from buildtools.os_utils import get_file_list
from buildtools.os_utils import del_empty_dirs

from nylon.verifylexer import HeadlessVerifyLexer

NYLON_DIR = os.path.dirname(os.path.abspath(__file__))

ALL_WRITTEN = []

def ordered_dump(data, stream=None, Dumper=yaml.Dumper, **kwds):
    class OrderedDumper(Dumper):
        pass

    def _dict_representer(dumper, data):
        return dumper.represent_mapping(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            data.items())
    OrderedDumper.add_representer(collections.OrderedDict, _dict_representer)
    return yaml.dump(data, stream, OrderedDumper, **kwds)

class NylonExporter(object):
    EXT_TYPES = {
        'image/png':  'png',
        'image/jpeg': 'jpg',
        'image/gif':  'gif',
        'image/svg':  'svg',
        'image/webp': 'webp',
    }
    def __init__(self):
        self.files_written=[]

    def _export_header(self, basedir: str, _id: str, tags: list, element: html.HtmlElement) -> None:
        os_utils.ensureDirExists(basedir, noisy=True)
        outfile = os.path.join(basedir, _id + '.header.yml')
        with open(outfile, 'w') as f:
            data = {
                'id': _id,
                'tags': tags,
            }
            if 'created' in element.attrib:
                data['created'] = element.attrib.get('created','201707040000')
            data['modifier'] = element.attrib.get('modifier','nylon')
            if 'twine-position' in element.attrib:
                if 'twine' not in data:
                    data['twine']={}
                data['twine']['position'] = element.attrib['twine-position']
            yaml.dump(data, f, default_flow_style=False)
        self.files_written.append(outfile)

    def _export_file(self, basedir: str, _id: str, tags: list, element: html.HtmlElement, ext: str) -> None:
        os_utils.ensureDirExists(basedir, noisy=True)
        outfile = os.path.join(basedir, _id + '.' + ext)
        with codecs.open(outfile, 'w', encoding='utf-8-sig') as f:
            text = ''
            if element.text is not None:
                text = element.text.strip()
                text = text.replace('\\n', '\n')
                text = text.replace('\\r', '')
                text = text.replace('\\s', '\\')
                text = text.replace('\\t', '\t')
                text = re.sub(r'[ \t]*\n', '\n', text)
            f.write(libhtml.unescape(text))
        self.files_written.append(outfile)


    def export_tiddler(self, outfile_base: str, element: html.HtmlElement, skip_yml: bool = False, subdir: str = '') -> None:
        _id = element.attrib['tiddler']
        tags = element.attrib['tags'].split(' ')
        outfile = os.path.join(outfile_base, 'tiddler')
        with log.info('Exporting %s...', _id):
            if 'Twine.image' in tags:
                if not skip_yml:
                    self._export_header(os.path.join(outfile, 'images', subdir), _id, tags, element)
                data = element.text.strip()
                #log.info('find(":") = %r',data.find(':'))
                #log.info('find(";") = %r',data.find(';'))
                #log.info('find(",") = %r',data.find(','))
                mimetype = data[data.find(':') + 1:data.find(';')]
                outfile = os.path.join(outfile, 'images', subdir, _id + '.' + self.EXT_TYPES[mimetype])
                with open(outfile, 'wb') as f:
                    data = data[data.find(',') + 1:]
                    data = data.replace('\\n','\n').strip()
                    try:
                        f.write(binascii.a2b_base64(data))
                    except binascii.Error as e:
                        log.error('%s: %s (data=%r)', outfile, str(e), data)
                self.files_written.append(outfile)
            elif 'script' in tags:
                if not skip_yml:
                    self._export_header(os.path.join(outfile, 'scripts', subdir), _id, tags, element)
                self._export_file(os.path.join(outfile, 'scripts', subdir), _id, tags, element, 'js')
            elif 'stylesheet' in tags:
                if not skip_yml:
                    self._export_header(os.path.join(outfile, 'css', subdir), _id, tags, element)
                self._export_file(os.path.join(outfile, 'css', subdir), _id, tags, element, 'css')
            else:
                if not skip_yml:
                    self._export_header(os.path.join(outfile, 'passages', subdir), _id, tags, element)
                self._export_file(os.path.join(outfile, 'passages', subdir), _id, tags, element, 'tw')


    def export_tale(self, compiled_file: str, out_file: str, pathmapfile: str=None, skip_unfuck: bool=False, skip_yml:bool=False):
        from nylon.encoding import unfuckFile
        self.files_written = []
        if not skip_unfuck:
            unfuckFile(compiled_file)
        pathmap = {}
        if pathmapfile:
            with open(pathmapfile, 'r') as f:
                for subdir, passages in yaml.safe_load(f).items():
                    for passage in passages:
                        pathmap[passage]=subdir
        parser = html.HTMLParser(remove_blank_text=True)
        xml = html.parse(compiled_file, parser)
        outfile_base = os.path.dirname(out_file)
        projectfile = os.path.join(outfile_base, 'story.yml')
        project = collections.OrderedDict([
            ('title', 'UNTITLED'),
            ('story-settings', {}),
        ])
        for element in xml.iter():
            element.tail = None
            if element.tag == 'title':
                project['title'] = element.text.strip()
            if element.tag == "div" and 'tags' in element.attrib:
                if element.attrib.get('tiddler', '') == 'StorySettings':
                    for line in element.text.split('\\n'):
                        if line.strip() == '':
                            continue
                        log.info(line)
                        k, v = line.strip().split(':')
                        project['story-settings'][k] = v == 'on'
                self.export_tiddler(os.path.join(outfile_base, 'src'), element, skip_yml, subdir=pathmap.get(element.attrib.get('tiddler',''), ''))
        with open(out_file, 'wb') as output:
            output.write(html.tostring(xml, pretty_print=True))
        self.files_written.append(out_file)
        with open(projectfile, 'w') as f:
            ordered_dump(project, f, default_flow_style=False)
        del_empty_dirs(outfile_base)
        self.files_written.append(projectfile)
        return self.files_written
        
