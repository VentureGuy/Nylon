import semantic_version, os

NAME = 'Nylon'
VERSION = semantic_version.Version('0.2.0')

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
ASSETS_DIR = os.path.join(ROOT_DIR, 'assets')
TEMPLATE_DIR = os.path.join(ASSETS_DIR, 'templates')
