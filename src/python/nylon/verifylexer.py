from nylon._twineimports import TweeLexer
class HeadlessVerifyLexer(TweeLexer):
    '''
    Hacked out of the poor, molested corpse of Twine.  This edition of VerifyLexer
    does not need a widget.
    '''

    # Takes a PassageWidget instead of a PassageFrame
    def __init__(self, passage, header, tiddlywiki):
        self.passage = passage
        self.header = header
        self.tiddlywiki = tiddlywiki
        self.twineChecks, self.stylesheetChecks, self.scriptChecks = self.getHeader().passageChecks()

    def getText(self):
        return self.passage.text

    def getHeader(self):
        return self.header

    def passageExists(self, title):
        return title in self.tiddlywiki.tiddlers.keys()

    def includedPassageExists(self, title):
        return self.passageExists(title) # I have no fucking clue.

    def check(self):
        """Collect error messages for this passage, using the overridden applyStyles() method."""
        self.errorList = []
        if self.passage.isScript():
            for i in self.scriptChecks:
                self.errorList += [e for e in i(passage=self.passage)]

        elif self.passage.isStylesheet():
            for i in self.stylesheetChecks:
                self.errorList += [e for e in i(passage=self.passage)]

        else:
            self.lex()
        return sorted(self.errorList, key = lambda a: (a[1][0] if a[1] else float('inf')))

    def applyStyle(self, a, b, c):
        return []
