

class NylonException(Exception):
    pass

class WarningAsError(NylonException):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return 'WarningAsError: '+self.msg
