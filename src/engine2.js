﻿  /**
   * Twine for Nylon v8.4.2019
   */
  /*
  **
  ** Basic utility functions
  **
   */
  /*
   * Changes gibberish into usable code.
   */
  /*
   * Loads passages from JSON/YAML files in paths relative to the HTML file.
   *
   * Game data directory would be structured like:
   *
   * passages/
   *   __INDEX__
   *   PassageA.json
   *   PassageB.json
   *   ...
   * MODS.txt
   * mods/
   *   __INDEX__
   *   ...
   *
   * Supports loading mod indices via entries in MODS.txt. Mod passages
   * overwrite base files.
   *
   * CONFIG:
   *   lang: yml/(json)
   *   paths: ['passages']
   *   allow-mods: yes/no
   *   index-files: '__INDEX__'
   *   obfuscation:
   *     id: (null|vigenere|...)
   *     key: ...
   */
  /*
   * Does absolutely nothing.
   */
  /*
   * Basic Vigenere obfuscation
   */
  /*
  Returns a deep copy of the given object.

  NOTE: 1. `clone()` does not clone functions, however, since function definitions are
           immutable, the only issues are with expando properties and scope.  The former
           really should not be done.  The latter is problematic either way (damned if
           you do, damned if you don't).
        2. `clone()` does not maintain referential relationships (e.g. multiple references
           to the same object will, post-cloning, refer to different equivalent objects;
           i.e. each reference will get its own clone of the original object).
  */
  /* Polyfill for fade transition */
var DEBUG_CODEGEN, ENodeTypes, FORBIDDEN_INSERT_NODETYPES, NylonDeobfuscator, NylonEmbeddedLoader, NylonFileLoader, NylonFileParsingError, NylonHistory, NylonLazyLoader, NylonNullDeobfuscator, NylonPassage, NylonPassageLoader, NylonStory, NylonVigenereDeobfuscator, PRINT_CODEGEN_WARNINGS, TWINE_CATCH_ERRORS, Wikifier, addClickHandler, addStyle, alterCSS, clone, decompile, delta, fade, findPassageParent, hasTransition, insertElement, insertText, internalEval, isCyclic, isElement, isNode, macros, makeRequest, nylonErrorHandler, recompile, removeChildren, rot13, scrollWindowInterval, scrollWindowTo, setPageElement, setTransitionCSS, state, syncFetch, tale, throwError, version,
  indexOf = [].indexOf,
  boundMethodCheck = function(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new Error('Bound instance method accessed before binding'); } },
  hasProp = {}.hasOwnProperty;

// @requires: engine/_globals.coffee

// Set to debug code generation
// (macros.button.callback, macros.set.run, Wikifier.linkFormatter.makeLink, etc)
DEBUG_CODEGEN = false;

// Prints warnings in codegen.
PRINT_CODEGEN_WARNINGS = true;

TWINE_CATCH_ERRORS = true;

ENodeTypes = {
  ELEMENT: 1,
  ATTR: 2,
  TEXT: 3,
  CDATASECTION: 4,
  ENTITYREFERENCE: 5,
  ENTITY: 6,
  PROCESSINGINSTRUCTION: 7,
  COMMENT: 8,
  DOCUMENT: 9,
  DOCUMENTTYPE: 10,
  DOCUMENTFRAGMENT: 11,
  NOTATION: 12
};

FORBIDDEN_INSERT_NODETYPES = [];

nylonErrorHandler = function(place, parser, exception, message) {
  if (parser) {
    console.error('Nylon caught an exception while parsing ' + parser.fullMatch() + '.');
  } else {
    console.error('Nylon caught an exception (parser not present).');
  }
  if (message) {
    console.error(message);
  }
  console.error(exception);
};

// FROM TWINE 2
/* Used to eval within Twine, but outside the context of a particular function */
internalEval = function(s) {
  // eval("{x:1,y:1}") fails miserably unless the obj. literal
  // is not the first expression in the line (hence, "0,").
  return eval('0,' + s);
};

window.internalEval = internalEval;

clone = function(orig) {
  /*
  	Create a copy of the original object.

  	NOTE: 1. Each non-generic object that we wish to support must receive a special
  	         case below.
  	      2. Since we're using the `instanceof` operator to identify special cases,
  	         this may fail to properly identify such cases if the author engages in
  	         cross-<iframe> object manipulation.  The solution to this problem would
  	         be for the author to pass messages between the frames, rather than doing
  	         direct cross-frame object manipulation.  That is, in fact, what they should
  	         be doing in the first place.
  	      3. We cannot use `Object.prototype.toString.call(orig)` to solve #2 because
  	         the shims for `Map` and `Set` return `[object Object]` rather than
  	         `[object Map]` and `[object Set]`.
  */
  var copy;
  /*
  	Immediately return non-objects.
  */
  if (typeof orig !== 'object' || orig === null) {
    // lazy equality for null
    return orig;
  }
  /*
  	Honor native clone methods.
  */
  if (typeof orig.clone === 'function') {
    return orig.clone(true);
  } else if (orig.nodeType && typeof orig.cloneNode === 'function') {
    return orig.cloneNode(true);
  }
  copy = void 0;
  if (Array.isArray(orig)) {
    // considering #2, `orig instanceof Array` might be more appropriate
    copy = [];
  } else if (orig instanceof Date) {
    copy = new Date(orig.getTime());
  } else if (orig instanceof Map) {
    copy = new Map();
    orig.forEach(function(val, key) {
      copy.set(key, clone(val));
    });
  } else if (orig instanceof RegExp) {
    copy = new RegExp(orig);
  } else if (orig instanceof Set) {
    copy = new Set();
    orig.forEach(function(val) {
      copy.add(clone(val));
    });
  } else {
    // unknown or generic object
    // Try to ensure that the returned object has the same prototype as the original.
    copy = Object.create(Object.getPrototypeOf(orig));
  }
  /*
  	Duplicate the original object's own enumerable properties, which will include expando
  	properties on non-generic objects.

  	NOTE: This does not preserve ES5 property attributes.  Neither does the delta coding
  	      or serialization code, however, so it's not really an issue at the moment.
  */
  Object.keys(orig).forEach(function(name) {
    copy[name] = clone(orig[name]);
  });
  return copy;
};

// Nylon
// Used in sanity checking in insertElement/insertText
// https://stackoverflow.com/a/384380
///////////////////////////////////////
//Returns true if it is a DOM node
isNode = function(o) {
  if (typeof Node === 'object') {
    return o instanceof Node;
  } else {
    return o && typeof o === 'object' && typeof o.nodeType === 'number' && typeof o.nodeName === 'string';
  }
};

//Returns true if it is a DOM element
isElement = function(o) {
  if (typeof HTMLElement === 'object') {
    return o instanceof HTMLElement;
  } else {
    return o && typeof o === 'object' && o !== null && o.nodeType === 1 && typeof o.nodeName === 'string';
  }
};

isCyclic = function(obj) {
  var properties;
  properties = [];
  return (function(obj) {
    var i, key, ownProps;
    key = void 0;
    i = void 0;
    ownProps = [];
    if (obj && typeof obj === 'object') {
      if (properties.indexOf(obj) > -1) {
        return true;
      }
      properties.push(obj);
      for (key in obj) {
        key = key;
        if (Object.prototype.hasOwnProperty.call(obj, key) && recurse(obj[key])) {
          return true;
        }
      }
    }
    return false;
  })(obj);
};

insertElement = function(a, d, f, c, e) {
  var b, ex;
  // a = parent
  // d = tag
  // f = id
  // c = class
  // e = text
  b = document.createElement(d);
  try {
    if (f) {
      b.id = f;
    }
    if (c) {
      b.className = c;
    }
    if (e) {
      insertText(b, e);
    }
    if (a) {
      a.appendChild(b);
    }
  } catch (error) {
    ex = error;
    console.log('Failed insertElement(parent=%s, tag="%s", id="%s", class="%s", text="%s")', a, d, f, c, e);
    console.log(ex);
    if (!TWINE_CATCH_ERRORS) {
      throw ex;
    }
  }
  return b;
};

addClickHandler = function(el, fn) {
  if (el.addEventListener) {
    el.addEventListener('click', fn);
  } else if (el.attachEvent) {
    el.attachEvent('onclick', fn);
  }
};

insertText = function(a, b) {
  var ex;
  try {
    return a.appendChild(document.createTextNode(b));
  } catch (error) {
    ex = error;
    console.log('Failed insertText(parent=%s, text="%s")', a, b);
    throw ex;
  }
};

removeChildren = function(a) {
  while (a.hasChildNodes()) {
    a.removeChild(a.firstChild);
  }
};

findPassageParent = function(el) {
  while (el && el !== document.body && !~el.className.indexOf('passage')) {
    el = el.parentNode;
  }
  if (el === document.body) {
    return null;
  } else {
    return el;
  }
};

setPageElement = function(c, b, a) {
  var place;
  place = void 0;
  if (place = (typeof c === 'string' ? document.getElementById(c) : c)) {
    removeChildren(place);
    if (tale.has(b)) {
      tale.get(b, (page) => {
        return new Wikifier(place, page.processText());
      });
    } else {
      new Wikifier(place, a);
    }
  }
};

scrollWindowTo = function(e, margin) {
  var a, b, c, d, g, h, j, k, m, scrollWindowInterval;
  d = window.scrollY ? window.scrollY : document.documentElement.scrollTop;
  m = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight;
  g = k(e);
  j = d > g ? -1 : 1;
  b = 0;
  c = Math.abs(d - g);
  h = function() {
    b += 0.1;
    window.scrollTo(0, d + j * c * Math.easeInOut(b));
    if (b >= 1) {
      window.clearInterval(scrollWindowInterval);
    }
  };
  k = function(o) {
    var h;
    var n, p;
    p = a(o);
    h = o.offsetHeight;
    n = d + m;
    p = Math.min(Math.max(p + (margin || 0) * (p < d ? -1 : 1), 0), n);
    if (p < d) {
      return p;
    } else {
      if (p + h > n) {
        if (h < m) {
          return p - (m - h) + 20;
        } else {
          return p;
        }
      } else {
        return p;
      }
    }
  };
  a = function(l) {
    var m;
    m = 0;
    while (l.offsetParent) {
      m += l.offsetTop;
      l = l.offsetParent;
    }
    return m;
  };
  scrollWindowInterval && window.clearInterval(scrollWindowInterval);
  if (c) {
    scrollWindowInterval = window.setInterval(h, 25);
  }
};

// Returns an object containing the properties in neu which differ from old.
delta = function(old, neu) {
  var ret, vars;
  vars = void 0;
  ret = {};
  if (old && neu) {
    for (vars in neu) {
      vars = vars;
      if (neu[vars] !== old[vars]) {
        ret[vars] = neu[vars];
      }
    }
  }
  return ret;
};

// Convert various exotic objects into JSON-serialisable plain objects.
decompile = function(val) {
  var e, i, ret;
  i = void 0;
  ret = void 0;
  if (typeof val !== 'object' && typeof val !== 'function' || !val) {
    return val;
  } else if (val.decompile !== void 0 && typeof val.decompile === 'function') {
    //console.log("Decompiling %s...", val.constructor.name);
    return val.decompile();
  } else if (val instanceof Passage) {
    return {
      '[[Passage]]': val.id
    };
  } else if (Array.isArray(val)) {
    ret = [];
  } else {
    ret = {};
  }
// Deep-copy own properties
  for (i in val) {
    i = i;
    if (Object.prototype.hasOwnProperty.call(val, i) && !isCyclic(val[i])) {
      ret[i] = decompile(val[i]);
    }
  }
  // Functions: store the decompiled function body as a property
  // arbitrarily called "[[Call]]".
  if (typeof val === 'function' || val instanceof RegExp) {
    try {
      // Check if it can be recompiled (i.e. isn't a bound or native)
      internalEval(val + '');
      ret['[[Call]]'] = val + '';
    } catch (error) {
      e = error;
      // Silently fail
      ret['[[Call]]'] = 'function(){}';
    }
  }
  return ret;
};

// Reverses the effects of decompile()
// Takes recently JSON.parse()d objects and makes them exotic again.
recompile = function(val) {
  var e, i, ret;
  i = void 0;
  ret = val;
  //console.log("Recompile %s", typeof val['_type']);
  if (val && typeof val === 'object') {
    if (typeof val['_type'] === 'string') {
      //console.log("Recompiling %s...", val["_type"]);
      ret = new (val['_type'])();
      ret.deserialize(val);
      return ret;
    }
    // Passages
    //if typeof val['[[Passage]]'] == 'number'
    //  return tale.get(val['[[Passage]]'])
    // NO.

    // Functions/RegExps
    if (typeof val['[[Call]]'] === 'string') {
      try {
        ret = internalEval(val['[[Call]]']);
      } catch (error) {
        e = error;
      }
    }
// Recursively recompile all properties
// or, if a function/regexp, deep-copy them to the new function.
    for (i in val) {
      i = i;
      if (Object.prototype.hasOwnProperty.call(val, i)) {
        ret[i] = recompile(val[i]);
      }
    }
  }
  return ret;
};

addStyle = function(b) {
  var a;
  if (document.createStyleSheet) {
    document.getElementsByTagName('head')[0].insertAdjacentHTML('beforeEnd', '&nbsp;<style>' + b + '</style>');
  } else {
    a = document.createElement('style');
    a.appendChild(document.createTextNode(b));
    document.getElementsByTagName('head')[0].appendChild(a);
  }
};

alterCSS = function(text) {
  var imgPassages, temp;
  temp = '';
  imgPassages = tale.lookup('tags', 'Twine.image');
  // Remove comments
  text = text.replace(/\/\*(?:[^\*]|\*(?!\/))*\*\//g, '');
  // Replace :link
  text = text.replace(/:link/g, '[class*=Link]');
  // Replace :visited
  text = text.replace(/:visited/g, '.visitedLink');
  // Hoist @import
  text = text.replace(/@import\s+(?:url\s*\(\s*['"]?|['"])[^"'\s]+(?:['"]?\s*\)|['"])\s*([\w\s\(\)\d\:,\-]*);/g, function(e) {
    temp += e;
    return '';
  });
  text = temp + text;
  // Add images
  return text.replace(new RegExp(Wikifier.imageFormatter.lookahead, 'gim'), function(m, p1, p2, p3, src) {
    var i;
    i = 0;
    while (i < imgPassages.length) {
      if (imgPassages[i].title === src) {
        src = imgPassages[i].text;
        break;
      }
      i++;
    }
    return 'url(' + src + ')';
  });
};

setTransitionCSS = function(styleText) {
  var style;
  styleText = alterCSS(styleText);
  style = document.getElementById('transitionCSS');
  if (style.styleSheet) {
    (style.styleSheet.cssText = styleText);
  } else {
    (style.innerHTML = styleText);
  }
};

throwError = function(a, b, tooltip, exception = null) {
  var elem;
  if (a) {
    elem = insertElement(a, 'span', null, 'marked', b);
    tooltip && elem.setAttribute('title', tooltip);
  } else {
    alert('Regrettably, this ' + tale.identity() + '\'s code just ran into a problem:\n' + b + '.\n' + softErrorMessage);
  }
};

rot13 = function(s) {
  return s.replace(/[a-zA-Z]/g, function(c) {
    return String.fromCharCode((c <= 'Z' ? 90 : 122) >= (c = c.charCodeAt() + 13) ? c : c - 26);
  });
};

fade = function(f, c) {
  var a, b, d, e, g, h;
  h = void 0;
  e = f.cloneNode(true);
  g = c.fade === 'in' ? 1 : -1;
  d = function() {
    h += 0.05 * g;
    b(e, Math.easeInOut(h));
    if (g === 1 && h >= 1 || g === -1 && h <= 0) {
      f.style.visibility = c.fade === 'in' ? 'visible' : 'hidden';
      if (e.parentNode) {
        e.parentNode.replaceChild(f, e);
      }
      window.clearInterval(a);
      if (c.onComplete) {
        c.onComplete.call(f);
      }
    }
  };
  b = function(k, j) {
    var l;
    l = Math.floor(j * 100);
    k.style.zoom = 1;
    k.style.filter = 'alpha(opacity=' + l + ')';
    k.style.opacity = j;
  };
  f.parentNode.replaceChild(e, f);
  if (c.fade === 'in') {
    h = 0;
    e.style.visibility = 'visible';
  } else {
    h = 1;
  }
  b(e, h);
  a = window.setInterval(d, 25);
};

window.clone = clone;

window.insertElement = insertElement;

window.addClickHandler = addClickHandler;

window.insertText = insertText;

window.setPageElement = setPageElement;

// Nylon
scrollWindowInterval = void 0;

Math.easeInOut = function(a) {
  return 1 - ((Math.cos(a * Math.PI) + 1) / 2);
};

String.prototype.readMacroParams = function(keepquotes) {
  var exec, params, re, val;
  exec = void 0;
  re = /(?:\s*)(?:(?:"([^"]*)")|(?:'([^']*)')|(?:\[\[((?:[^\]]|\](?!\]))*)\]\])|([^"'\s]\S*))/mg;
  params = [];
  while (true) {
    val = void 0;
    exec = re.exec(this);
    if (exec) {
      if (exec[1]) {
        val = exec[1];
        keepquotes && (val = '"' + val + '"');
      } else if (exec[2]) {
        val = exec[2];
        keepquotes && (val = '\'' + val + '\'');
      } else if (exec[3]) {
        val = exec[3];
        keepquotes && (val = '"' + val.replace('"', '\"') + '"');
      } else if (exec[4]) {
        val = exec[4];
      }
      val && params.push(val);
    }
    if (!exec) {
      break;
    }
  }
  return params;
};

String.prototype.readBracketedList = function() {
  var a, b, c, d, e, f;
  c = void 0;
  b = '\\[\\[([^\\]]+)\\]\\]';
  a = '[^\\s$]+';
  e = '(?:' + b + ')|(' + a + ')';
  d = new RegExp(e, 'mg');
  f = [];
  while (true) {
    c = d.exec(this);
    if (c) {
      if (c[1]) {
        f.push(c[1]);
      } else {
        if (c[2]) {
          f.push(c[2]);
        }
      }
    }
    if (!c) {
      break;
    }
  }
  return f;
};

/*
**
** Polyfills
**
 */
Object.create || (function() {
  var F;
  F = function() {};
  Object.create = function(o) {
    if (typeof o !== 'object') {
      throw TypeError();
    }
    F.prototype = o;
    return new F();
  };
})();

String.prototype.trim || (String.prototype.trim = function() {
  return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
});

Array.isArray || (Array.isArray = function(arg) {
  return Object.prototype.toString.call(arg) === '[object Array]';
});

Array.prototype.indexOf || (Array.prototype.indexOf = function(b, d) {
  var a, c;
  d = d === null ? 0 : d;
  a = this.length;
  c = d;
  while (c < a) {
    if (this[c] === b) {
      return c;
    }
    c++;
  }
  return -1;
});

Array.prototype.forEach || (Array.prototype.forEach = function(fun) {
  var i, len, t, thisArg;
  if (this === null) {
    throw TypeError();
  }
  t = Object(this);
  len = +t.length;
  if (typeof fun !== 'function') {
    throw TypeError();
  }
  thisArg = arguments.length >= 2 ? arguments[1] : void 0;
  i = 0;
  while (i < len) {
    if (i in t) {
      fun.call(thisArg, t[i], i, t);
    }
    i++;
  }
});

hasTransition = 'transition' in document.documentElement.style || '-webkit-transition' in document.documentElement.style;

window.arrayBufferToString = function(buf) {
  return String.fromCharCode.apply(null, new Uint8Array(buf));
};

window.stringToArrayBuffer = function(str) {
  var buf, bufView, i, strLen;
  buf = new ArrayBuffer(str.length * 2);
  // 2 bytes for each char
  bufView = new Uint8Array(buf);
  i = 0;
  strLen = str.length;
  while (i < strLen) {
    bufView[i] = str.charCodeAt(i);
    i++;
  }
  return buf;
};

Function.prototype.getter = function(prop, get) {
  return Object.defineProperty(this.prototype, prop, {
    get,
    configurable: true
  });
};

Function.prototype.setter = function(prop, set) {
  return Object.defineProperty(this.prototype, prop, {
    set,
    configurable: true
  });
};

Function.prototype.property = function(prop, desc) {
  return Object.defineProperty(this.prototype, prop, desc);
};

macros = {};

version = {
  extensions: {}
};

tale = state = null;

window.HALT_WAIT = false;

/*
 * Nylon passage object.
 *
 * Rewritten to no longer need the DOM for init.
 */
NylonPassage = class NylonPassage {
  static unescapeLineBreaks(a) {
    if (a && typeof a === "string") {
      return a.replace(/\\n/mg, "\n").replace(/\\t/mg, "\t").replace(/\\s/mg, "\\").replace(/\\/mg, "\\").replace(/\r/mg, "");
    } else {
      return "";
    }
  }

  constructor(title1, id1 = -1, tags1 = [], text1 = '', deobfuscator1 = null, config1 = null) {
    this.title = title1;
    this.id = id1;
    this.tags = tags1;
    this.text = text1;
    this.deobfuscator = deobfuscator1;
    this.config = config1;
    this.element = null;
    if (this.id > -1) {
      this.preload();
    }
  }

  preload() {
    var tag;
    this.text = NylonPassage.unescapeLineBreaks(this.text);
    if (this.deobfuscator) {
      this.deobfuscator.prepare(this.id, this.config);
      this.title = this.deobfuscate(this.title);
      this.text = this.deobfuscate(this.text);
      this.tags = [
        (function() {
          var len1,
        ref,
        results,
        x;
          ref = this.tags;
          results = [];
          for (x = 0, len1 = ref.length; x < len1; x++) {
            tag = ref[x];
            results.push(this.deobfuscate(tag));
          }
          return results;
        }).call(this)
      ];
    }
    if (!this.isImage()) {
      this.preloadImages();
    }
    // Check for the .char selector, or the [data-char] selector
    // False positives aren't a big issue.
    if (/\.char\b|\[data\-char\b/.exec(this.text) && Wikifier.charSpanFormatter) {
      Wikifier.formatters.push(Wikifier.charSpanFormatter);
      return delete Wikifier.charSpanFormatter;
    }
  }

  deobfuscate(a) {
    if (deobfuscator) {
      return deobfuscator.deobfuscate(a);
    } else {
      return a;
    }
  }

  isImage() {
    return indexOf.call(this.tags, 'Twine.image') >= 0;
  }

  preloadImages() {
    var k, u;
    // Don't preload URLs containing '$' - suspect that they are variables.
    u = '\\s*[\'"]?([^"\'$]+\\.(jpe?g|a?png|gif|bmp|webp|svg))[\'"]?\\s*';
    k = function(c, e) {
      var d, i;
      i = void 0;
      d = void 0;
      while (true) {
        d = c.exec(this.text);
        if (d) {
          i = new Image();
          i.src = d[e];
        }
        if (!d) {
          break;
        }
      }
      return k;
    };
    k.call(this, new RegExp(Wikifier.imageFormatter.lookahead.replace('[^\\[\\]\\|]+', u), 'mg'), 4).call(this, new RegExp('url\\s*\\(' + u + '\\)', 'mig'), 1).call(this, new RegExp('src\\s*=' + u, 'mig'), 1);
  }

  setTags(b) {
    var t;
    t = this.tags !== null && this.tags.length ? this.tags.join(' ') : '';
    if (t) {
      b.setAttribute('data-tags', this.tags.join(' '));
    }
    document.body.setAttribute('data-tags', t);
    document.getElementsByTagName('html')[0].setAttribute('data-tags', t);
  }

  processText() {
    var ret;
    ret = this.text;
    if (indexOf.call(this.tags, 'nobr') >= 0) {
      ret = ret.replace(/\n/g, '‌');
    }
    if (this.isImage()) {
      ret = '[img[' + ret + ']]';
    }
    return ret;
  }

};

NylonDeobfuscator = (function() {
  class NylonDeobfuscator {
    static Register(cls) {
      return this._ALL[cls.ID] = cls;
    }

    static CreateByID(story, id, config) {
      console && console.log('Selected deobfuscator', id);
      return new this._ALL[id](story, config);
    }

    constructor(ID, Story, Config) {
      this.ID = ID;
      this.Story = Story;
      this.Config = Config;
      return;
    }

    Deobfuscate(data) {
      return data;
    }

  };

  NylonDeobfuscator.ID = '';

  NylonDeobfuscator.NAME = '';

  NylonDeobfuscator._ALL = {};

  return NylonDeobfuscator;

}).call(this);

NylonVigenereDeobfuscator = (function() {
  class NylonVigenereDeobfuscator extends NylonDeobfuscator {
    constructor(story, config) {
      super('vigenere', story, config);
      this.Key = this.Config.key;
    }

    Deobfuscate(data) {
      var e, i, k, o, ref, string, x;
      string = new Uint8Array(data);
      o = new Uint8Array(string.length);
      for (i = x = 0, ref = data.length; (0 <= ref ? x <= ref : x >= ref); i = 0 <= ref ? ++x : --x) {
        k = this.Key.charCodeAt(i % this.Key.length);
        e = (string[i] - k + 256) % 256;
        o[i] = e;
      }
      return new ArrayBuffer(o);
    }

  };

  NylonVigenereDeobfuscator.ID = 'vigenere';

  NylonVigenereDeobfuscator.NAME = 'Vigenere Cypher';

  return NylonVigenereDeobfuscator;

}).call(this);

NylonDeobfuscator.Register(NylonVigenereDeobfuscator);

NylonPassageLoader = (function() {
  class NylonPassageLoader {
    static Register(cls) {
      this._ALL[cls.ID] = cls;
    }

    static CreateByID(story, id, config) {
      console && console.log('Selected loader', id);
      return new this._ALL[id](story, config);
    }

    constructor(Story, Config) {
      this.has = this.has.bind(this);
      this.load = this.load.bind(this);
      this.Story = Story;
      this.Config = Config;
      this.Passages = {};
      this.Progress = function(message, step, steps) {
        return console.log(`(${step}/${steps}) ${message}`);
      };
    }

    has(passageID) {
      return passageID in this.Passages;
    }

    get(passageID, done) {}

    _get(passageID) {
      return this._ALL[passageID];
    }

    load(opts) {
      if ('progress' in opts) {
        this.Progress = opts.progress;
      }
    }

  };

  NylonPassageLoader._ALL = {};

  return NylonPassageLoader;

}).call(this);

version.extensions.backMacro = {
  major: 2,
  minor: 0,
  revision: 0
};

macros.back = {
  labeltext: '&#171; back',
  handler: function(a, b, e, parser) {
    var c, el, labelParam, labeltouse, r, steps, stepsParam, stepsParam2;
    labelParam = void 0;
    c = void 0;
    el = void 0;
    labeltouse = this.labeltext;
    steps = 1;
    stepsParam = e.indexOf('steps');
    stepsParam2 = '';
    // Steps parameter
    if (stepsParam > 0) {
      stepsParam2 = e[stepsParam - 1];
      if (stepsParam2[0] === '$') {
        try {
          stepsParam2 = internalEval(Wikifier.parse(stepsParam2));
        } catch (error) {
          r = error;
          nylonErrorHandler(a, parser, r, '<<back>> steps: bad expression');
          throwError(a, parser.fullMatch() + ' bad expression: ' + r.message, parser.fullMatch());
          return;
        }
      }
      // Previously, trying to go back more steps than were present in the
      // history would silently revert to just 1 step.
      // Instead, let's just go back to the start.
      steps = +stepsParam2;
      if (steps >= state.history.length - 1) {
        steps = state.history.length - 2;
      }
      e.splice(stepsParam - 1, 2);
    }
    // Label parameter
    labelParam = e.indexOf('label');
    if (labelParam > -1) {
      if (!e[labelParam + 1]) {
        nylonErrorHandler(a, parser, null, '<<back>> label: keyword needs an additional label parameter.');
        throwError(a, parser.fullMatch() + ': ' + e[labelParam] + ' keyword needs an additional label parameter', parser.fullMatch());
        return;
      }
      labeltouse = e[labelParam + 1];
      e.splice(labelParam, 2);
    }
    // What's left is the passage name parameter
    if (stepsParam <= 0) {
      if (e[0]) {
        if (e[0].charAt(0) === '$') {
          try {
            e = internalEval(Wikifier.parse(e[0]));
          } catch (error) {
            r = error;
            nylonErrorHandler(a, parser, r, '<<back>> name: bad expression');
            throwError(a, parser.fullMatch() + ' bad expression: ' + r.message, parser.fullMatch());
            return;
          }
        } else {
          e = e[0];
        }
        if (!tale.has(e)) {
          nylonErrorHandler(a, parser, r, `<<back>> name: Passage ${e} does not exist.`);
          throwError(a, `The \"${e}\" passage does not exist`, parser.fullMatch());
          return;
        }
        c = 0;
        while (c < state.history.length) {
          if (state.history[c].passage.title === e) {
            steps = c;
            break;
          }
          c++;
        }
      }
    }
    el = document.createElement('a');
    el.className = b;
    addClickHandler(el, (function(b) {
      return function() {
        return macros.back.onclick(b === 'back', steps, el);
      };
    })(b));
    el.innerHTML = labeltouse;
    a.appendChild(el);
  }
};

// ---
// generated by js2coffee 2.2.0
version.extensions.choiceMacro = {
  major: 2,
  minor: 0,
  revision: 0
};

macros.choice = {
  callback: function() {
    var i, other, passage;
    i = void 0;
    other = void 0;
    passage = findPassageParent(this);
    if (passage) {
      other = passage.querySelectorAll('.choice');
      i = 0;
      while (i < other.length) {
        other[i].outerHTML = '<span class=disabled>' + other[i].innerHTML + '</span>';
        i++;
      }
      state.history[0].variables['choice clicked'][passage.id.replace(/\|[^\]]*$/, '')] = true;
    }
  },
  handler: function(A, C, D, parser) {
    /* End patch */
    var id, link, match, passage, text;
    link = void 0;
    id = void 0;
    match = void 0;
    text = D[1] || D[0].split('|')[0];
    passage = findPassageParent(A);
    if (!passage) {
      throwError(A, '<<' + C + '>> can\'t be used here.', parser.fullMatch());
      return;
    }
    id = passage && passage.id.replace(/\|[^\]]*$/, '');
    if (id && (state.history[0].variables['choice clicked'] || (state.history[0].variables['choice clicked'] = {}))[id]) {
      insertElement(A, 'span', null, 'disabled', text);
      /* Nylon Patch for Twine 1.4.2 Sugarcane */
      window.twineCurrentEl = A;
      window.twineCurrentParser = parser;
    } else {
      match = new RegExp(Wikifier.linkFormatter.lookahead).exec(parser.fullMatch());
      if (match) {
        link = Wikifier.linkFormatter.makeLink(A, match, this.callback);
      } else {
        link = Wikifier.linkFormatter.makeLink(A, [0, text, D[0]], this.callback);
      }
      link.className += ' ' + C;
    }
  }
};

// ---
// generated by js2coffee 2.2.0
version.extensions.forgetMacro = {
  major: 1,
  minor: 0,
  revision: 0
};

macros.forget = {
  handler: function(place, macroName, params) {
    /* End patch */
    var match, re, statement, variable;
    re = void 0;
    match = void 0;
    variable = void 0;
    statement = params.join(' ');
    /* Nylon Patch for Twine 1.4.2 Sugarcane */
    window.twineCurrentEl = place;
    window.twineCurrentParser = parser;
    re = new RegExp(Wikifier.textPrimitives.variable, 'g');
    while (match = re.exec(statement)) {
      variable = match[1] + '';
      delete state.history[0].variables[variable];
      delete window.localStorage[macros.remember.prefix + variable];
    }
  }
};

// ---
// generated by js2coffee 2.2.0
version.extensions.textInputMacro = {
  major: 2,
  minor: 0,
  revision: 0
};

macros.checkbox = macros.radio = macros.textinput = {
  handler: function(A, C, D, parser) {
    var class_, id, input, match, q;
    match = void 0;
    class_ = C.replace('input', 'Input');
    q = A.querySelectorAll('input');
    id = class_ + '|' + (q && q.length || 0);
    input = insertElement(null, 'input', id, class_);
    input.name = D[0];
    input.type = C.replace('input', '');
    // IE 8 support - delay insertion until now
    A.appendChild(input);
    if (C === 'textinput' && D[1]) {
      match = new RegExp(Wikifier.linkFormatter.lookahead).exec(parser.fullMatch());
      if (match) {
        Wikifier.linkFormatter.makeLink(A, match, macros.button.callback, 'button');
      } else {
        Wikifier.linkFormatter.makeLink(A, [0, D[2] || D[1], D[1]], macros.button.callback, 'button');
      }
    } else if ((C === 'radio' || C === 'checkbox') && D[1]) {
      input.value = D[1];
      insertElement(A, 'label', '', '', D[1]).setAttribute('for', id);
      if (D[2]) {
        insertElement(A, 'br');
        D.splice(1, 1);
        macros[C].handler(A, C, D);
      }
    }
    /* Nylon Patch for Twine 1.4.2 Sugarcane */
    window.twineCurrentEl = A;
    window.twineCurrentParser = parser;
  }
};

// ---
// generated by js2coffee 2.2.0
/* End patch */
version.extensions.rememberMacro = {
  major: 2,
  minor: 0,
  revision: 0
};

macros.remember = {
  handler: function(place, macroName, params, parser) {
    var e, match, re, statement, value, variable;
    variable = void 0;
    value = void 0;
    re = void 0;
    match = void 0;
    statement = params.join(' ');
    /* Nylon Patch for Twine 1.4.2 Sugarcane */
    window.twineCurrentEl = place;
    window.twineCurrentParser = parser;
    /* End patch */
    macros.set.run(place, parser.fullArgs(), null, params.join(' '));
    if (!window.localStorage) {
      throwError(place, '<<remember>> can\'t be used ' + (window.location.protocol === 'file:' ? ' by local HTML files ' : '') + ' in this browser.', parser.fullMatch());
      return;
    }
    re = new RegExp(Wikifier.textPrimitives.variable, 'g');
    while (match = re.exec(statement)) {
      variable = match[1];
      value = state.history[0].variables[variable];
      try {
        value = JSON.stringify(value);
      } catch (error) {
        e = error;
        throwError(place, 'can\'t <<remember>> the variable $' + variable + ' (' + typeof value + ')', parser.fullMatch());
        return;
      }
      window.localStorage[this.prefix + variable] = value;
    }
  },
  init: function() {
    var e, i, value, variable;
    i = void 0;
    variable = void 0;
    value = void 0;
    this.prefix = 'Twine.' + tale.defaultTitle + '.';
    for (i in window.localStorage) {
      i = i;
      if (i.indexOf(this.prefix) === 0) {
        variable = i.substr(this.prefix.length);
        value = window.localStorage[i];
        try {
          value = JSON.parse(value);
          state.history[0].variables[variable] = value;
        } catch (error) {
          e = error;
        }
      }
    }
  },
  expire: null,
  prefix: null
};

// ---
// generated by js2coffee 2.2.0
version.extensions.setMacro = {
  major: 1,
  minor: 1,
  revision: 0
};

macros.set = {
  handler: function(a, b, c, parser) {
    /* Nylon Patch for Twine 1.4.2 Sugarcane */
    window.twineCurrentEl = a;
    window.twineCurrentParser = parser;
    /* End patch */
    macros.set.run(a, parser.fullArgs(), parser, c.join(' '));
  },
  run: function(a, expression, parser, original) {
    var e;
    try {
      return internalEval(expression);
    } catch (error) {
      e = error;
      /* Nylon Patch for Twine 1.4.2 Sugarcane */
      console.log('Bad expr: ' + expression);
      console.log('Bad orig: ' + original);
      nylonErrorHandler(a, parser, e, '<<set>> bad expression: ' + (original || expression));
      /* End patch */
      throwError(a, 'bad expression: ' + (original || expression), parser ? parser.fullMatch() : expression);
    }
  }
};

// ---
// generated by js2coffee 2.2.0
/*
 * A random integer function
 * 1 argument: random int from 0 to a inclusive
 * 2 arguments: random int from a to b inclusive (order irrelevant)
 */
window.random = function(a, b) {
  var from, to;
  from = void 0;
  to = void 0;
  if (!b) {
    from = 0;
    to = a;
  } else {
    from = Math.min(a, b);
    to = Math.max(a, b);
  }
  to += 1;
  return ~~(Math.random() * (to - from)) + from;
};

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
window.random.float = function(min, max) {
  return Math.random() * (max - min) + min;
};

/*
 * The maximum is inclusive and the minimum is inclusive
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 */
window.random.int = function(a, b) {
  var max, min;
  min = void 0;
  max = void 0;
  if (!b) {
    min = 0;
    max = a;
  } else {
    [min, max] = [a, b];
  }
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
};

/*
 * Random item from list
 */
window.random.choice = function(items) {
  return items[items.length * Math.random() | 0];
};

window.either = window.random.choice;

NylonNullDeobfuscator = (function() {
  class NylonNullDeobfuscator extends NylonDeobfuscator {
    constructor(story, config) {
      super(null, story, config);
    }

    Deobfuscate(data) {
      return data;
    }

  };

  NylonNullDeobfuscator.ID = null;

  NylonNullDeobfuscator.NAME = 'NULL';

  return NylonNullDeobfuscator;

}).call(this);

NylonDeobfuscator.Register(NylonNullDeobfuscator);

/*
 * Provides base lazy-loading capabilities.
 */
NylonLazyLoader = class NylonLazyLoader extends NylonPassageLoader {
  constructor(story, config) {
    super(story, config);
    this.get = this.get.bind(this);
    this.PreloadData = {};
    this.LoadLazily = 'load-lazily' in config ? config['load-lazily'] : true;
    this.LoadedPassageCount = 0;
    this.PreloadedPassageCount = 0;
  }

  load() {}

  preloadPassage(id, index, data, done) {
    this.Passages[id] = null;
    this.PreloadData[id] = {
      name: id,
      index: index,
      data: data
    };
    this.PreloadedPassages++;
    if (!this.LoadLazily) {
      return this.loadPassage(id, function() {
        this.progress && this.progress.progress(`Loading ${mod_id} passages`, this.LoadedPassageCount, this.PreloadedPassageCount);
        this.LoadedPassageCount++;
        if (this.PreloadedPassageCount === this.LoadedPassageCount) {
          return done && done();
        }
      });
    } else {
      return done && done();
    }
  }

  has(id) {
    return id in this.PreloadData;
  }

  loadPassage(passageID, cb) {}

  get(id, done) {
    boundMethodCheck(this, NylonLazyLoader);
    if (!(id in this.Passages)) {
      return null;
    }
    if (this.Passages[id] === null) {
      return this.loadPassage(id, done);
    } else {
      return done(this.Passages[id]);
    }
  }

};

version.extensions.buttonMacro = {
  major: 1,
  minor: 0,
  revision: 0
};

macros.button = {
  callback: function() {
    var codegen, el, i, inputs;
    el = findPassageParent(this);
    if (el) {
      inputs = el.querySelectorAll('input');
      i = 0;
      while (i < inputs.length) {
        // VGFix: Sanity checks.
        if (inputs[i] === null || inputs[i].name === '') {
          //console.log("macros.button.callback: ", inputs[i]);
          if (PRINT_CODEGEN_WARNINGS) {
            console.warn('macros.button.callback: Skipping %s (invalid)', inputs[i]);
          }
          i++;
          continue;
        }
        codegen = '';
        if (inputs[i].type !== 'checkbox' && (inputs[i].type !== 'radio' || inputs[i].checked)) {
          codegen = inputs[i].name + ' = "' + inputs[i].value.replace(/"/g, '\"') + '"';
        } else if (inputs[i].type === 'checkbox' && inputs[i].checked) {
          codegen = inputs[i].name + ' = [].concat(' + inputs[i].name + ' || []);';
          codegen += inputs[i].name + '.push("' + inputs[i].value.replace(/"/g, '\"') + '")';
        }
        if (codegen !== '') {
          //console.log("macros.button.callback: processing %s",codegen);
          macros.set.run(null, Wikifier.parse(codegen));
        }
        i++;
      }
    }
  },
  handler: function(A, C, D, parser) {
    var link, match;
    link = void 0;
    match = new RegExp(Wikifier.linkFormatter.lookahead).exec(parser.fullMatch());
    if (match) {
      Wikifier.linkFormatter.makeLink(A, match, this.callback, 'button');
    } else {
      Wikifier.linkFormatter.makeLink(A, [0, D[1] || D[0], D[0]], this.callback, 'button');
    }
  }
};

// ---
// generated by js2coffee 2.2.0
version.extensions.returnMacro = {
  major: 2,
  minor: 0,
  revision: 0
};

macros['return'] = {
  labeltext: '&#171; return',
  handler: function(a, b, e) {
    macros.back.handler.call(this, a, b, e);
  }
};

// ---
// generated by js2coffee 2.2.0
/*
 * Wikifier object
 *
 * Directly converted from engine.js. Needs more work probably.
 */
window.twineCurrentEl = null;

Wikifier = (function() {
  class Wikifier {
    constructor(place, source) {
      this.source = source;
      this.output = place;
      /* Nylon Patch for Twine 1.4.2 Sugarcane */
      window.twineCurrentEl = place;
      /* End patch */
      this.nextMatch = 0;
      this.assembleFormatterMatches(Wikifier.formatters);
      this.subWikify(this.output);
    }

    assembleFormatterMatches(formatters) {
      var formatter, len1, pattern, x;
      this.formatters = [];
      pattern = [];
      for (x = 0, len1 = formatters.length; x < len1; x++) {
        formatter = formatters[x];
        pattern.push('(' + formatter.match + ')');
        this.formatters.push(formatter);
      }
      this.formatterRegExp = new RegExp(pattern.join('|'), 'mg');
    }

    subWikify(output, terminator) {
      var formatterMatch, matchingFormatter, oldOutput, t, terminatorMatch, terminatorRegExp;
      // Temporarily replace the output pointer
      terminatorMatch = void 0;
      formatterMatch = void 0;
      oldOutput = this.output;
      this.output = output;
      // Prepare the terminator RegExp
      terminatorRegExp = terminator ? new RegExp('(' + terminator + ')', 'mg') : null;
      while (true) {
        // Prepare the RegExp match positions
        this.formatterRegExp.lastIndex = this.nextMatch;
        if (terminatorRegExp) {
          terminatorRegExp.lastIndex = this.nextMatch;
        }
        // Get the first matches
        formatterMatch = this.formatterRegExp.exec(this.source);
        terminatorMatch = terminatorRegExp ? terminatorRegExp.exec(this.source) : null;
        // Check for a terminator match
        if (terminatorMatch && (!formatterMatch || terminatorMatch.index <= formatterMatch.index)) {
          // Output any text before the match
          if (terminatorMatch.index > this.nextMatch) {
            this.outputText(this.output, this.nextMatch, terminatorMatch.index);
          }
          // Set the match parameters
          this.matchStart = terminatorMatch.index;
          this.matchLength = terminatorMatch[1].length;
          this.matchText = terminatorMatch[1];
          this.nextMatch = terminatorMatch.index + terminatorMatch[1].length;
          // Restore the output pointer and exit
          this.output = oldOutput;
          return;
        } else if (formatterMatch) {
          // Output any text before the match
          if (formatterMatch.index > this.nextMatch) {
            this.outputText(this.output, this.nextMatch, formatterMatch.index);
          }
          // Set the match parameters
          this.matchStart = formatterMatch.index;
          this.matchLength = formatterMatch[0].length;
          this.matchText = formatterMatch[0];
          this.nextMatch = this.formatterRegExp.lastIndex;
          // Figure out which formatter matched
          matchingFormatter = -1;
          t = 1;
          while (t < formatterMatch.length) {
            if (formatterMatch[t]) {
              matchingFormatter = t - 1;
              break;
            }
            t++;
          }
          // Call the formatter
          if (matchingFormatter !== -1) {
            this.formatters[matchingFormatter].handler(this);
          }
        }
        if (!(terminatorMatch || formatterMatch)) {
          break;
        }
      }
      // Output any text after the last match
      if (this.nextMatch < this.source.length) {
        this.outputText(this.output, this.nextMatch, this.source.length);
        this.nextMatch = this.source.length;
      }
      // Restore the output pointer
      this.output = oldOutput;
    }

    outputText(place, startPos, endPos) {
      var ex;
      if (place && isNode(place) && !(place.nodeType in FORBIDDEN_INSERT_NODETYPES)) {
        try {
          // VG - Sanity checking, or we get really weird errors.
          insertText(place, this.source.substring(startPos, endPos));
        } catch (error) {
          ex = error;
          console.log('outputText(place.nodeType=%d, startPos=%d, endPos=%d) = "%s"', place.nodeType, startPos, endPos, this.source.substring(startPos, endPos));
          console.log(ex);
        }
      }
    }

    //throw ex; And sometimes you just don't have a choice.
    fullMatch() {
      return this.source.slice(this.matchStart, this.source.indexOf('>>', this.matchStart) + 2);
    }

    fullArgs(includeName) {
      var endPos, source, startPos;
      source = this.source.replace(/\u200c/g, ' ');
      endPos = this.nextMatch - 2;
      startPos = source.indexOf((includeName ? '<<' : ' '), this.matchStart);
      if (!~startPos || !~endPos || endPos <= startPos) {
        return '';
      }
      return Wikifier.parse(source.slice(startPos + (includeName ? 2 : 1), endPos).trim());
    }

    static parse(input) {
      var alter, b, found, g, m, re;
      m = void 0;
      re = void 0;
      b = input;
      found = [];
      g = Wikifier.textPrimitives.unquoted;
      // Extract all the variables, and set them to 0 if undefined.
      alter = function(from, to) {
        b = b.replace(new RegExp(from + g, 'gim'), to);
        return alter;
      };
      re = new RegExp(Wikifier.textPrimitives.variable + g, 'gi');
      while (m = re.exec(input)) {
        if (!~found.indexOf(m[0])) {
          // This deliberately contains a 'null or undefined' check
          b = m[0] + ' == null && (' + m[0] + ' = 0);' + b;
          found.push(m[0]);
        }
        alter(Wikifier.textPrimitives.variable, 'state.history[0].variables.$1')('[ \u0009]eq[ \u0009]', ' == ')('[ \u0009]neq[ \u0009]', ' != ')('[ \u0009]gt[ \u0009]', ' > ')('[ \u0009]gte[ \u0009]', ' >= ')('[ \u0009]lt[ \u0009]', ' < ')('[ \u0009]lte[ \u0009]', ' <= ')('[ \u0009]and[ \u0009]', ' && ')('[ \u0009]or[ \u0009]', ' || ')('[ \u0009]not[ \u0009]', ' ! ')('[ \u0009]is exactly[ \u0009]', ' === ')('[ \u0009]is[ \u0009]', ' == ')('[ \u0009]to[ \u0009]', ' = ');
      }
      return b;
    }

  };

  Wikifier.textPrimitives = {
    upperLetter: '[A-ZÀ-ÞŐŰ]',
    lowerLetter: '[a-zß-ÿ_0-9\\-őű]',
    anyLetter: '[A-Za-zÀ-Þß-ÿ_0-9\\-ŐŰőű]'
  };

  Wikifier.textPrimitives.variable = '\\$((?:' + Wikifier.textPrimitives.anyLetter.replace('\\-', '') + '*' + Wikifier.textPrimitives.anyLetter.replace('0-9\\-', '') + '+' + Wikifier.textPrimitives.anyLetter.replace('\\-', '') + '*)+)';

  Wikifier.textPrimitives.unquoted = '(?=(?:[^"\'\\\\]*(?:\\\\.|\'(?:[^\'\\\\]*\\\\.)*[^\'\\\\]*\'|"(?:[^"\\\\]*\\\\.)*[^"\\\\]*"))*[^\'"]*$)';

  return Wikifier;

}).call(this);

Wikifier.formatHelpers = {
  charFormatHelper: function(a) {
    var b;
    b = insertElement(a.output, this.element);
    a.subWikify(b, this.terminator);
  },
  inlineCssHelper: function(w) {
    var gotMatch, hadStyle, lookahead, lookaheadMatch, lookaheadRegExp, s, styles, unDash, v;
    s = void 0;
    v = void 0;
    lookaheadMatch = void 0;
    gotMatch = void 0;
    styles = [];
    lookahead = Wikifier.styleByCharFormatter.lookahead;
    lookaheadRegExp = new RegExp(lookahead, 'mg');
    hadStyle = false;
    unDash = function(str) {
      var s;
      var t;
      s = str.split('-');
      if (s.length > 1) {
        t = 1;
        while (t < s.length) {
          s[t] = s[t].substr(0, 1).toUpperCase() + s[t].substr(1);
          t++;
        }
      }
      return s.join('');
    };
    styles.className = '';
    while (true) {
      lookaheadRegExp.lastIndex = w.nextMatch;
      lookaheadMatch = lookaheadRegExp.exec(w.source);
      gotMatch = lookaheadMatch && lookaheadMatch.index === w.nextMatch;
      if (gotMatch) {
        hadStyle = true;
        if (lookaheadMatch[5]) {
          styles.className += lookaheadMatch[5].replace(/\./g, ' ') + ' ';
        } else if (lookaheadMatch[1]) {
          s = unDash(lookaheadMatch[1]);
          v = lookaheadMatch[2];
        } else {
          s = unDash(lookaheadMatch[3]);
          v = lookaheadMatch[4];
        }
        switch (s) {
          case 'bgcolor':
            s = 'backgroundColor';
            break;
          case 'float':
            s = 'cssFloat';
        }
        styles.push({
          style: s,
          value: v
        });
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
      }
      if (!gotMatch) {
        break;
      }
    }
    return styles;
  },
  monospacedByLineHelper: function(w) {
    var lookaheadMatch, lookaheadRegExp;
    lookaheadRegExp = new RegExp(this.lookahead, 'mg');
    lookaheadRegExp.lastIndex = w.matchStart;
    lookaheadMatch = lookaheadRegExp.exec(w.source);
    if (lookaheadMatch && lookaheadMatch.index === w.matchStart) {
      insertElement(w.output, 'pre', null, null, lookaheadMatch[1]);
      w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
    }
  }
};

Wikifier.formatters = [
  {
    name: 'table',
    match: '^\\|(?:[^\\n]*)\\|(?:[fhc]?)$',
    lookahead: '^\\|([^\\n]*)\\|([fhc]?)$',
    rowTerminator: '\\|(?:[fhc]?)$\\n?',
    cellPattern: '(?:\\|([^\\n\\|]*)\\|)|(\\|[fhc]?$\\n?)',
    cellTerminator: '(?:\\x20*)\\|',
    rowTypes: {
      'c': 'caption',
      'h': 'thead',
      '': 'tbody',
      'f': 'tfoot'
    },
    handler: function(w) {
      var currRowType,
  lookaheadMatch,
  lookaheadRegExp,
  matched,
  nextRowType,
  prevColumns,
  rowContainer,
  rowCount,
  rowElement,
  table;
      rowContainer = void 0;
      rowElement = void 0;
      lookaheadMatch = void 0;
      matched = void 0;
      table = insertElement(w.output,
  'table');
      lookaheadRegExp = new RegExp(this.lookahead,
  'mg');
      currRowType = null;
      nextRowType = void 0;
      prevColumns = [];
      rowCount = 0;
      w.nextMatch = w.matchStart;
      while (true) {
        lookaheadRegExp.lastIndex = w.nextMatch;
        lookaheadMatch = lookaheadRegExp.exec(w.source);
        matched = lookaheadMatch && lookaheadMatch.index === w.nextMatch;
        if (matched) {
          nextRowType = lookaheadMatch[2];
          if (nextRowType !== currRowType) {
            rowContainer = insertElement(table,
  this.rowTypes[nextRowType]);
          }
          currRowType = nextRowType;
          if (currRowType === 'c') {
            if (rowCount === 0) {
              rowContainer.setAttribute('align',
  'top');
            } else {
              rowContainer.setAttribute('align',
  'bottom');
            }
            w.nextMatch = w.nextMatch + 1;
            w.subWikify(rowContainer,
  this.rowTerminator);
          } else {
            rowElement = insertElement(rowContainer,
  'tr');
            this.rowHandler(w,
  rowElement,
  prevColumns);
          }
          rowCount++;
        }
        if (!matched) {
          break;
        }
      }
    },
    rowHandler: function(w,
  e,
  prevColumns) {
      var cell,
  cellMatch,
  cellRegExp,
  col,
  currColCount,
  last,
  lastColCount,
  lastColElement,
  matched,
  spaceLeft,
  spaceRight,
  styles,
  t;
      cellMatch = void 0;
      matched = void 0;
      col = 0;
      currColCount = 1;
      cellRegExp = new RegExp(this.cellPattern,
  'mg');
      while (true) {
        cellRegExp.lastIndex = w.nextMatch;
        cellMatch = cellRegExp.exec(w.source);
        matched = cellMatch && cellMatch.index === w.nextMatch;
        if (matched) {
          if (cellMatch[1] === '~') {
            last = prevColumns[col];
            if (last) {
              last.rowCount++;
              last.element.setAttribute('rowSpan',
  last.rowCount);
              last.element.setAttribute('rowspan',
  last.rowCount);
              last.element.valign = 'center';
            }
            w.nextMatch = cellMatch.index + cellMatch[0].length - 1;
          } else if (cellMatch[1] === '>') {
            currColCount++;
            w.nextMatch = cellMatch.index + cellMatch[0].length - 1;
          } else if (cellMatch[2]) {
            w.nextMatch = cellMatch.index + cellMatch[0].length;
            break;
          } else {
            spaceLeft = false;
            spaceRight = false;
            lastColCount = void 0;
            lastColElement = void 0;
            styles = void 0;
            cell = void 0;
            t = void 0;
            w.nextMatch++;
            styles = Wikifier.formatHelpers.inlineCssHelper(w);
            while (w.source.substr(w.nextMatch,
  1) === ' ') {
              spaceLeft = true;
              w.nextMatch++;
            }
            if (w.source.substr(w.nextMatch,
  1) === '!') {
              cell = insertElement(e,
  'th');
              w.nextMatch++;
            } else {
              cell = insertElement(e,
  'td');
            }
            prevColumns[col] = {
              rowCount: 1,
              element: cell
            };
            lastColCount = 1;
            lastColElement = cell;
            if (currColCount > 1) {
              cell.setAttribute('colSpan',
  currColCount);
              cell.setAttribute('colspan',
  currColCount);
              currColCount = 1;
            }
            t = 0;
            while (t < styles.length) {
              cell.style[styles[t].style] = styles[t].value;
              t++;
            }
            w.subWikify(cell,
  this.cellTerminator);
            if (w.matchText.substr(w.matchText.length - 2,
  1) === ' ') {
              spaceRight = true;
            }
            if (spaceLeft && spaceRight) {
              cell.align = 'center';
            } else if (spaceLeft) {
              cell.align = 'right';
            } else if (spaceRight) {
              cell.align = 'left';
            }
            w.nextMatch = w.nextMatch - 1;
          }
          col++;
        }
        if (!matched) {
          break;
        }
      }
    }
  },
  {
    name: 'rule',
    match: '^----$\\n?',
    handler: function(w) {
      insertElement(w.output,
  'hr');
    }
  },
  {
    name: 'emdash',
    match: '--',
    becomes: String.fromCharCode(8212),
    handler: function(a) {
      insertElement(a.output,
  'span',
  null,
  'char',
  this.becomes).setAttribute('data-char',
  'emdash');
    }
  },
  {
    name: 'heading',
    match: '^!{1,5}',
    terminator: '\\n',
    handler: function(w) {
      var e;
      e = insertElement(w.output,
  'h' + w.matchLength);
      w.subWikify(e,
  this.terminator);
    }
  },
  {
    name: 'monospacedByLine',
    match: '^\\{\\{\\{\\n',
    lookahead: '^\\{\\{\\{\\n((?:^[^\\n]*\\n)+?)(^\\}\\}\\}$\\n?)',
    handler: Wikifier.formatHelpers.monospacedByLineHelper
  },
  {
    name: 'quoteByBlock',
    match: '^<<<\\n',
    terminator: '^<<<\\n',
    handler: function(w) {
      var e;
      e = insertElement(w.output,
  'blockquote');
      w.subWikify(e,
  this.terminator);
    }
  },
  {
    name: 'list',
    match: '^(?:(?:\\*+)|(?:#+))',
    lookahead: '^(?:(\\*+)|(#+))',
    terminator: '\\n',
    handler: function(w) {
      var bulletType,
  currLevel,
  currType,
  len,
  lookaheadMatch,
  lookaheadRegExp,
  matched,
  newLevel,
  newType,
  placeStack,
  t;
      newType = void 0;
      newLevel = void 0;
      t = void 0;
      len = void 0;
      bulletType = void 0;
      lookaheadMatch = void 0;
      matched = void 0;
      lookaheadRegExp = new RegExp(this.lookahead,
  'mg');
      placeStack = [w.output];
      currType = null;
      currLevel = 0;
      w.nextMatch = w.matchStart;
      while (true) {
        lookaheadRegExp.lastIndex = w.nextMatch;
        lookaheadMatch = lookaheadRegExp.exec(w.source);
        matched = lookaheadMatch && lookaheadMatch.index === w.nextMatch;
        if (matched) {
          newLevel = lookaheadMatch[0].length;
          if (lookaheadMatch[1]) {
            bulletType = lookaheadMatch[1].slice(-1);
            newType = 'ul';
          } else if (lookaheadMatch[2]) {
            newType = 'ol';
          }
          w.nextMatch += newLevel;
          if (newLevel > currLevel) {
            t = currLevel;
            while (t < newLevel) {
              placeStack.push(insertElement(placeStack[placeStack.length - 1],
  newType));
              t++;
            }
          } else if (newLevel < currLevel) {
            t = currLevel;
            while (t > newLevel) {
              placeStack.pop();
              t--;
            }
          } else if (newLevel === currLevel && newType !== currType) {
            placeStack.pop();
            placeStack.push(insertElement(placeStack[placeStack.length - 1],
  newType));
          }
          currLevel = newLevel;
          currType = newType;
          t = insertElement(placeStack[placeStack.length - 1],
  'li');
          // Currently unused
          if (bulletType && bulletType !== '*') {
            t.setAttribute('data-bullet',
  bulletType);
          }
          w.subWikify(t,
  this.terminator);
        }
        if (!matched) {
          break;
        }
      }
    }
  },
  Wikifier.urlFormatter = {
    name: 'urlLink',
    match: '(?:https?|mailto|javascript|ftp|data):[^\\s\'"]+(?:/|\\b)',
    handler: function(w) {
      var e;
      e = Wikifier.createExternalLink(w.output,
  w.matchText);
      w.outputText(e,
  w.matchStart,
  w.nextMatch);
    }
  },
  Wikifier.linkFormatter = {
    name: 'prettyLink',
    match: '\\[\\[',
    lookahead: '\\[\\[([^\\|]*?)(?:\\|(.*?))?\\](?:\\[(.*?)])?\\]',
    makeInternalOrExternal: function(out,
  title,
  callback,
  type) {
      if (title && !tale.has(title) && (title.match(Wikifier.urlFormatter.match,
  'g') || ~title.search(/[\.\\\/#]/))) {
        return Wikifier.createExternalLink(out,
  title,
  callback,
  type);
      } else {
        return Wikifier.createInternalLink(out,
  title,
  callback,
  type);
      }
    },
    makeCallback: function(code,
  callback,
  fullmatch) {
      return function() {
        // Codegen debugging - VG
        if (DEBUG_CODEGEN) {
          console.log('Wikifier.linkFormatter.makeCallback(): Full Match',
  fullmatch,
  code,
  Wikifier.parse(code));
        }
        macros.set.run(null,
  Wikifier.parse(code),
  null,
  code);
        typeof callback === 'function' && callback.call(this);
      };
    },
    makeLink: function(out,
  match,
  callback2,
  type) {
      var callback,
  link,
  title;
      link = void 0;
      title = void 0;
      callback = void 0;
      // Codegen debugging - VG
      if (DEBUG_CODEGEN) {
        console.log('Wikifier.linkFormatter.makeLink():',
  match);
      }
      if (match[3]) {
        // Code
        callback = this.makeCallback(match[3],
  callback2,
  match);
      } else {
        typeof callback2 === 'function' && (callback = callback2);
      }
      title = Wikifier.parsePassageTitle(match[2] || match[1]);
      link = this.makeInternalOrExternal(out,
  title,
  callback,
  type);
      setPageElement(link,
  null,
  match[2] ? match[1] : title);
      return link;
    },
    handler: function(w) {
      var lookaheadMatch,
  lookaheadRegExp;
      lookaheadRegExp = new RegExp(this.lookahead,
  'mg');
      lookaheadRegExp.lastIndex = w.matchStart;
      lookaheadMatch = lookaheadRegExp.exec(w.source);
      if (lookaheadMatch && lookaheadMatch.index === w.matchStart) {
        this.makeLink(w.output,
  lookaheadMatch);
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
      }
    }
  },
  Wikifier.imageFormatter = {
    name: 'image',
    match: '\\[(?:[<]{0,1})(?:[>]{0,1})[Ii][Mm][Gg]\\[',
    lookahead: '\\[([<]?)(>?)img\\[(?:([^\\|\\]]+)\\|)?([^\\[\\]\\|]+)\\](?:\\[([^\\]]*)\\]?)?(\\])',
    importedImage: function(img,
  passageName) {
      var e,
  imgPassages,
  imgname,
  j;
      imgPassages = void 0;
      imgname = void 0;
      try {
        // Try to parse it as a variable
        imgname = internalEval(Wikifier.parse(passageName));
      } catch (error) {
        e = error;
      }
      if (!imgname) {
        imgname = passageName;
      }
      // Base64 passage transclusion
      imgPassages = tale.lookup('tags',
  'Twine.image');
      j = 0;
      while (j < imgPassages.length) {
        if (imgPassages[j].title === imgname) {
          img.src = imgPassages[j].text;
          return;
        }
        j++;
      }
      img.src = img.src || imgname;
    },
    handler: function(w) {
      var e,
  img,
  j,
  lookaheadMatch,
  lookaheadRegExp,
  title;
      e = void 0;
      img = void 0;
      j = void 0;
      lookaheadMatch = void 0;
      lookaheadRegExp = new RegExp(this.lookahead,
  'mig');
      lookaheadRegExp.lastIndex = w.matchStart;
      lookaheadMatch = lookaheadRegExp.exec(w.source);
      if (lookaheadMatch && lookaheadMatch.index === w.matchStart) {
        e = w.output;
        title = Wikifier.parsePassageTitle(lookaheadMatch[5]);
        if (title) {
          e = Wikifier.linkFormatter.makeInternalOrExternal(w.output,
  title);
        }
        img = insertElement(e,
  'img');
        if (lookaheadMatch[1]) {
          img.align = 'left';
        } else if (lookaheadMatch[2]) {
          img.align = 'right';
        }
        if (lookaheadMatch[3]) {
          img.title = lookaheadMatch[3];
        }
        // Setup the image if it's referencing an image passage.
        this.importedImage(img,
  lookaheadMatch[4]);
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
      }
    }
  },
  {
    name: 'macro',
    match: '<<',
    lookahead: /<<([^>\s]+)(?:\s*)((?:\\.|'(?:[^'\\]*\\.)*[^'\\]*'|"(?:[^"\\]*\\.)*[^"\\]*"|[^'"\\>]|>(?!>))*)>>/mg,
    handler: function(w) {
      var e,
  lookaheadMatch,
  lookaheadRegExp,
  macro,
  name,
  params;
      lookaheadRegExp = new RegExp(this.lookahead);
      lookaheadRegExp.lastIndex = w.matchStart;
      lookaheadMatch = lookaheadRegExp.exec(w.source.replace(/\u200c/g,
  '\n'));
      if (lookaheadMatch && lookaheadMatch.index === w.matchStart && lookaheadMatch[1]) {
        params = lookaheadMatch[2].readMacroParams();
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
        name = lookaheadMatch[1];
        try {
          macro = macros[name];
          if (macro && typeof macro === 'object' && macro.handler) {
            macro.handler(w.output,
  name,
  params,
  w);
          } else if (name[0] === '$') {
            macros.print.handler(w.output,
  name,
  [name].concat(params),
  w);
          } else if (tale.has(name)) {
            macros.display.handler(w.output,
  name,
  [name].concat(params),
  w);
          } else {
            throwError(w.output,
  'No macro or passage called "' + name + '"',
  w.fullMatch());
          }
        } catch (error) {
          e = error;
          throwError(w.output,
  'Error executing macro ' + name + ': ' + e.toString(),
  w.fullMatch());
        }
      }
    }
  },
  {
    name: 'html',
    match: '<html>',
    lookahead: '<html>((?:.|\\n)*?)</html>',
    handler: function(w) {
      var e,
  lookaheadMatch,
  lookaheadRegExp;
      lookaheadRegExp = new RegExp(this.lookahead,
  'mg');
      lookaheadRegExp.lastIndex = w.matchStart;
      lookaheadMatch = lookaheadRegExp.exec(w.source);
      if (lookaheadMatch && lookaheadMatch.index === w.matchStart) {
        e = insertElement(w.output,
  'span');
        e.innerHTML = lookaheadMatch[1];
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
      }
    }
  },
  {
    name: 'commentByBlock',
    match: '/%',
    lookahead: '/%((?:.|\\n)*?)%/',
    handler: function(w) {
      var lookaheadMatch,
  lookaheadRegExp;
      lookaheadRegExp = new RegExp(this.lookahead,
  'mg');
      lookaheadRegExp.lastIndex = w.matchStart;
      lookaheadMatch = lookaheadRegExp.exec(w.source);
      if (lookaheadMatch && lookaheadMatch.index === w.matchStart) {
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
      }
    }
  },
  {
    name: 'boldByChar',
    match: '\'\'',
    terminator: '\'\'',
    element: 'strong',
    handler: Wikifier.formatHelpers.charFormatHelper
  },
  {
    name: 'strikeByChar',
    match: '==',
    terminator: '==',
    element: 'strike',
    handler: Wikifier.formatHelpers.charFormatHelper
  },
  {
    name: 'underlineByChar',
    match: '__',
    terminator: '__',
    element: 'u',
    handler: Wikifier.formatHelpers.charFormatHelper
  },
  {
    name: 'italicByChar',
    match: '//',
    terminator: '//',
    element: 'em',
    handler: Wikifier.formatHelpers.charFormatHelper
  },
  {
    name: 'subscriptByChar',
    match: '~~',
    terminator: '~~',
    element: 'sub',
    handler: Wikifier.formatHelpers.charFormatHelper
  },
  {
    name: 'superscriptByChar',
    match: '\\^\\^',
    terminator: '\\^\\^',
    element: 'sup',
    handler: Wikifier.formatHelpers.charFormatHelper
  },
  {
    name: 'monospacedByChar',
    match: '\\{\\{\\{',
    lookahead: '\\{\\{\\{((?:.|\\n)*?)\\}\\}\\}',
    handler: function(w) {
      var e,
  lookaheadMatch,
  lookaheadRegExp;
      lookaheadRegExp = new RegExp(this.lookahead,
  'mg');
      lookaheadRegExp.lastIndex = w.matchStart;
      lookaheadMatch = lookaheadRegExp.exec(w.source);
      if (lookaheadMatch && lookaheadMatch.index === w.matchStart) {
        e = insertElement(w.output,
  'code',
  null,
  null,
  lookaheadMatch[1]);
        w.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
      }
    }
  },
  Wikifier.styleByCharFormatter = {
    name: 'styleByChar',
    match: '@@',
    terminator: '@@',
    lookahead: '(?:([^\\(@]+)\\(([^\\)\\|\\n]+)(?:\\):))|(?:([^\\.:@]+):([^;\\|\\n]+);)|(?:\\.([^;\\|\\n]+);)',
    handler: function(w) {
      var e,
  styles,
  t;
      e = insertElement(w.output,
  'span',
  null,
  null,
  null);
      styles = Wikifier.formatHelpers.inlineCssHelper(w);
      if (styles.length === 0) {
        e.className = 'marked';
      } else {
        t = 0;
        while (t < styles.length) {
          e.style[styles[t].style] = styles[t].value;
          t++;
        }
        if (typeof styles.className === 'string') {
          e.className = styles.className;
        }
      }
      w.subWikify(e,
  this.terminator);
    }
  },
  {
    name: 'lineBreak',
    match: '\\n',
    handler: function(w) {
      insertElement(w.output,
  'br');
    }
  },
  {
    name: 'continuedLine',
    match: '\\\\\\s*?\\n',
    handler: function(a) {
      a.nextMatch = a.matchStart + 2;
    }
  },
  {
    name: 'htmlCharacterReference',
    match: '(?:(?:&#?[a-zA-Z0-9]{2,8};|.)(?:&#?(?:x0*(?:3[0-6][0-9a-fA-F]|1D[c-fC-F][0-9a-fA-F]|20[d-fD-F][0-9a-fA-F]|FE2[0-9a-fA-F])|0*(?:76[89]|7[7-9][0-9]|8[0-7][0-9]|761[6-9]|76[2-7][0-9]|84[0-3][0-9]|844[0-7]|6505[6-9]|6506[0-9]|6507[0-1]));)+|&#?[a-zA-Z0-9]{2,8};)',
    handler: function(w) {
      var el;
      el = document.createElement('div');
      el.innerHTML = w.matchText;
      insertText(w.output,
  el.textContent);
    }
  },
  {
    name: 'htmltag',
    match: '<(?:\\/?[\\w\\-]+|[\\w\\-]+(?:(?:\\s+[\\w\\-]+(?:\\s*=\\s*(?:\".*?\"|\'.*?\'|[^\'\">\\s]+))?)+\\s*|\\s*)\\/?)>',
    tagname: '<(\\w+)',
    voids: ['area',
  'base',
  'br',
  'col',
  'embed',
  'hr',
  'img',
  'input',
  'link',
  'meta',
  'param',
  'source',
  'track',
  'wbr'],
    tableElems: ['table',
  'thead',
  'tbody',
  'tfoot',
  'th',
  'tr',
  'td',
  'colgroup',
  'col',
  'caption',
  'figcaption'],
    cleanupTables: function(e) {
      var elems,
  i,
  name;
      i = void 0;
      name = void 0;
      elems = [].slice.call(e.children);
      // Remove non-table child elements that aren't children of <td>s
      i = 0;
      while (i < elems.length) {
        if (elems[i].tagName) {
          name = elems[i].tagName.toLowerCase();
          if (this.tableElems.indexOf(name) === -1) {
            elems[i].outerHTML = '';
          } else if (['col',
  'caption',
  'figcaption',
  'td',
  'th'].indexOf(name) === -1) {
            this.cleanupTables.call(this,
  elems[i]);
          }
        }
        i++;
      }
    },
    handler: function(a) {
      var e,
  isstyle,
  isvoid,
  lookahead,
  lookaheadMatch,
  lookaheadRegExp,
  passage,
  re,
  setter,
  tmp,
  tn;
      tmp = void 0;
      passage = void 0;
      setter = void 0;
      e = void 0;
      isvoid = void 0;
      isstyle = void 0;
      lookaheadRegExp = void 0;
      lookaheadMatch = void 0;
      lookahead = void 0;
      re = new RegExp(this.tagname).exec(a.matchText);
      tn = re && re[1] && re[1].toLowerCase();
      if (tn && tn !== 'html') {
        lookahead = '<\\/\\s*' + tn + '\\s*>';
        isvoid = this.voids.indexOf(tn) !== -1;
        isstyle = tn === 'style' || tn === 'script';
        lookaheadRegExp = new RegExp(lookahead,
  'mg');
        lookaheadRegExp.lastIndex = a.matchStart;
        lookaheadMatch = lookaheadRegExp.exec(a.source);
        if (lookaheadMatch || isvoid) {
          if (isstyle) {
            e = document.createElement(tn);
            e.type = 'text/css';
            // IE8 compatibility
            tmp = a.source.slice(a.nextMatch,
  lookaheadMatch.index);
            if (e.styleSheet) {
              (e.styleSheet.cssText = tmp);
            } else {
              (e.innerHTML = tmp);
            }
            a.nextMatch = lookaheadMatch.index + lookaheadMatch[0].length;
          } else {
            // Creating a loose <tr> element creates it wrapped inside a <tbody> element.
            e = document.createElement(a.output.tagName);
            e.innerHTML = a.matchText;
            while (e.firstChild) {
              e = e.firstChild;
            }
            if (!isvoid) {
              a.subWikify(e,
  lookahead);
            }
          }
          if (!e || !e.tagName) {
            return;
          }
          if (e.tagName.toLowerCase() === 'table') {
            this.cleanupTables.call(this,
  e);
          }
          // Special data-setter attribute
          if (setter = e.getAttribute('data-setter')) {
            setter = Wikifier.linkFormatter.makeCallback(setter);
          }
          // Special data-passage attribute
          if (passage = e.getAttribute('data-passage')) {
            if (tn !== 'img') {
              addClickHandler(e,
  Wikifier.linkFunction(Wikifier.parsePassageTitle(passage),
  e,
  setter));
              if (tn === 'area' || tn === 'a') {
                e.setAttribute('href',
  'javascript:;');
              }
            } else {
              Wikifier.imageFormatter.importedImage(e,
  passage);
            }
          }
          a.output.appendChild(e);
        } else {
          throwError(a.output,
  'HTML tag \'' + tn + '\' wasn\'t closed.',
  a.matchText);
        }
      }
    }
  }
];

// Optional - included if the ".char" selector appears anywhere in the story.
Wikifier.charSpanFormatter = {
  name: 'char',
  match: '[^\n]',
  handler: function(a) {
    // Line breaks do NOT get their own charspans
    insertElement(a.output, 'span', null, 'char', a.matchText).setAttribute('data-char', a.matchText === ' ' ? 'space' : a.matchText === '\u0009' ? 'tab' : a.matchText);
  }
};

Wikifier.parsePassageTitle = function(title) {
  var e;
  if (title && !tale.has(title)) {
    try {
      title = (internalEval(this.parse(title)) || title) + '';
    } catch (error) {
      e = error;
    }
  }
  return title;
};

Wikifier.linkFunction = function(title, el, callback) {
  return function() {    //console.log("Wikifier.linkFunction('"+title+"', ...)");
    var passage;
    if (state.rewindTo) {
      passage = findPassageParent(el);
      if (passage && passage.parentNode.lastChild !== passage) {
        state.rewindTo(passage, true);
      }
    }
    if (title) {
      state.display(title, el, null, callback);
    } else if (typeof callback === 'function') {
      callback();
    }
  };
};

Wikifier.createInternalLink = function(place, title, callback, type) {
  var el, suffix, tag;
  tag = type === 'button' ? 'button' : 'a';
  suffix = type === 'button' ? 'Button' : 'Link';
  el = insertElement(place, tag);
  el.tabIndex = 0;
  if (tale.has(title)) {
    el.className = 'internal' + suffix;
  } else {
    //if visited(title)
    //  el.className += ' visited' + suffix
    el.className = 'broken' + suffix;
  }
  addClickHandler(el, Wikifier.linkFunction(title, el, callback));
  if (place) {
    place.appendChild(el);
  }
  return el;
};

Wikifier.createExternalLink = function(place, url, callback, type) {
  var el, tag;
  tag = type === 'button' ? 'button' : 'a';
  el = insertElement(place, tag);
  el.href = url;
  el.tabIndex = 0;
  el.className = 'external' + (type === 'button' ? 'Button' : 'Link');
  el.target = '_blank';
  if (typeof callback === 'function') {
    addClickHandler(el, callback);
  }
  if (place) {
    place.appendChild(el);
  }
  return el;
};

// ---
// generated by js2coffee 2.2.0

// Make publically accessible.
window.Wikifier = Wikifier;

NylonHistory = (function() {
  // @requires: engine/_funcsAndPolyfills.coffee
  /*
   * History object
   *
   * Modified to be a bit more configurable.
   *
   * CONFIG:
   *  max-length: 5
   */
  class NylonHistory {
    constructor(Config) {
      this.Config = Config;
      this.history = [
        {
          passage: null,
          variables: {}
        }
      ];
      // Unique identifier for this game session
      this.id = (new Date()).getTime() + '';
      // URL of the bookmark link
      this.hash = '';
      // Nylon - Max length of @history
      this.MaxLength = this.Config['max-length'] || 5;
    }

    //@property 'title', ->
    //    @history[0].title
    /*
    Fuck this.
    @encodeHistory: (hIndex, noVars) ->
        ret = '.'
        vars = undefined
        type = undefined
        hist = state.history[hIndex]
        variables = if state.history[hIndex + 1] then delta(state.history[hIndex + 1].variables, hist.variables) else hist.variables

        vtob = (val) ->
            try
                return window.btoa(unescape(encodeURIComponent(JSON.stringify(decompile(val)))))
            catch e
                return '0'
            return
        if !hist.passage or hist.passage.id == null
            return ''
        ret += hist.passage.id.toString(36)
        if noVars
            return ret
        for vars of variables
            type = typeof d[vars]
            if type != 'undefined'
                ret += '$' + vtob(vars) + ',' + vtob(d[vars])
        for vars of hist.linkVars
            type = typeof hist.linkVars[vars]
            if type != 'function' and type != 'undefined'
                ret += '[' + vtob(vars) + ',' + vtob(hist.linkVars[vars])
        return ret

    @decodeHistory: (str, prev, done_callback) ->
        name = undefined
        splits = undefined
        variable = undefined
        c = undefined
        d = undefined
        ret = variables: clone(prev.variables) or {}
        match = /([a-z0-9]+)((?:\$[A-Za-z0-9\+\/=]+,[A-Za-z0-9\+\/=]+)*)((?:\[[A-Za-z0-9\+\/=]+,[A-Za-z0-9\+\/=]+)*)/g.exec(str)

        btov = (str) ->
            try
                return recompile(JSON.parse(decodeURIComponent(escape(window.atob(str)))))
            catch e
                return 0
            return

        if match
            name = parseInt(match[1], 36)
            if !tale.has(name)
                return false
            if match[2]
                ret.variables or (ret.variables = {})
                splits = match[2].split('$')
                c = 0
                while c < splits.length
                    variable = splits[c].split(',')
                    d = btov(variable[0])
                    if d
                        ret.variables[d] = btov(variable[1])
                    c++
            if match[3]
                ret.linkVars or (ret.linkVars = {})
                splits = match[3].split('[')
                c = 0
                while c < splits.length
                    variable = splits[c].split(',')
                    d = btov(variable[0])
                    if d
                        ret.linkVars[d] = btov(variable[1])
                    c++
            tale.get name, (page) =>
                ret.passage = page
                done_callback(ret)
                return
            return
        return
    */
    save() {
      var a, b, hist;
      hist = void 0;
      b = void 0;
      a = '';
      b = this.history.length - 1;
      while (b >= 0) {
        hist = this.history[b];
        if (!hist) {
          break;
        }
        //a += NylonHistory.encodeHistory(b)
        b--;
      }
      return '#' + a;
    }

    restore() {
      var a, b, c, d, vars;
      a = void 0;
      b = void 0;
      c = void 0;
      vars = void 0;
      try {
        if (!window.location.hash || window.location.hash === '#') {
          if (testplay && testplay !== 'Start') {
            if (tale.has('StoryInit')) {
              new Wikifier(insertElement(null, 'span'), tale.get('StoryInit').text);
            }
            this.display(testplay, null, 'quietly');
            return true;
          }
          return false;
        }
        if (window.location.hash.substr(0, 2) === '#!') {
          c = window.location.hash.substr(2).split('_').join(' ');
          this.display(c, null, 'quietly');
          return true;
        }
        a = window.location.hash.replace('#', '').split('.');
        b = 0;
        while (b < a.length) {
          vars = NylonHistory.decodeHistory(a[b], vars || {});
          if (vars) {
            if (b === a.length - 1) {
              vars.variables = clone(this.history[0].variables);
              for (c in this.history[0].linkVars) {
                vars.variables[c] = clone(this.history[0].linkVars[c]);
              }
              this.history.unshift(vars);
              this.display(vars.passage.title, null, 'back');
            } else {
              this.history.unshift(vars);
            }
          }
          b++;
        }
        return true;
      } catch (error) {
        d = error;
        return false;
      }
    }

    saveVariables(c, el, callback) {
      if (typeof callback === 'function') {
        callback.call(el);
      }
      this.history.push({
        passage: c,
        variables: clone(this.history[0].variables)
      });
      // Nylon: Snip off excessive frames.
      while (this.history.length > Math.max(1, this.MaxLength)) {
        this.history.pop();
      }
    }

    restart() {
      if (typeof window.history.replaceState === 'function') {
        typeof this.pushState === 'function' && this.pushState(true, window.location.href.replace(/#.*$/, ''));
        window.location.reload();
      } else {
        window.location.hash = '';
      }
    }

  };

  NylonHistory.getter('title', function() {
    return this.history[0].title;
  });

  return NylonHistory;

}).call(this);

NylonEmbeddedLoader = (function() {
  // ---
  // generated by js2coffee 2.2.0
  /*
   * Basically the same thing as the old Twine 1.4.2 passage loader, except lazy-loading.
   *
   * This, as in the old loader does, just grabs stuff out of HTML elements injected into the bottom of the document.
   * config:
   *  load-lazily: yes/no
   */
  class NylonEmbeddedLoader extends NylonLazyLoader {
    constructor(story, config) {
      super(story, config);
      this.PreloadData = {};
    }

    load() {
      var child, childname, index, len1, ref, results, x;
      ref = document.getElementById("storeArea").children;
      results = [];
      for (index = x = 0, len1 = ref.length; x < len1; index = ++x) {
        child = ref[index];
        childname = child.getAttribute && child.getAttribute("tiddler");
        switch (childname) {
          case 'StorySettings':
            continue;
        }
        results.push(this.preloadPassage(childname, index, child, null));
      }
      return results;
    }

    loadPassage(passageID, cb) {
      var c, childname, pd, tags, text;
      pd = this.PreloadData[passageID];
      c = pd.data;
      childname = pd.name;
      tags = c.getAttribute && c.getAttribute("tags");
      text = c.firstChild ? c.firstChild.nodeValue : "";
      this.Passage[passageID] = new NylonPassage(passageID, pd.index, tags.split(' '), text, this.Story.Deobfuscator, null); // Too old for config objects.
      return cb && cb(this.Passage[passageID]);
    }

  };

  NylonEmbeddedLoader.ID = 'embedded';

  return NylonEmbeddedLoader;

}).call(this);

NylonPassageLoader.Register(NylonEmbeddedLoader);

version.extensions.actionsMacro = {
  major: 1,
  minor: 2,
  revision: 0
};

macros.actions = {
  handler: function(a, f, g) {
    var b, c, d, e, v;
    v = state.history[0].variables;
    e = insertElement(a, 'ul');
    if (!v['actions clicked']) {
      v['actions clicked'] = {};
    }
    b = 0;
    while (b < g.length) {
      if (v['actions clicked'][g[b]]) {
        b++;
        continue;
      }
      d = insertElement(e, 'li');
      c = Wikifier.createInternalLink(d, g[b], (function(link) {
        return function() {
          state.history[0].variables['actions clicked'][link] = true;
        };
      })(g[b]));
      insertText(c, g[b]);
      b++;
    }
  }
};

// ---
// generated by js2coffee 2.2.0
/**
 * Nylon Extension
 */
version.extensions.foreachMacros = {
  major: 1,
  minor: 0,
  revision: 0
};

macros['foreach'] = {
  handler: function(place, macroName, params, parser) {
    var chunks, clauses, conditions, currentClause, currentCond, data, e, endPos, expr, firstletter, i, inExpr, lastIndex, loopvar, nesting, rawCond, rawConds, src, srcOffset, t;
    conditions = [];
    clauses = [];
    rawConds = [];
    inExpr = '';
    srcOffset = parser.source.indexOf('>>', parser.matchStart) + 2;
    src = parser.source.slice(srcOffset);
    endPos = -1;
    rawCond = params.join(' ');
    currentCond = parser.fullArgs();
    currentClause = '';
    t = 0;
    nesting = 0;
    i = 0;
    while (i < src.length) {
      if (src.substr(i, 10) === '<<foreach ') {
        nesting++;
      }
      if (src.substr(i, 14) === '<<endforeach>>') {
        nesting--;
        if (nesting < 0) {
          endPos = srcOffset + i + 14;
          rawConds.push(rawCond);
          conditions.push(currentCond.trim());
          clauses.push(currentClause);
          break;
        }
      }
      currentClause += src.charAt(i);
      i++;
    }
    if (endPos !== -1) {
      parser.nextMatch = endPos;
      lastIndex = 0;
      try {
        chunks = [];
        expr = '';
        loopvar = '';
        firstletter = '';
        i = 0;
        while (i < clauses.length) {
          lastIndex = i;
          chunks = rawConds[i].split(' ');
          if (chunks.length >= 3) {
            expr = Wikifier.parse(chunks.slice(2).join(' '));
            loopvar = chunks[0];
            //console.log(expr,loopvar);
            data = internalEval(expr);
            firstletter = loopvar.substr(0, 1);
            if (chunks[1] === 'in') {
              if (!(expr instanceof Array)) {
                data = Object.values(data);
              }
              this._forIn(parser, place, clauses[i], expr, loopvar, data);
            } else if (chunks[1] === 'of') {
              if (!(expr instanceof Array)) {
                data = Object.keys(data);
              }
              this._forIn(parser, place, clauses[i], expr, loopvar, data);
            }
          }
          i++;
        }
      } catch (error) {
        e = error;
        console.log(e);
        /* Nylon Patch for Twine 1.4.2 Sugarcane */
        console.log(conditions[lastIndex]);
        console.log(clauses[lastIndex]);
        console.log(e.stack);
        /* End patch */
        throwError(place, '<<foreach>> bad condition: ' + rawConds[i], parser.fullMatch());
      }
    } else {
      throwError(place, 'I can\'t find a matching <<endforeach>>', parser.fullMatch());
    }
  },
  _forIn: function(parser, place, clause, expr, loopvar, collection) {
    var each, firstletter, i;
    each = null;
    i = 0;
    firstletter = '';
    i = 0;
    while (i < collection.length) {
      window.state.history[0].variables['loop'] = {
        index: i,
        value: collection[i]
      };
      firstletter = loopvar.substr(1);
      if (firstletter === '$') {
        window.state.history[0].variables[firstletter] = collection[i];
      } else {
        internalEval(loopvar + ' = window.state.history[0].variables.loop.value;');
      }
      new Wikifier(place, clause);
      window.twineCurrentEl = place;
      window.twineCurrentParser = parser;
      i++;
    }
  }
};

// END Nylon

// ---
// generated by js2coffee 2.2.0
version.extensions.printMacro = {
  major: 1,
  minor: 1,
  revision: 1
};

macros.print = {
  handler: function(place, macroName, params, parser) {
    var args, e, output;
    args = parser.fullArgs(macroName !== 'print');
    output = void 0;
    /* Nylon Patch for Twine 1.4.2 Sugarcane */
    window.twineCurrentEl = place;
    window.twineCurrentParser = parser;
    try {
      // See comment within macros.display
      /* End patch */
      output = internalEval(args);
      if (output !== null && (typeof output !== 'number' || !isNaN(output))) {
        new Wikifier(place, '' + output);
      }
    } catch (error) {
      e = error;
      nylonErrorHandler(place, parser, e, '<<print>> bad expression: ' + params.join(' '));
      throwError(place, '<<print>> bad expression: ' + params.join(' '), parser.fullMatch());
    }
  }
};

NylonStory = (function() {
  // ---
  // generated by js2coffee 2.2.0
  // @requires: engine/_funcsAndPolyfills.coffee
  // @requires: engine/NylonHistory.coffee
  /*
   * Nylon Core/Main Loop
   */
  class NylonStory {
    constructor(config = null) {
      var k, obfID, plID, ref, v;
      this.canBookmark = this.canBookmark.bind(this);
      this.Config = {};
      this.PassageLoader = null;
      this.Deobfuscator = null;
      this.LoadingProgressCallback = this._OnProgress;
      this.LoadingEndsCallback = this._OnLoadEnds;
      this.Prerender = [];
      this.Postrender = [];
      this.Config = {
        lookup: function(a, b) {
          if (indexOf.call(this, a) < 0) {
            return b;
          }
          return this[a];
        }
      };
      if (config !== null) {
        for (k in config) {
          v = config[k];
          this.Config[k] = v;
        }
      } else if ('nylonSettings' in window && window.nylonSettings !== null) {
        ref = window.nylonSettings;
        for (k in ref) {
          v = ref[k];
          this.Config[k] = v;
        }
      }
      if (!('obfuscation' in this.Config)) {
        this.Config.obfuscation = {
          'id': 'null'
        };
      }
      obfID = this.Config.obfuscation.id;
      this.Deobfuscator = NylonDeobfuscator.CreateByID(this, obfID, this.Config.obfuscation);
      plID = NylonEmbeddedLoader.ID;
      if ('loader' in this.Config && 'id' in this.Config.loader) {
        plID = this.Config.loader.id;
      }
      this.PassageLoader = NylonPassageLoader.CreateByID(this, plID, this.Config.loader || {});
    }

    initHistory() {
      return new NylonHistory(this.Config);
    }

    load() {
      this.loadingscreen = document.createElement('section');
      this.loadingscreen.setAttribute('id', 'nylon-loading-screen');
      document.body.prepend(this.loadingscreen);
      this.loadingicon = document.createElement('img');
      this.loadingicon.setAttribute('id', 'nylon-loading-icon');
      this.loadingicon.setAttribute('src', 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PGNpcmNsZSBjeD0iMzIiIGN5PSIzMiIgcj0iMzIiIGZpbGw9IiM2ZjhhOTEiLz48cGF0aCBkPSJNNTkuODQ2IDE2LjMxTDE1LjkxMiA1OS42MjZhMzIgMzIgMCAwMDIuOCAxLjQ1MWw0Mi41NDYtNDEuOTQzYTMyIDMyIDAgMDAtMS40MTItMi44MjJ6TTQ1LjA3NCAyLjgyMkwyLjczOCA0NC44NTdhMzIgMzIgMCAwMDEuNDEyIDIuODI3bDQzLjc0LTQzLjQzYTMyIDMyIDAgMDAtMi44MTYtMS40MzJ6IiBmaWxsPSIjZmZmIiBmaWxsLW9wYWNpdHk9Ii4zNjUiLz48cGF0aCBkPSJNMTAuNDM4IDM5LjI1OGwxNC42MjUtMTQuNSAxMy42MjUgMTQuNjI1IDE0Ljg3NS0xNC43NSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjMiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIvPjwvc3ZnPg=='); //replaced at compile-time with loadlogo.svg.
      this.loadingscreen.appendChild(this.loadingicon);
      this.loadingbarouter = document.createElement('div');
      this.loadingbarouter.setAttribute('id', 'nylon-loading-bar-outer');
      this.loadingscreen.appendChild(this.loadingbarouter);
      this.loadingbar = document.createElement('div');
      this.loadingbar.setAttribute('id', 'nylon-loading-bar-inner');
      this.loadingbarouter.appendChild(this.loadingbar);
      this.loadingtext = document.createElement('span');
      this.loadingtext.setAttribute('id', 'nylon-loading-text');
      this.loadingbar.appendChild(this.loadingtext);
      this.loadingmessage = document.createElement('span');
      this.loadingmessage.setAttribute('id', 'nylon-loading-message');
      this.loadingscreen.appendChild(this.loadingmessage);
      return this.PassageLoader.load({
        progress: (message, step, steps) => {
          return this.LoadingProgressCallback(message, step, steps);
        },
        end: () => {
          return this.LoadingEndsCallback();
        }
      });
    }

    _OnProgress(message, step, steps) {
      var pct;
      /*
      <section id="loadingscreen">
          <img id="loadingicon" src="blob:application/svg:..." />
          <div id="loadingbar">
              <span id="loadingtext">...</span>
          </div>
          <span id="loadingtext">...</span>
      </section>
      */
      this.loadingscreen.style.display = 'fixed';
      this.loadingmessage.innerText = message;
      pct = (step / steps) * 100.0;
      this.loadingbar.style.width = `${pct}%`;
    }

    _OnLoadEnds() {
      this.loadingbar = null;
      this.loadingbarouter = null;
      this.loadingicon = null;
      this.loadingmessage = null;
      this.loadingtext = null;
      this.loadingscreen.parentNode.removeChild(this.loadingscreen);
      this.loadingscreen = null;
      state.display('Start');
    }

    has(title_or_id) {
      return this.PassageLoader.has(title_or_id);
    }

    get(title_or_id, done_callback) {
      this.PassageLoader.get(title_or_id, done_callback);
    }

    _get(title_or_id) {
      return this.PassageLoader._get(title_or_id);
    }

    lookup(key, value, sortBy = 'title') {
      var i, matches, passage, ref, ref1, title, x;
      matches = [];
      ref = this.PassageLoader.Passages;
      for (title in ref) {
        passage = ref[title];
        for (i = x = 0, ref1 = passage[key].length; (0 <= ref1 ? x <= ref1 : x >= ref1); i = 0 <= ref1 ? ++x : --x) {
          if (passage[key][i] === value) {
            matches.push(passage);
          }
        }
      }
      matches.sort(function(a, b) {
        if (a[sortBy] === b[sortBy]) {
          return 0;
        } else {
          if (a[sortBy] < b[sortBy]) {
            return -1;
          } else {
            return 1;
          }
        }
      });
      return matches;
    }

    canUndo() {
      return this.Config['can-undo'];
    }

    identity() {
      var meta;
      if (!this.identity) {
        meta = document.querySelector('meta[name=\'identity\']');
        this.identity = meta ? meta.getAttribute('content') : 'story';
      }
      return this.identity;
    }

    forEachStylesheet(tags = [], callback = null) {
      var len1, len2, passage, ref, ref1, tag, x, y;
      if (callback === null) {
        return;
      }
      ref = Object.values(this.PassageLoader.Passages);
      for (x = 0, len1 = ref.length; x < len1; x++) {
        passage = ref[x];
        if (passage && indexOf.call(passage.tags, 'stylesheet') >= 0) {
          ref1 = passage.tags;
          for (y = 0, len2 = ref1.length; y < len2; y++) {
            tag = ref1[y];
            if (indexOf.call(tags, tag) >= 0) {
              callback(passage);
              break;
            }
          }
        }
      }
    }

    setPageElements() {
      var sm, storyTitle;
      if (window.preSetPageElements !== void 0) {
        if ('*' in window.preSetPageElements) {
          window.preSetPageElements['*']();
        }
      }
      setPageElement('storyTitle', 'StoryTitle', this.defaultTitle);
      storyTitle = document.getElementById('storyTitle');
      document.title = this.title = storyTitle && (storyTitle.textContent || storyTitle.innerText) || this.defaultTitle;
      setPageElement('storySubtitle', 'StorySubtitle', '');
      if (tale.has('StoryAuthor')) {
        setPageElement('titleSeparator', null, '\n');
        setPageElement('storyAuthor', 'StoryAuthor', '');
      }
      if (tale.has('StoryMenu')) {
        sm = document.getElementById('storyMenu');
        if (sm === null) {
          sm = document.createElement('div');
          document.body.appendChild(sm);
          sm.setAttribute('id', 'storyMenu');
        }
        sm.setAttribute('style', '');
        setPageElement('storyMenu', 'StoryMenu', '');
      }
      if (window.postSetPageElements !== void 0) {
        //console.log("Calling postSetPageElements");
        if ('*' in window.postSetPageElements) {
          window.postSetPageElements['*']();
        }
      }
    }

    canBookmark() {
      return false;
    }

  };

  Object.defineProperties(NylonStory.prototype, {
    /*
     * Backwards-compatibility with Twine.
     */
    passages: {
      get: function() {
        return this.PassageLoader.Passages;
      }
    },
    /*
     * Backwards-compatibility with Twine.
     */
    defaultTitle: {
      get: function() {
        return this.Config.title || 'Untitled Story';
      }
    },
    /*
     * Backwards-compatibility with Twine.
     */
    settings: {
      get: function() {
        return this.Config;
      }
    },
    /*
     * Backwards-compatibility with Twine.
     */
    storysettings: {
      get: function() {
        return this.Config;
      }
    }
  });

  return NylonStory;

}).call(this);

NylonFileParsingError = class NylonFileParsingError {
  constructor() {}

  toString() {
    return 'Failed to load mod file.';
  }

};

makeRequest = function(method, url, binary) {
  return new Promise(function(resolve, reject) {
    var xhr;
    xhr = new XMLHttpRequest();
    //if binary
    //  xhr.responseType = 'arraybuffer'
    xhr.open(method, url);
    xhr.onload = function() {
      if (this.status >= 200 && this.status < 300) {
        resolve(xhr.response);
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText
        });
      }
    };
    xhr.onerror = function() {
      reject({
        status: this.status,
        statusText: xhr.statusText
      });
    };
    xhr.send();
  });
};

syncFetch = function(method, url, binary) {
  var xhr;
  console && console.log(url);
  xhr = new XMLHttpRequest();
  xhr.open(method, url, false);
  //if binary
  //  xhr.responseType = 'arraybuffer'
  xhr.send();
  return xhr;
};

NylonFileLoader = (function() {
  var objToUrlParams;

  class NylonFileLoader extends NylonLazyLoader {
    constructor(story, config) {
      super(story, config);
      this.load = this.load.bind(this);
      this.fetchNextMod = this.fetchNextMod.bind(this);
      this.Language = indexOf.call(config, 'lang') >= 0 ? config['lang'] : 'json';
      this.ModCount = 0;
      this.ModPageCount = 0;
      this.PagesToLoad = [];
      this.DataPaths = [];
      this.DataPaths.push([indexOf.call(config, 'path') >= 0 ? config['path'] : 'core', indexOf.call(config, 'checksum') >= 0 ? config['checksum'] : null]);
      this.AllowMods = indexOf.call(config, 'allow-mods') >= 0 ? config['allow-mods'] : true;
      this.IndexFile = indexOf.call(config, 'index-file') >= 0 ? config['index-file'] : '__INDEX__';
    }

    getExt() {
      if (this.Story.Deobfuscator.ID !== null) {
        return '.dat';
      }
      switch (this.Language) {
        case 'json':
          return '.json';
        case 'yaml':
          return '.yml';
        default:
          alert('Incorrect NylonFileLoader.Language.  Should be either "json" or "yaml".');
          return null;
      }
    }

    parseSyncResponse(response) {
      var bytes, data;
      console.log("Response:", response);
      bytes = new Uint8Array(response.response);
      bytes = this.Story.Deobfuscator.Deobfuscate(bytes);
      data = {};
      switch (this.Language) {
        case 'json':
          return JSON.parse(arrayBufferToString(bytes));
        case 'yaml':
          return yaml.load(arrayBufferToString(bytes));
        default:
          window.HALT_WAIT = true;
          alert('Incorrect NylonFileLoader.Language.  Should be either "json" or "yaml".');
          return null;
      }
    }

    parseResponse(response, cb) {
      console.log("Response:", response);
      response.arrayBuffer().then((bytes) => {
        var data;
        bytes = this.Story.Deobfuscator.Deobfuscate(bytes);
        data = {};
        switch (this.Language) {
          case 'json':
            cb(JSON.parse(arrayBufferToString(bytes)));
            break;
          case 'yaml':
            cb(yaml.load(arrayBufferToString(bytes)));
            break;
          default:
            window.HALT_WAIT = true;
            alert('Incorrect NylonFileLoader.Language.  Should be either "json" or "yaml".');
        }
      }).catch(function(err) {
        console.error(err);
        return alert('Unable to convert body into an ArrayBuffer.');
      });
    }

    getFromServer(url, data = {}) {
      var uriparams;
      uriparams = '';
      if (Object.keys(data).length > 0) {
        uriparams = '?' + this.objToUrlParams(data);
      }
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      return fetch(url + uriparams, {
        method: 'GET',
        mode: 'same-origin',
        cache: 'no-cache', // No caching.
        credentials: 'omit' // We don't expect to need credentials.
      });
    }

    postToServer(url, data) {
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      return fetch(url + uriparams, {
        method: 'POST',
        mode: 'same-origin',
        cache: 'no-cache', // No caching.
        credentials: 'omit', // We don't expect to need credentials.
        body: JSON.stringify(data)
      });
    }

    load(progress) {
      boundMethodCheck(this, NylonFileLoader);
      this.progress = progress;
      this.PreloadData = {};
      this.Passages = {};
      if (this.AllowMods) {
        //xhr = fetch 'GET', '/mods', false
        this.getFromServer('/mods').then((response) => {
          if (this.checkResponse(response)) {
            response.json().then((data) => {
              var key;
              console.log(data);
              this.DataPaths = [];
              for (key in data) {
                this.DataPaths.push([key, data[key]]);
              }
            });
          }
        }).then(() => {
          this.loadFromDataPaths();
        }).catch(function(err) {
          window.HALT_WAIT = true;
          alert("Unable to fetch /mods from the server.");
          console.error(err);
        });
      } else {
        this.loadFromDataPaths();
      }
    }

    loadFromDataPaths() {
      this.ModCount = this.DataPaths.length;
      this.progress && this.progress.progress('Loading mods', 0, this.ModCount);
      return this.fetchNextMod();
    }

    fetchNextMod() {
      var checksum, key, msg;
      boundMethodCheck(this, NylonFileLoader);
      if (this.DataPaths.length) {
        key = this.DataPaths.pop();
        checksum = null;
        if (typeof key === 'object' && key.constructor.name === 'Array') {
          [key, checksum] = key;
        }
        msg = key === 'core' ? 'Loading game...' : `Loading mod ${key}...`;
        this.progress && this.progress.progress(msg, this.ModCount - this.DataPaths.length, this.ModCount);
        this.loadMod(key, checksum, () => {
          var a;
          a = () => {
            this.fetchNextMod();
          };
          setTimeout(a, 10);
        });
      } else {
        this.progress && this.progress.end();
      }
    }

    checkResponse(response) {
      console.log(`Received ${response.status} ${response.statusText}`);
      return response.status === 200;
    }

    loadMod(mod_id, checksum, cb) {
      /*
       * Index:
       * {"Title":"MD5Checksum"}
       */
      var indexfile;
      indexfile = `/mod/${mod_id}/${this.IndexFile}`;
      this.getFromServer(indexfile).then((response) => {
        var actualchecksum, err;
        if (this.checkResponse(response)) {
          if (!this.AllowMods && checksum) {
            actualchecksum = CryptoJS.MD5(response.responseText);
            if (actualchecksum !== checksum) {
              err = `NylonFileLoader: Index ${indexfile} has a checksum mismatch; It may be corrupt, or you may have modified it. Please reinstall.\n\nOriginal: ${checksum}\nCurrent: ${actualchecksum}`;
              alert(err);
              console.error(err);
              throw new Exception("Checksum mismatch");
            }
          }
          return this.parseResponse(response, (storyIndex) => {
            var childname, index, len, len1, ref, step, x;
            len = Object.keys(storyIndex).length;
            step = 0;
            ref = Object.keys(storyIndex);
            for (index = x = 0, len1 = ref.length; x < len1; index = ++x) {
              childname = ref[index];
              checksum = storyIndex[childname];
              switch (childname) {
                case 'StorySettings':
                  continue;
              }
              this.preloadPassage(childname, index, {
                mod_id: mod_id,
                checksum: checksum
              });
            }
            if (this.LoadLazily) {
              cb();
            }
          });
        }
      });
    }

    /*
     * 2
     * ---
     * [header]
     * ---
     * CONTENT
     */
    loadPassage(passageID, cb) {
      var pd;
      this.Passages[passageID] = null;
      pd = this.PreloadData[passageID];
      //xhr = syncFetch 'GET', "/mod/#{pd.data.mod_id}/#{passageID}#{@getExt()}", @Story.Deobfuscator.ID != null
      this.getFromServer(`/mod/${pd.data.mod_id}/${passageID}${this.getExt()}`).then((response) => {
        var actualchecksum, checksum;
        if (this.checkResponse(response)) {
          console.log("Response checks out!");
          if (!this.AllowMods && pd.data.checksum) {
            checksum = pd.data.checksum;
            actualchecksum = CryptoJS.MD5(response.response);
            if (actualchecksum !== checksum) {
              throw new Exception(`Passage /mod/${pd.data.mod_id}/${passageID}${this.getExt()} has a checksum mismatch; It may be corrupt, or you may have modified it.`);
            }
          }
          this.parseResponse(response, (chunk) => {
            var _, header, text;
            console.log("Received", chunk);
            if (chunk[0] !== NylonFileLoader.FILE_FORMAT_VERSION) {
              throw new NylonFileParsingError('File format version is incorrect');
            }
            [_, header, text] = chunk;
            header.text = text;
            this.Passages[passageID] = new NylonPassage(header.id, header.id, header.tags, text, this.Story.Deobfuscator, header);
            cb && cb(this.Passages[passageID]);
          });
        }
      });
    }

  };

  NylonFileLoader.ID = 'file';

  NylonFileLoader.FILE_FORMAT_VERSION = 2;

  NylonFileLoader.STATE_DEAD = 0;

  NylonFileLoader.STATE_INITIALIZING = 1;

  NylonFileLoader.STATE_INITIALIZED = 2;

  NylonFileLoader.STATE = 0;

  objToUrlParams = function(dict) {
    var k, o, v;
    o = [];
    for (k in dict) {
      if (!hasProp.call(dict, k)) continue;
      v = dict[k];
      o.push(`${convertURIComponent(k)}=${convertURIComponent(v)}`);
    }
    return o.join('&');
  };

  return NylonFileLoader;

}).call(this);

NylonPassageLoader.Register(NylonFileLoader);

version.extensions.ifMacros = {
  major: 2,
  minor: 0,
  revision: 0
};

macros['if'] = {
  handler: function(place, macroName, params, parser) {
    var clauses, conditions, currentClause, currentCond, e, endPos, i, lastIndex, nesting, rawCond, rawConds, src, srcOffset, t;
    conditions = [];
    clauses = [];
    rawConds = [];
    srcOffset = parser.source.indexOf('>>', parser.matchStart) + 2;
    src = parser.source.slice(srcOffset);
    endPos = -1;
    rawCond = params.join(' ');
    currentCond = parser.fullArgs();
    currentClause = '';
    t = 0;
    nesting = 0;
    i = 0;
    while (i < src.length) {
      if (src.substr(i, 6) === '<<else' && !nesting) {
        rawConds.push(rawCond);
        conditions.push(currentCond.trim());
        clauses.push(currentClause);
        currentClause = '';
        t = src.indexOf('>>', i + 6);
        if (src.substr(i + 6, 4) === ' if ' || src.substr(i + 6, 3) === 'if ') {
          rawCond = src.slice(i + 9, t);
          currentCond = Wikifier.parse(rawCond);
        } else {
          rawCond = '';
          currentCond = 'true';
        }
        i = t + 2;
      }
      if (src.substr(i, 5) === '<<if ') {
        nesting++;
      }
      if (src.substr(i, 9) === '<<endif>>') {
        nesting--;
        if (nesting < 0) {
          endPos = srcOffset + i + 9;
          rawConds.push(rawCond);
          conditions.push(currentCond.trim());
          clauses.push(currentClause);
          break;
        }
      }
      currentClause += src.charAt(i);
      i++;
    }
    if (endPos !== -1) {
      parser.nextMatch = endPos;
      lastIndex = 0;
      try {
        i = 0;
        while (i < clauses.length) {
          lastIndex = i;
          if (internalEval(conditions[i])) {
            //console.log(clauses[i]);
            new Wikifier(place, clauses[i]);
            /* Nylon Patch for Twine 1.4.2 Sugarcane */
            window.twineCurrentEl = place;
            window.twineCurrentParser = parser;
            /* End patch */
            break;
          }
          i++;
        }
      } catch (error) {
        e = error;
        console.log(e);
        /* Nylon Patch for Twine 1.4.2 Sugarcane */
        console.log(conditions[lastIndex]);
        console.log(clauses[lastIndex]);
        console.log(e.stack);
        /* End patch */
        throwError(place, '<<' + (i ? 'else ' : '') + 'if>> bad condition: ' + rawConds[i], !i ? parser.fullMatch() : '<<else if ' + rawConds[i] + '>>');
      }
    } else {
      throwError(place, 'I can\'t find a matching <<endif>>', parser.fullMatch());
    }
  }
};

macros['else'] = macros.elseif = macros.endif = {
  handler: function() {}
};

// ---
// generated by js2coffee 2.2.0
// @requires: engine/_funcsAndPolyfills.coffee
// @requires: engine/NylonStory.coffee
window.STARTUP = function() {
  var len1, macro, macroID, passage, passageID, ref, ref1, storyCSS, styleText, x;
  // Initialise Tale
  console.log("STARTUP.coffee: Starting NylonStory");
  window.tale = new NylonStory(window.configuration ? window.configuration : null);
  // Run all scripts
  console.log("STARTUP.coffee: Running scripts");
  ref = tale.lookup('tags', 'scripts');
  for (x = 0, len1 = ref.length; x < len1; x++) {
    passage = ref[x];
    console.log("  STARTUP.coffee: passage", passage);
    scriptEval(passage);
  }
  // Init history
  console.log("STARTUP.coffee: Starting history");
  window.state = tale.initHistory();
  console.log("STARTUP.coffee: Starting macros");
  for (macroID in macros) {
    macro = macros[macroID];
    console.log("  STARTUP.coffee: macro", macroID);
    if (typeof macro.init === 'function') {
      macro.init();
    }
  }
  // Collect CSS from stylesheets
  styleText = '';
  ref1 = tale.passages;
  for (passageID in ref1) {
    passage = ref1[passageID];
    if (!('stylesheet' in passage.tags)) {
      continue;
    }
    if (passage.tags.length === 1 && passage.tags[0] === 'stylesheet') {
      styleText += passage.text;
    } else if (passage.tags.length === 2 && passage.tags[1] === 'transition') {
      setTransitionCSS(passage.text);
    }
  }
  //styleText = alertCSS styleText
  storyCSS = document.getElementById('storyCSS');
  if (storyCSS.styleSheet) {
    storyCSS.styleSheet = styleText;
  } else {
    storyCSS.innerHTML = styleText;
  }
  state.init();
  tale.load();
};

version.extensions.SilentlyMacro = {
  major: 1,
  minor: 1,
  revision: 0
};

macros.nobr = macros.silently = {
  handler: function(place, macroName, f, parser) {
    var a, c, d, h, i, k, l;
    i = void 0;
    h = insertElement(null, 'div');
    k = parser.source.indexOf('>>', parser.matchStart) + 2;
    a = parser.source.slice(k);
    d = -1;
    c = '';
    l = 0;
    i = 0;
    while (i < a.length) {
      if (a.substr(i, macroName.length + 7) === '<<end' + macroName + '>>') {
        if (l === 0) {
          d = k + i + macroName.length + 7;
          break;
        } else {
          l--;
        }
      } else if (a.substr(i, macroName.length + 4) === '<<' + macroName + '>>') {
        l++;
      }
      if (macroName === 'nobr' && a.charAt(i) === '\n') {
        c += '‌';
      } else {
        // Zero-width space
        c += a.charAt(i);
      }
      i++;
    }
    if (d !== -1) {
      new Wikifier((macroName === 'nobr' ? place : h), c);
      /* Nylon Patch for Twine 1.4.2 Sugarcane */
      window.twineCurrentEl = place;
      window.twineCurrentParser = parser;
      /* End patch */
      parser.nextMatch = d;
    } else {
      throwError(place, 'can\'t find matching <<end' + macroName + '>>', parser.fullMatch());
    }
  }
};

macros.endsilently = {
  handler: function() {}
};

// ---
// generated by js2coffee 2.2.0
version.extensions.displayMacro = {
  major: 2,
  minor: 0,
  revision: 0
};

macros.display = {
  parameters: [],
  handler: function(place, macroName, params, parser) {
    var e, j, name, oldDisplayParams, output, t;
    t = void 0;
    j = void 0;
    output = void 0;
    oldDisplayParams = void 0;
    name = parser.fullArgs();
    if (macroName !== 'display') {
      output = macroName;
      // Shorthand displays can have parameters
      params = parser.fullMatch().replace(/^\S*|>>$/g, '').readMacroParams(true);
      try {
        // The above line recreates params, with the quotes.
        j = 0;
        while (j < params.length) {
          params[j] = internalEval(Wikifier.parse(params[j]));
          j++;
        }
      } catch (error) {
        e = error;
        throwError(place, parser.fullMatch() + ' bad argument: ' + params[j], parser.fullMatch());
        return;
      }
    } else {
      try {
        output = internalEval(name);
      } catch (error) {
        e = error;
      }
      if (output === null) {
        // Last-ditch attempt
        if (tale.has(name)) {
          output = name;
        }
      }
    }
    if (!output) {
      throwError(place, '<<' + macroName + '>>: "' + name + '" did not evaluate to a passage name', parser.fullMatch());
    } else if (!tale.has(output + '')) {
      throwError(place, '<<' + macroName + '>>: The "' + output + '" passage does not exist', parser.fullMatch());
    } else {
      oldDisplayParams = this.parameters;
      this.parameters = params;
      tale.get(output + '', (t) => {
        if (t.tags.indexOf('script') > -1) {
          scriptEval(t);
        } else {
          new Wikifier(place, t.processText());
        }
        return this.parameters = oldDisplayParams;
      });
    }
  }
};

// ---
// generated by js2coffee 2.2.0

//# sourceMappingURL=engine2.in.js.map
