
import sys,os
sys.path.append(os.path.join('src', 'python'))

from twinehack.ast.lexer import Lexer

def test_lex_with(inputstr):
    print('IN: {!r}'.format(inputstr))
    lex = Lexer(lextab=None)
    lex.input(inputstr)
    for token in lex:
        print('  '+repr(token))

test_lex_with('howdy<<if derp>><<print lol>><<endif>>')
'''
LexToken(INLINE_HTML,'<a>',1,0)
LexToken(OPEN_SUPERTAG,'<<',1,3)
LexToken(SUPERTAG_IDENTIFIER,'if',1,5)
LexToken(ID,'derp',1,8)
LexToken(CLOSE_SUPERTAG,'>>',1,12)
'''

test_lex_with('Yo wassup <<if $shit >= false>><<print whoa>><<endif>>')
'''
LexToken(INLINE_HTML,'Yo wassup ',1,0)
LexToken(OPEN_SUPERTAG,'<<',1,10)
LexToken(SUPERTAG_IDENTIFIER,'if',1,12)
LexToken(ID,'$shit',1,15)
LexToken(GE,'>=',1,21)
LexToken(FALSE,'false',1,24)
LexToken(CLOSE_SUPERTAG,'>>',1,29)
LexToken(OPEN_SUPERTAG,'<<',1,31)
LexToken(SUPERTAG_IDENTIFIER,'print',1,33)
LexToken(ID,'whoa',1,39)
LexToken(CLOSE_SUPERTAG,'>>',1,43)
LexToken(OPEN_SUPERTAG,'<<',1,45)
LexToken(SUPERTAG_IDENTIFIER,'endif',1,47)
LexToken(CLOSE_SUPERTAG,'>>',1,52)
'''

test_lex_with('Time to test\n<<button [[label]]>>\n<<button [[label|passage()]]>>\n<<button [[label|passage()][$statement="callback"]]>>')

test_lex_with('<<if $test>>test<<elseif $test>>test<<endif>>')
