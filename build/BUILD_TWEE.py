'''
Simple tool to rebuild lextab.py and yacctab.py.
'''

import os
from buildtools import log, os_utils, ENV

log.enableANSIColors()

def rm(filename):
    if os.path.isfile(filename):
        log.info('<red>RM</red>\t%s',filename)
        os.remove(filename)

rm(os.path.join('src','python','twinehack','ast','lextab.py'))
rm(os.path.join('src','python','twinehack','ast','yacctab.py'))

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
ENV.appendTo('PYTHONPATH', os.path.join(SCRIPT_DIR,'lib','twine'))

with os_utils.Chdir(os.path.join('src','python')):
    os_utils.cmd(['python','-m','unittest'])
