
import sys,os
sys.path.append(os.path.join('src', 'python'))

from twinehack.ast.parser import Parser
from twinehack.ast.nodes import dump_slimit_struct

def test_parser_with(inputstr, debug=False):
    print('IN: {!r}'.format(inputstr))
    parser = Parser()
    dump_slimit_struct(parser.parse(inputstr, debug=debug))

test_parser_with('howdy<<if derp>><<print lol>><<endif>>')

test_parser_with('Yo wassup <<if $shit >= false>><<print whoa>><<endif>>')

test_parser_with('<<print $lol>>EmbeddedHTML string<<nobr>>what<<endnobr>>')

test_parser_with('Time to test\n<<button [[label]]>>\n<<button [[label|passage()]]>>\n<<button [[label|passage()][$statement = "callback"]]>>')

test_parser_with('<<if $test>>test<<elseif $test>>test<<endif>>')
