import os, sys
from buildtools import log, os_utils, utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.utils import SerializableFileLambda
from buildtools.maestro.web import MinifySVGTarget, UglifyJSTarget, DartSCSSBuildTarget
from buildtools.maestro.coffeescript import CoffeeBuildTarget
from buildtools.maestro.fileio import ReplaceTextTarget
from buildtools.maestro.git import GitSubmoduleCheckTarget
from buildtools.maestro.package_managers import YarnBuildTarget
#from buildtools.maestro.web import DatafyImagesTarget

import skins.sugarcane as sugarcane

NYLON_BASE = os.path.abspath(os.path.dirname(__file__))
NODE_MODULES_DIR = os.path.join(NYLON_BASE, 'node_modules')
TMP_DIR = os.path.join(NYLON_BASE, 'tmp')
sys.path.append(os.path.join(NYLON_BASE, 'src', 'python'))

from nylon.utils import CoffeeAcquireFilesLambda

def main():
    EXE_SUFFIX = '.exe' if os_utils.is_windows() else ''
    CMD_SUFFIX = '.cmd' if os_utils.is_windows() else ''

    env = os_utils.ENV.clone()
    env.prependTo('PATH', os.path.join(NODE_MODULES_DIR, '.bin'))
    COFFEE = os.path.join(NODE_MODULES_DIR, '.bin', 'coffee'+CMD_SUFFIX)#env.assertWhich('coffee'+CMD_SUFFIX)
    UGLIFYJS = os.path.join(NODE_MODULES_DIR, '.bin', 'uglifyjs'+CMD_SUFFIX)#env.assertWhich('uglifyjs'+CMD_SUFFIX)
    SVGO = os.path.join(NODE_MODULES_DIR, '.bin', 'svgo'+CMD_SUFFIX)#env.assertWhich('uglifyjs'+CMD_SUFFIX)
    SASS = os.path.join(NODE_MODULES_DIR, '.bin', 'dart-sass'+CMD_SUFFIX)

    maestro = BuildMaestro()
    argp = maestro.build_argparser()
    # Not needed, since we build everything before committing.
    #argp.add_argument('--release', action='store_true', help='Builds Nylon for release.  Minifies stuff, deploys to web/.')
    #argp.add_argument('--submodules', action='store_true', help='Checks submodules.')
    args = maestro.parse_args(argp)

    submodules = maestro.add(GitSubmoduleCheckTarget())
    yarn = maestro.add(YarnBuildTarget())

    getJSBuiltinsJS = maestro.add(CoffeeBuildTarget(
        target = os.path.join(TMP_DIR, 'getJSBuiltins.js'),
        files = [os.path.join(NYLON_BASE, 'src', 'coffee', 'getJSBuiltins.coffee')],
        make_map=False,
        coffee_opts=['-bcm', '--no-header'],
        coffee_executable=COFFEE
    ))
    getJSBuiltinsMin = maestro.add(UglifyJSTarget(
        target=os.path.join(TMP_DIR, 'getJSBuiltins.min.js'),
        inputfile=getJSBuiltinsJS.target,
        dependencies=[getJSBuiltinsJS.target],
        uglify_executable=UGLIFYJS))
    getJSBuiltinsHTML = maestro.add(ReplaceTextTarget(
        target=os.path.join(NYLON_BASE, 'tools', 'get-js-classes.htm'),
        filename=os.path.join(NYLON_BASE, 'src', 'get-js-classes.tmpl.htm'),
        replacements={
            r'\{\{ JS \}\}': SerializableFileLambda(getJSBuiltinsJS.target, as_blob=False)
        },
        dependencies=[getJSBuiltinsMin.target]))

    COFFEE_PRELOADS = [
        os.path.join(NYLON_BASE, 'src', 'coffee', 'engine', '_funcsAndPolyfills.coffee'),
        os.path.join(NYLON_BASE, 'src', 'coffee', 'engine', '_globals.coffee')
    ]
    acquired_coffee = CoffeeAcquireFilesLambda(
        os.path.join(NYLON_BASE, 'src', 'coffee', 'engine'),
        manual_load_order=COFFEE_PRELOADS,
        additional_files=[],
        files_before=COFFEE_PRELOADS)

    minLoadSVG = maestro.add(MinifySVGTarget(
        target=os.path.join(TMP_DIR, 'loadlogo.min.svg'),
        source=os.path.join(NYLON_BASE, 'assets', 'images', 'svg', 'loadlogo.svg'),
        svgo_executable=SVGO
    ))

    scriptingCoffeeJS = maestro.add(CoffeeBuildTarget(
        target=os.path.join(TMP_DIR, 'engine2.in.js'),
        files=acquired_coffee,
        dependencies=[submodules.target, yarn.target],
        make_map=False,
        coffee_opts=['-bcm', '--no-header'],
        coffee_executable=COFFEE))

    replacedJS = maestro.add(ReplaceTextTarget(
        target=os.path.join(NYLON_BASE, 'src', 'engine2.js'),
        filename=scriptingCoffeeJS.target,
        replacements={
            r'\{LOADICON\}': SerializableFileLambda(minLoadSVG.target, as_blob=True)
        },
        dependencies=[minLoadSVG.target]))

    minifiedEngine = UglifyJSTarget(
        target=os.path.join(NYLON_BASE, 'src', 'engine2.min.js'),
        inputfile=replacedJS.target,
        dependencies=[replacedJS.target],
        uglify_executable=UGLIFYJS)
    maestro.add(minifiedEngine)

    sugarcane.addToBuild(maestro, COFFEE, UGLIFYJS, SASS, deps=[yarn.target, submodules.target])

    maestro.as_app(argp)

if __name__ == '__main__':
    main()
