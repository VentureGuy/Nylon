## Breaking up SET statements
This breaks up long `<<set>>` statements so they're easier to read.
**Search:** `; (\$[^;\n>]*)`<br>
**Replace:** `>>\\\n<<set $1`

## Convert br lists to tables
Used for lists that look like `''Label:'' <<print $value>>` with an optional `<br>` at the end.
**Search:** `''([A-Za-z]+:)''\s+(<<[^>]+>>)(<br>)?`<br>
**Replace:** `<tr>\n\t\t<th>$1</th>\n\t\t<td>$2</td>\n\t</tr>`
