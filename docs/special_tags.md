# Special Passage Tags

## Twine
<table>
<thead><th>Tag</th><th>Description</th></thead>
<tbody>
<tr><th><code>nobr</code></th><td>Replaces newlines (`\n`) with non-breaking spaces, tells the JS engine not to replace anything with `<br>` tags.</td></tr>
<tr><th><code>stylesheet</code></th><td>Treats the passage as CSS.  Only `[img[]]` macros are processed.</td></tr>
<tr><th><code>script</code></th><td>Treats the passage as raw javascript.  Only `[img[]]` macros are processed.</td></tr>
<tr><th><code>image</code></th><td>Treats the passage as an image blob.</td></tr>
<tr><th><code>annotation</code></th><td>Marks the passage as a Twine annotation. No processing is done.</td></tr>
</tbody></table>

## Nylon
<table>
<thead><th>Tag</th><th>Description</th></thead>
<tbody>
<tr><th><code>nylon:strip-lines</code></th><td>Strips whitespace at the beginning and end of each line prior to compiling. Useful for indented passages.</td></tr>
</tbody></table>
