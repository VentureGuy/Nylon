'''
Dump all passages from Twine 1.4.x HTML file.
'''
import os
import sys
import argparse

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'src', 'python'))

from twinehack import NylonExporter

def main():
    argp = argparse.ArgumentParser()
    argp.add_argument('input', help='Path of the original compiled .html file.')
    argp.add_argument('output', help='Path of the resultant tidied .html file.  SHOULD BE IN ITS OWN DIRECTORY!')
    argp.add_argument('--skip-unfuck', action='store_true', help='Disable encoding fix step, which takes a long time.')
    argp.add_argument('--skip-yml', action='store_true', help='Disable producing YML headers (makes diffing easier).')

    args = argp.parse_args()
    NylonExporter().export_tale(args.input, args.output, skip_unfuck=args.skip_unfuck, skip_yml=args.skip_yml)

if __name__ == '__main__':
    main()
