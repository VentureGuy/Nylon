import os
import sys
import argparse
import re

from buildtools import os_utils

sys.path.append(os.path.join('src', 'python'))
from twinehack.ast.parser import PassageParser
from twinehack.ast.nodes import *
from twinehack.ast.PrettifySugar import PrettifySugar, EParaStyle

def main():
    argp = argparse.ArgumentParser()
    argp.add_argument('--strip-nobr', action='store_true', help='Remove <<nobr>>/<<endnobr>> tags.')
    argp.add_argument('--split-conditions', action='store_true', help='Split conditions in <<if>>/<<elseif>> tags to their own lines. DOES NOT COMPILE IN LIVE GAMES, ONLY USE FOR ANALYSIS.')
    argp.add_argument('-p', '--para-style', choices=['none','p','br'], default="none", help="How to format paragraphs. NONE=No additional formatting, P=<p> tags, BR=<br> tags.")
    argp.add_argument('inputfile', help='Path of the input file.')
    argp.add_argument('outputfile', help='Path of the output file.')
    args = argp.parse_args()
    pp = PassageParser()
    pp.parseFile(args.inputfile)
    os_utils.ensureDirExists(os.path.dirname(args.outputfile), noisy=True)
    pstyle = EParaStyle.NONE
    pstyles = {
        'none': EParaStyle.NONE,
        'p': EParaStyle.P,
        'br': EParaStyle.BR
    }
    if args.para_style != 'none':
        pstyle = pstyles[args.para_style.lower()]
    PrettifySugar(pp.passage).prettifyToFile(args.outputfile, strip_nobr=args.strip_nobr, split_conditions=args.split_conditions, paragraph_style=pstyle)


if __name__ == '__main__':
    main()
