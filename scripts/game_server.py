import os, hashlib, sys#, json

from typing import Any
from flask import Flask, jsonify#, request

cli = sys.modules['flask.cli']
cli.show_server_banner = lambda *x: None

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
HTML_FILE = os.path.join(SCRIPT_DIR, 'core', 'index.template.html')
CACHE_DIR = os.path.join(SCRIPT_DIR, '.cache')
FONTS_DIR = os.path.join(SCRIPT_DIR, 'fonts')
CORE_DIR = os.path.join(SCRIPT_DIR, 'core')
MODS_DIR = os.path.join(SCRIPT_DIR, 'mods')

FILENAME = '@@FILENAME@@'
ENGINE_NAME = '@@ENGINE_NAME@@'
ENGINE_VERSION = '@@ENGINE_VERSION@@'
GAME_NAME = '@@GAME_NAME@@'
GAME_VERSION = '@@GAME_VERSION@@'

def hashfile(afile, hasher, blocksize=65536):
    if isinstance(afile, str):
        with open(afile, 'rb') as f:
            return hashfile(f, hasher, blocksize)
    buf = afile.read(blocksize)
    while buf == b'':
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()


def md5sum(filename, blocksize=65536):
    with open(filename, 'rb') as f:
        return hashfile(f, hashlib.md5())


def sha256sum(filename, blocksize=65536):
    with open(filename, 'rb') as f:
        return hashfile(f, hashlib.sha256())

app = Flask(__name__)
@app.route('/')
def output_game():
    with open(HTML_FILE) as f:
        return f.read()

@app.route('/fonts/<path:path>')
def output_fonts(path=[]):
    for pc in path.split('/'):
        if pc.startswith('..'):
            return 'No'
    fullpath = os.path.abspath(os.path.join(FONTS_DIR, path))
    if not fullpath.startswith(FONTS_DIR):
        return 'No'
    with open(fullpath, 'rb') as f:
        return f.read()

@app.route('/mods')
def get_mod_list():
    o = {}
    o['core']=md5sum(os.path.join(CORE_DIR, '__INDEX__'))
    if os.path.isdir('mods'):
        for dirname in os.listdir('mods'):
            idx = os.path.join('mods', dirname, '__INDEX__')
            o[dirname]=md5sum(idx)
    return jsonify(o)

@app.route('/mod/<mod_id>/<basename>')
def get_mod_data(mod_id, basename):
    basename = basename.split('/')[-1]
    fullpath = os.path.abspath(os.path.join(MODS_DIR, mod_id, basename))
    if not fullpath.startswith(MODS_DIR):
        return 'No'
    if mod_id == 'core':
        filename = os.path.join(CORE_DIR, basename)
    with open(filename, 'rb') as f:
        return f.read()

def argp_port(_x: Any) -> Any:
    import argparse
    x = int(_x)
    if x < 0:
        raise argparse.ArgumentTypeError("%r must be a positive integer between 0 and 65535", _x)
    if x > 65535:
        raise argparse.ArgumentTypeError("%r must be a positive integer between 0 and 65535", _x)
    return x

def main() -> None:
    import argparse
    print(f'{ENGINE_NAME} v{ENGINE_VERSION} - Game Server')
    print(f'Game: {GAME_NAME} v{GAME_VERSION}')

    # https://pyinstaller.readthedocs.io/en/v3.3.1/runtime-information.html
    if getattr(sys, 'frozen', False):
        print(f'The source code for this pyinstaller executable is available at {FILENAME}.')

    argp = argparse.ArgumentParser()
    argp.add_argument('--port', type=argp_port, help="Port to listen on. Default: random available port (0)", default=0)
    args = argp.parse_args()

    suffix = ''
    if args.port == 0:
        suffix += ', and will use a random available port'
    else:
        suffix += f'. You have elected to use port {args.port:g} instead of a random one.'
    print(f'For security reasons, this server will only be visible to you{suffix}.')
    app.run(host='127.0.0.1', port=args.port)

if __name__ == '__main__':
    main()
