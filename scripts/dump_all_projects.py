import yaml
import shutil
import sys
import os
from buildtools import os_utils, log
from buildtools.utils import sha256sum

sys.path.append(os.path.join('src', 'python'))
from twinehack import NylonExporter, unfuckFile, get_file_list


with open('projects.yml', 'r') as f:
    for _id, path in yaml.load(f).items():
        outpath = os.path.join('projects', _id)
        before = get_file_list(outpath, start='.')
        if os.path.isfile(outpath):
            os.remove(outpath)
        #if os.path.isdir(outpath):
        #    shutil.rmtree(outpath)
        os_utils.ensureDirExists(outpath)
        utf8file = os.path.join(outpath, '.orig.utf8')
        if os_utils.canCopy(path, utf8file):
            unfuckFile(path, utf8file)
        outfile = os.path.join(outpath, 'index.html')
        written = NylonExporter().export_tale(utf8file, outfile, skip_unfuck=True, skip_yml=False, pathmapfile='PassageMap.yml') + [utf8file]
        remove = []

        for filename in set(before) - set(written):
            if '.git' in filename:
                continue
            #remove.append(filename)
            log.info('rm %s', filename)
            os.remove(filename)

        #with open(os.path.join(outpath, 'sync.yml'), 'w') as f:
        #    yaml.dump({'before':before, 'written':written, 'remove':remove}, f, default_flow_style=False)
