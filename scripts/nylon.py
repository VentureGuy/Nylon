import os
import sys
import argparse

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'src', 'python'))
from twinehack import NylonBuilder

def main():
    argp = argparse.ArgumentParser()
    argp.add_argument('path', help='Path of the project.')
    argp.add_argument('-o', '--output', help='Path of the resultant tidied .html file.  SHOULD BE IN ITS OWN DIRECTORY!', metavar='output.html')
    #argp.add_argument('--skip-unfuck', action='store_true', help='Disable encoding fix step, which can take a long time.')
    args = argp.parse_args()
    nylon = NylonBuilder(args.path)
    nylon.outfile = args.output
    nylon.Compile()

if __name__ == '__main__':
    main()
