import argparse
import os
import sys
from buildtools import log, os_utils
from datetime import datetime

import yaml

DATEFORMAT = '%Y%m%d%H%M'


def main():
    argp = argparse.ArgumentParser()
    argp.add_argument('filename', type=str, help='filename of the .tw file.')
    argp.add_argument('-t', '--tags', type=str, default='', help='Extra tags to add to header.')
    argp.add_argument('-a', '--no-autotags', action='store_true', help='Don\'t automatically detect tags.')

    args = argp.parse_args()
    basefn, ext = os.path.splitext(os.path.basename(args.filename))
    abspath = os.path.abspath(args.filename)
    absfn, ext = os.path.splitext(abspath)
    dirname = os.path.dirname(abspath)
    os_utils.ensureDirExists(dirname, noisy=True)

    tags = [x.strip() for x in args.tags.split(',') if x != '']

    if not args.no_autotags:
        print(ext)
        if ext == '.tw':
            tags += [
                'nobr',
                'nylon:strip-lines',
            ]

    if not os.path.isfile(abspath):
        log.info('Writing %s...', abspath)
        with open(abspath, 'w') as f:
            f.write('')
    log.info('Writing %s...', absfn + '.header.yml')
    with open(absfn + '.header.yml', 'w') as f:
        header = {
            'id': basefn,
            'created': datetime.strftime(datetime.now(), DATEFORMAT),
            'modifier': 'twee',
            'tags': list(set(tags)),
            'twine': {
                'pos': '0,0'
            }
        }
        yaml.dump(header, f, default_flow_style=False)


if __name__ == '__main__':
    main()
