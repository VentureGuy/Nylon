import argparse
import os
import pickle
import sys

import yaml

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'lib', 'twine'))

# MUST come after the syspath modification. Needed for pickle.
import tiddlywiki


def load_tws(from_file):
    data = {}
    with open(from_file, 'rb') as f:
        data = pickle.load(f)
    return data


def main():
    argp = argparse.ArgumentParser()
    argp.add_argument('input', type=str, metavar='input.tws')
    argp.add_argument('output', type=str, metavar='output.yml')
    args = argp.parse_args()
    data = load_tws(args.input)
    with open(args.output, 'w') as f:
        yaml.dump(data, f)


if __name__ == '__main__':
    main()
