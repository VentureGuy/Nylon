export WINE_PREFIX=".wine"
export PYTHON_VERSION=3.8.3
wget -O winetricks https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
wget -O python.zip https://www.python.org/ftp/python/${PYTHON_VERSION}/python-${PYTHON_VERSION}-embed-win32.zip
./winetricks -q win10
./winetricks vcrun2015
mkdir -pv .wine/drive_c/python3
unzip python.zip -d .wine/drive_c/python3
wget -O .wine/drive_c/get-pip.py https://bootstrap.pypa.io/get-pip.py
wine .wine/drive_c/python3/python.exe .wine/drive_c/get-pip.py
