import os
import sys
import argparse
import re

from buildtools import os_utils

sys.path.append(os.path.join('src', 'python'))
from twinehack.ast.parser import PassageParser
from twinehack.ast.Sugar2Coffee import Sugar2Coffee

def main():
    argp = argparse.ArgumentParser()
    argp.add_argument('inputfile', help='Path of the input file.')
    argp.add_argument('outputfile', help='Path of the output file.')
    args = argp.parse_args()
    pp = PassageParser()
    pp.parseFile(args.inputfile)
    os_utils.ensureDirExists(os.path.dirname(args.outputfile), noisy=True)
    Sugar2Coffee(pp.passage).transcodeToFile(args.outputfile, indentchars='\t')


if __name__ == '__main__':
    main()
