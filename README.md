# Nylon
*What Happens when a Sleep-Deprived GameDev takes a Hatchet to Twine*

Nylon originally started as a complex wrapper around modded Twine 1.4.2, designed to permit CLI
building of Twine applications, while also providing reverse-engineering functionality,
AST analysis and manipulation, and a slew of other tools.

Nylon is now an engine of its own, using new features modern browsers support, and the compiler
is as fast as a bat out of Hell since it has to do less processing. The engine has been rebuilt
with the intent of permitting easy modding of Nylon games.

**Nylon's game code has been completely recoded** with the exception of TiddlyWiki.

# Caveats
Backwards-compatibility with Twine is attempted, but no promises are made.  One major change has been limiting
the `history` to 5 entries to avoid memory leaks.

For God's sake, don't use it unless you know what you're doing.  Right now, it's
in its infancy and I *just* moved everything around.

Documentation soon-ish.

# What needs to be done?

* [ ] Testing apparatus needs some TLC.
* [ ] Code documentation (*ugh*)
* [ ] Installation Documentation (WIP)
* [x] Rearrange the code so that everything isn't in `__init__.py`.
