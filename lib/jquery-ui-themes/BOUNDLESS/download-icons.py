import os, re
from buildtools import cmd, log
import yaml
BASE_URI = 'http://download.jqueryui.com/themeroller/images/'


CONFIG = {
    'ui-icons_{COLOR}_256x240.png': [
        'ffffff',
        '222222',
        'e61c98',
        '4b8e0b',
        'a83300',
        'cccccc',
    ],
    'ui-bg_highlight-soft_{COLOR}_1x100.png': [
        #'35_222222',
        '33_003147',
        '80_eeeeee'
    ],
    'ui-bg_highlight-hard_{COLOR}_1x100.png': [
        '20_0972a5'
    ],
    'ui-bg_glass_{COLOR}_1x400.png':[
        '40_ffc73d'
    ]
}
REG_IMAGEURL=re.compile(r'url\("([^"]+)"\)')
def main():
    import argparse
    argp = argparse.ArgumentParser()
    argp.add_argument('origfile', help="Original jQuery Theme")
    argp.add_argument('filebasedir', help="Directory to dump icons in.")
    args = argp.parse_args()

    download_icons(args.origfile, args.basedir)

def download_icons(origfile, basedir):
    NEWCONFIG = {}
    with open(origfile, 'r') as f:
        for line in f:
            for match in REG_IMAGEURL.findall(line):
                url = match.group(1)
                if url.startswith("images/"):
                    chunks = url.split('_')
                    key = '_'.join([chunks[0], '{COLOR}', chunks[-1]])
                    color = '_'.join([chunks[1:-2]])
                    if key not in NEWCONFIG:
                        NEWCONFIG[key]=[]
                    if color not in NEWCONFIG[key]:
                        NEWCONFIG[key].append(color)
    with open('jquery-config.yml', 'w') as w:
        yaml.dump(NEWCONFIG, w, default_flow_style=False)
    for _filename, colors in CONFIG.items():
        for color in colors:
            filename =_filename.format(COLOR=color)
            dlurl = BASE_URI+filename
            target = os.path.join(basedir, filename)
            if not os.path.isfile(target):
                cmd(['wget', '-O', target, dlurl], critical=True, show_output=True)
